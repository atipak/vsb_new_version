from flask import Flask, request, session, g, redirect, url_for, render_template, flash, send_from_directory, abort
import os
import json
from functools import wraps
import werkzeug
import multiprocessing
import csv
import zipfile
import sys

import src.main as vsb

app = Flask(__name__)
app.secret_key = os.urandom(24)

LIBRARIES = [
    ["http://getbootstrap.com/", "bootstrap.png", "Bootstrap"],
    ["https://jquery.com/", "jquery.png", "JQuery"],
    ["http://www.rdkit.org/", "rdkit.png", "RDkit"],
    ["http://flask.pocoo.org/", "flask.png", "Flask"]
]

temporaly_results = {}
actual_tasks = {}
config = {}
i = 0
l = 0
k = 0
vsb_app = None
data_set_added = True
path_remote_configurations = "../configurations/remote_computers/"


# decorators
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'logged_in' not in session:
            return redirect(url_for('main_site', next=request.url))
        return f(*args, **kwargs)

    return decorated_function


def json_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.json:
            abort(400)
        return f(*args, **kwargs)

    return decorated_function


def files_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.files:
            abort(400)
        return f(*args, **kwargs)

    return decorated_function


# main addresses

@app.route("/", methods=['GET', 'POST'])
def main_site():
    error = None
    if request.method == 'POST':
        if "login" in request.form or "host" in request.form:
            # try log user in
            if "host" in request.form:
                username = "host"
            else:
                username = request.form["username"]
            return_value, id = check_user(username)
            if not return_value:
                error = 'Invalid username'
            else:
                session['user_id'] = id
                session['username'] = username
                session['logged_in'] = True
                flash('You were logged in')
                return render_template('home.html')
        elif "registration" in request.form:
            # try log user in
            if str(request.form['username']).strip() == "":
                error = 'Username cannot be an empty string'
            else:
                return_value, id_user = add_new_user(str(request.form['username']).strip(),
                                                     str(request.form['details']).strip())
                if not return_value:
                    error = id_user
                else:
                    session['user_id'] = id_user
                    session['username'] = request.form['username']
                    session['logged_in'] = True
                    flash('You were logged in')
                    return render_template('home.html')
    return render_template('home.html', ERROR=error)


# login, registration, log out

@app.route("/logout.html")
def logout_site():
    # log out
    session.pop('logged_in', None)
    return render_template('logout.html')


# logged

@app.route("/home/")
@app.route("/home.html")
@login_required
def logged_home_site():
    return render_template('home.html')


# run pages

@app.route("/run_ls.html", methods=['GET', 'POST'])
@login_required
def logged_run_ls():
    type = "ls"
    error = None
    if request.method == 'POST':
        # try read data from page
        dict = request.form
        if "user_id" in session:
            user_id = session["user_id"]
        else:
            user_id = 1
        b_result, d = reading_run_options(dict, request.files, user_id)
        if b_result == False:
            error = "Bad basic parameters"
        else:
            id = new_task_run(d, type)
            return render_template('id_of_query.html', ID=id)
    return render_template('run_ls.html', ERROR=error, TYPE=type, METHODS=get_methods("run"),
                           selections=get_selections())


@app.route("/run_rc.html", methods=['GET', 'POST'])
@login_required
def logged_run_rc():
    error = None
    type = "rc"
    if request.method == 'POST':
        # try read data from page
        dict = request.form
        if "user_id" in session:
            user_id = session["user_id"]
        else:
            user_id = 1
        b_result, d = reading_run_options(dict, request.files, user_id)
        if b_result == False:
            error = "Bad basic parameters"
        else:
            b_result, d = add_rc_infos(dict, d)
            if not b_result:
                error = "Bad pcs parameters"
            else:
                id = new_task_run(d, type)
                return render_template('id_of_query.html', ID=id)
    if os.path.exists(path_remote_configurations):
        configurations = os.listdir(path_remote_configurations)
    else:
        configurations = []
    return render_template('run_rc.html', ERROR=error, TYPE=type, METHODS=get_methods("run"),
                           selections=get_selections(), configurations=configurations)


@app.route("/run_mc.html", methods=['GET', 'POST'])
@login_required
def logged_run_mc():
    type = "mc"
    error = None
    if request.method == 'POST':
        # try read data from page
        dict = request.form
        if "user_id" in session:
            user_id = session["user_id"]
        else:
            user_id = 1
        b_result, d = reading_run_options(dict, request.files, user_id)
        if b_result == False:
            error = "Bad basic parameters"
        else:
            b_result, d = add_metacentrum_infos(dict, d)
            if not b_result:
                error = "Bad log in parameters"
            else:
                id = new_task_run(d, type)
                return render_template('id_of_query.html', ID=id)
    return render_template('run_mc.html', NODES=get_front_nodes(), ERROR=error, TYPE=type, METHODS=get_methods("run"),
                           selections=get_selections())


# put pages

@app.route("/put_data_set.html", methods=['GET', 'POST'])
@login_required
def logged_put_data_set():
    error = ""
    if request.method == 'POST':
        data_set_f = request.files['info_file']
        sdf_f = request.files['sdf_file']
        if data_set_f and allowed_file(data_set_f.filename) and sdf_f and allowed_file(sdf_f.filename):
            data_set_path = os.path.abspath(
                os.path.join(get_temp_path(), werkzeug.secure_filename(data_set_f.filename)))
            sdf_path = os.path.abspath(os.path.join(get_temp_path(), werkzeug.secure_filename(sdf_f.filename)))
            data_set_f.save(data_set_path)
            sdf_f.save(sdf_path)
            data = {
                "web": True,
                "task_type": "put",
                "task_subtype": "dataset",
                "sdf_file": sdf_path,
                "data_file": data_set_path
            }
            if error == "":
                try:
                    id, b_value = vsb.pc_service(data, None)
                    if b_value:
                        text = "Dataset name in database is " + str(id)
                        os.remove(sdf_path)
                        os.remove(data_set_path)
                        return render_template('information_page.html', text=text)
                    else:
                        return render_template('put_data_set.html',
                                               message="New data-set will be validated. If there is missing a molecule in SDF file which is written in data file, data-set won't be added. ",
                                               ERROR=id)
                except Exception as e:
                    return render_template('put_data_set.html',
                           message="New data-set will be validated. If there is missing a molecule in SDF file which is written in data file, data-set won't be added. ",
                           ERROR=str(e))
    return render_template('put_data_set.html',
                           message="New data-set will be validated. If there is missing a molecule in SDF file which is written in data file, data-set won't be added. ")


@app.route("/put_method.html", methods=['GET', 'POST'])
@login_required
def logged_put_method():
    error = ""
    if request.method == 'POST':
        method_type = request.form['method_type']
        if method_type == "Python":
            method_type = "py"
        else:
            method_type = "general"
        file = request.files['info_file']
        method_zip_path = None
        if file:
            file_n = werkzeug.secure_filename(file.filename)
            if str(file_n).endswith(".zip"):
                method_zip_path = os.path.abspath(os.path.join(get_temp_path(), file_n))
                file.save(method_zip_path)
            else:
                error = "File type isn't a zip"
        py_method_type = None
        if method_type == "py":
            py_method_type = request.form['py_m_type_option']
            py_method_type = str(py_method_type).replace("opt_", "")
        # unzip file
        if method_zip_path is not None:
            with zipfile.ZipFile(method_zip_path) as myzip:
                i = 0
                for file in myzip.namelist():
                    splited = os.path.split(file)
                    dir_part = splited[0]
                    file_part = splited[1]
                    splited_two = os.path.split(dir_part)
                    dir_part_two = splited_two[0]
                    file_part_two = splited_two[1]
                    if not (not dir_part == "" and not file_part == "") and not (
                                not dir_part_two == "" and not file_part_two == ""):
                        i += 1
                if not i == 1:
                    error = "More members in Zip file"
                info = myzip.infolist()[0]
                myzip.extractall(path=get_temp_path())
                method_path = os.path.abspath(os.path.join(get_temp_path(), info.filename))
            data = {
                "web": True,
                "task_type": "put",
                "task_subtype": "method",
                "method_type": method_type,
                "file_path": method_path
            }
            if py_method_type is not None:
                data["py_m_type_option"] = py_method_type
            txt = ""
            if error == "":
                # checking for duplicate
                data_check = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "method"
                }
                if method_type == "py":
                    if py_method_type == "repr":
                        data_check['l_name'] = info.filename
                    else:
                        data_check['s_name'] = info.filename
                else:
                    data_check['l_name'] = info.filename
                founded = vsb.pc_service(data_check, None)
                if founded is not None or len(founded) > 0:
                    txt = "Method with such name already is in database. \n"
                id = vsb.pc_service(data, None)
                if id is not None:
                    txt += "Your method was added. It's id is {0}".format(id)
                    return render_template('information_page.html', text=txt)
    return render_template('put_method.html', ERROR=error)


@app.route("/put_selection.html", methods=['GET', 'POST'])
@login_required
def logged_put_selection():
    error = ""
    if request.method == 'POST':
        data_set_id = str(request.form['data_set_id']).strip()
        if data_set_id == "":
            error = "Insert data-set ID"
        else:
            selection_f = request.files['info_file']
            selection_path = None
            if selection_f and allowed_file(selection_f.filename):
                selection_path = os.path.abspath(
                    os.path.join(get_temp_path(), werkzeug.secure_filename(selection_f.filename)))
            if selection_path is not None:
                selection_f.save(selection_path)
                data = {
                    "web": True,
                    "task_type": "put",
                    "task_subtype": "selection",
                    "id_dataset": data_set_id,
                    "data_file": selection_path
                }
                if error == "":
                    text, b_value = vsb.pc_service(data, None)
                    if b_value:
                        return render_template('information_page.html', text=text)
                    else:
                        return render_template('put_data_set.html',
                                               message="New data-set will be validated. If there is missing a molecule in SDF file which is written in data file, data-set won't be added. ",
                                               ERROR=id)
    return render_template('put_selection.html', ERROR=error, DATASETS=get_data_sets())


# show pages
@app.route("/method_details_<method_id>.html", methods=['GET', 'POST'])
@login_required
def details_on_method(method_id):
    chosen_option = "method_details"
    error = ""
    if request.method == 'POST':
        # try read data from page
        for key in request.form:
            if not str(key).find("bt_") == -1:
                index = int(str(key).replace("bt_", ""))
                if chosen_option in temporaly_results:
                    return show_details(chosen_option, temporaly_results[chosen_option], index)
                else:
                    break
            if not str(key).find("csv_") == -1:
                if chosen_option in temporaly_results:
                    csv_path = get_path(0, "csv")
                    header = ["ID of subtest", "Data-set", "Selection", "Loading function",
                              "Score function"]
                    # gens, loads, scores
                    ms = temporaly_results[chosen_option]
                    with open(csv_path, "w", newline="\n") as csv_file:
                        c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                        c_writer.writerow(header)
                        for m in ms:
                            c_writer.writerow(m)
                    return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
                else:
                    break

    # get method
    data = {
        "task_type": "get",
        "task_subtype": "method",
        "id": method_id,
        "web": True
    }
    m = vsb.pc_service(data, None)
    method = {}
    if len(m) == 0:
        return abort
    method["type"] = m[0][1]
    method["id"] = str(method_id)
    ids_names = []
    if m[0][1] == "general":
        method["name"] = m[0][4]
        ids_names.append([method_id, m[0][4], "Nothing"])
    else:
        if m[0][4].lower() == "empty":
            method["name"] = m[0][5]
            # score
            data = {
                "task_type": "get",
                "task_subtype": "method",
                "s_name": m[0][5],
                "web": True
            }
            ms = vsb.pc_service(data, None)
            for m in ms:
                ids_names.append([m[0], m[4], m[5]])
        elif m[0][5].lower() == "empty":
            # loading
            method["name"] = m[0][4]
            data = {
                "task_type": "get",
                "task_subtype": "method",
                "l_name": m[0][4],
                "web": True
            }
            ms = vsb.pc_service(data, None)
            for m in ms:
                ids_names.append([m[0], m[4], m[5]])
        else:
            method["name"] = m[0][4] + "/" + m[0][5]
            ids_names.append([method_id, m[0][4], m[0][5]])
    tests = []
    for id_value_name in ids_names:
        data = {
            "task_type": "get",
            "task_subtype": "test",
            "method_id": id_value_name[0],
            "web": True
        }
        tsts = vsb.pc_service(data, None)
        for t in tsts:
            tests.append([id_value_name[1], id_value_name[2]] + list(t))
    subtests = []
    for test in tests:
        test_id = test[2]
        data = {
            "task_type": "get",
            "task_subtype": "subtest",
            "test_id": test_id,
            "web": True
        }
        subs, names = vsb.pc_service(data, None)
        # 0 = ID of subtest, 2 dataset, 3 selection
        for sub in subs:
            subtests.append([sub[0], sub[2], sub[3], test[0], test[1]])
    temporaly_results[chosen_option] = subtests
    an_res = None
    count = 0
    an_dict = {}
    an = None
    # analysis
    for sbt in subtests:
        data = {
            "task_type": "get",
            "task_subtype": "analyze",
            "subtest_id": sbt[0],
            "web": True
        }
        ad, an = vsb.pc_service(data, None)
        if ad is not None and len(ad) > 0:
            if an_res is None:
                an_res = []
                for ts in range(2, len(ad[0])):
                    an_res.append(0)
            count += 1
            for ts in range(2, len(ad[0])):
                an_res[ts - 2] += ad[0][ts]
    if an is not None:
        for ts in range(2, len(an)):
            an_dict["Average " + an[ts]] = an_res[ts - 2] / count
    else:
        an_dict["Information"] = "No data to process"
    return render_template('methods_details.html', ERROR=error,
                           reference="/method_details_{0}.html".format(method_id),
                           HEADERS_TABLE=["ID of subtest", "Data-set", "Selection", "Loading function",
                                          "Score function"], RESULTS_TABLE=subtests, method=method,
                           methods_details=an_dict)


@app.route("/molecule_details/<molecule_name>/<subtest_id>.html", methods=['GET', 'POST'])
@login_required
def details_on_molecule(molecule_name, subtest_id):
    chosen_option = "molecule_details"
    error = ""
    if request.method == 'POST':
        # try read data from page
        for key in request.form:
            if not str(key).find("bt_") == -1:
                index = int(str(key).replace("bt_", ""))
                if chosen_option in temporaly_results:
                    return show_details(chosen_option, temporaly_results[chosen_option], index)
                else:
                    break
    if str(subtest_id).startswith("t"):
        d = str(subtest_id)[1:]
    else:
        data = {
            "task_type": "get",
            "task_subtype": "subtest",
            "id": subtest_id,
            "web": True
        }
        subtests, names = vsb.pc_service(data, None)
        if not len(subtests) == 1:
            return abort
        else:
            d = subtests[0][2]
    result_dict = {}
    data = {
        "task_type": "get",
        "task_subtype": "molecule",
        "molecule_name": molecule_name,
        "molecule_info": True,
        "dataset_id": d,
        "web": True
    }
    molecules, names = vsb.pc_service(data, None)
    count = len(molecules)
    if 0 < count:
        sum = 0
        max_value = float(molecules[0][0])
        min_value = float(molecules[0][0])
        for mol in molecules:
            score = float(mol[0])
            if max_value < score:
                max_value = score
            if min_value > score:
                min_value = score
            sum += score
        result_dict["Maximum score"] = max_value
        result_dict["Minimum score"] = min_value
        result_dict["Average score"] = sum / count
    data = {
        "task_type": "get",
        "task_subtype": "molecule",
        "true_value": True,
        "molecule_name": molecule_name,
        "data_set_id": d,
        "web": True
    }
    result_dict["True value"] = vsb.pc_service(data, None)
    result_dict["Data-set ID"] = d
    result_dict["Molecule's name"] = molecule_name
    png_path = get_path(molecule_name, "png")
    # creating image
    data = {
        "task_type": "get",
        "task_subtype": "molecule",
        "image": True,
        "molecule_name": molecule_name,
        "dataset_id": d,
        "png_file": os.path.abspath(png_path),
        "web": True
    }
    try:
        vsb.pc_service(data, None)
        if not os.path.exists(png_path):
            raise IOError
        img = True
    except:
        img = False
        error = "If you want to see an image of molecule, check, if you have installed PIL package"
    temporaly_results[chosen_option] = result_dict
    return render_template('molecule_description.html', molecule=result_dict, ERROR=error,
                           image_path=os.path.basename(get_image_path()) + "/" + os.path.basename(png_path),
                           names=names, molecules=molecules, IMG=img)


@app.route("/subtest_details_<subtest_id>.html", methods=['GET', 'POST'])
@login_required
def details_on_subtest(subtest_id):
    error = ""
    chosen_option = "subtest_details"
    if request.method == 'POST':
        if "r_file" in request.form:
            data = {
                "task_type": "get",
                "task_subtype": "file",
                "subtest_id": subtest_id,
                "web": True
            }
            result_files = vsb.pc_service(data, None)
            if len(result_files) > 0:
                result_file_path = result_files[0][3]
                data = {
                    "task_type": "front_end",
                    "task_subtype": "transform_path",
                    "path": result_file_path,
                    "web": True
                }
                new_path = vsb.pc_service(data, None)
                basedir, filename = os.path.split(new_path)
                return send_from_directory(basedir, filename, as_attachment=True)
        else:
            try:
                start_value = int(request.form['start_value'])
                end_value = int(request.form['end_value'])
                chosen_molecules = []
                if chosen_option in temporaly_results:
                    molecules = temporaly_results[chosen_option]
                    for i in range(start_value, end_value + 1):
                        chosen_molecules.append(molecules[i])
                    csv_path = get_path(533, "csv")
                    with open(csv_path, 'w', newline='') as csvfile:
                        molecule_writer = csv.writer(csvfile, delimiter=' ',
                                                     quotechar='|', quoting=csv.QUOTE_MINIMAL)
                        for molecule in chosen_molecules:
                            molecule_writer.writerow(molecule)
                    return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
            except:
                pass
    result_dict = {}
    # data-set
    data = {
        "task_type": "get",
        "task_subtype": "subtest",
        "id": subtest_id,
        "web": True
    }
    subtests, names = vsb.pc_service(data, None)
    if not len(subtests) == 1:
        return abort
    else:
        d = subtests[0][2]
    data = {
        "task_type": "get",
        "task_subtype": "dataset",
        "id_dataset": d,
        "web": True
    }
    b_value, dataset_info = vsb.pc_service(data, None)
    # molecules
    data = {
        "task_type": "get",
        "task_subtype": "molecule",
        "subtest_id": subtest_id,
        "web": True
    }
    molecules, mol_names = vsb.pc_service(data, None)
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    mols = []
    m_names = []
    count = len(molecules)
    if count > 0:
        import statistics
        for n in range(2, len(mol_names)):
            m_names.append(mol_names[n])
        m_names.append("Activity")
        sum = 0
        scores = []
        max_value = int(molecules[0][3])
        min_value = int(molecules[0][3])
        pam_number = len(molecules[0])
        for molecule in molecules:
            mol = []
            for i in range(2, pam_number):
                mol.append(molecule[i])
            if molecule[2] in dataset_info['active']:
                mol.append("1")
            else:
                mol.append("0")
            mols.append(mol)
            score = float(molecule[3])
            scores.append(score)
            sum += score
            if score < min_value:
                min_value = score
            if max_value < score:
                max_value = score
        result_dict['Maximum'] = max_value
        result_dict['Minimum'] = min_value
        result_dict['Median'] = statistics.median_low(scores)
        result_dict['Variance'] = statistics.variance(scores)
        result_dict['Mean'] = sum / count
    # analysis
    data = {
        "task_type": "get",
        "task_subtype": "analyze",
        "subtest_id": subtest_id,
        "web": True
    }
    analysis, names = vsb.pc_service(data, None)
    if len(analysis) > 0:
        analyze = analysis[0]
        for i in range(2, len(analyze)):
            result_dict[names[i]] = analyze[i]
    temporaly_results[chosen_option] = mols
    return render_template('subtest_analysis.html', subtest=result_dict, names=m_names, molecules=mols,
                           subtest_id=subtest_id, data_set_id=d)


# get pages
@app.route("/download_method/<id_value>.html", methods=['POST', 'GET'])
@login_required
def logged_download_method(id_value):
    global temporaly_results
    chosen_option = "method"
    error = None
    if request.method == 'GET' or request.method == "POST":
        # try read data from page
        data = {
            "task_type": "get",
            "task_subtype": "method",
            "id": id_value,
            "web": True
        }
        method = vsb.pc_service(data, None)
        if len(method) == 1:
            method_parts = method[0]
            f_file = None
            s_file = None
            if not method_parts[4].strip().lower() == "empty":
                f_file = True
            if not method_parts[5].strip().lower() == "empty":
                s_file = True
            if f_file is not None:
                data = {
                    "task_type": "get",
                    "task_subtype": "method",
                    "copy": True,
                    "method_id": method_parts[2],
                    "web": True
                }
                f_path = vsb.pc_service(data, None)
            if s_file is not None:
                data = {
                    "task_type": "get",
                    "task_subtype": "method",
                    "copy": True,
                    "method_id": method_parts[3],
                    "web": True
                }
                s_path = vsb.pc_service(data, None)
            if f_file is not None or s_file is not None:
                zip_path = get_path(25, "zip")
                with zipfile.ZipFile(zip_path, 'w') as myzip:
                    if method_parts[4] == method_parts[5]:
                        nm_f = method_parts[4] + "_l"
                        nm_s = method_parts[5] + "_s"
                    else:
                        nm_f = method_parts[4]
                        nm_s = method_parts[5]
                    if f_file is not None:
                        for root, dirs, files in os.walk(f_path):
                            pr = str(os.path.commonpath([root, f_path]))
                            add_path = str(root)[len(pr):].strip("/").strip("\\")
                            for file in files:
                                myzip.write(os.path.join(root, file), arcname=os.path.join(nm_f, add_path, file))
                    if s_file is not None:
                        for root, dirs, files in os.walk(s_path):
                            pr = str(os.path.commonpath([root, s_path]))
                            add_path = str(root)[len(pr):].strip("/").strip("\\")
                            for file in files:
                                myzip.write(os.path.join(root, file), arcname=os.path.join(nm_s, add_path, file))
                return send_from_directory(get_temp_path(), filename=os.path.basename(zip_path), as_attachment=True)


@app.route('/download_doc/')
def download_doc():
    return send_from_directory('static', 'documentation.pdf', as_attachment=True)


@app.route("/get_method.html", methods=['GET', 'POST'])
@login_required
def logged_get_method():
    global temporaly_results
    chosen_option = "method"
    error = None
    if request.method == 'POST':
        # try read data from page
        for key in request.form:
            if not str(key).find("csv_") == -1:
                if chosen_option in temporaly_results:
                    option = str(key).replace("csv_", "")
                    csv_path = get_path(0, "csv")
                    header = ["ID", "Method type", "Load function", "Score function"]
                    search_word = ""
                    # gens, loads, scores
                    if option == "loading":
                        ms = temporaly_results[chosen_option][1]
                        search_word = request.form["searchInput1"]
                    elif option == "scoring":
                        ms = temporaly_results[chosen_option][2]
                        search_word = request.form["searchInput2"]
                    else:
                        ms = temporaly_results[chosen_option][0]
                        search_word = request.form["searchInput3"]
                    with open(csv_path, "w", newline="\n") as csv_file:
                        c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                        c_writer.writerow(header)
                        for m in ms:
                            found = False
                            for mx in m:
                                if not str(mx).find(search_word) == -1:
                                    found = True
                                    break
                            if found:
                                c_writer.writerow(m)
                    return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
                else:
                    break
            if not str(key).find("gs_") == -1:
                index = int(str(key).replace("gs_", ""))
                if chosen_option in temporaly_results:
                    chosen_op = chosen_option + "_n_m"
                    return show_details(chosen_op, temporaly_results[chosen_option], index)
                else:
                    break
            if not str(key).find("dll_") == -1:
                index = int(str(key).replace("dll_", ""))
                if chosen_option in temporaly_results:
                    chosen_op = chosen_option + "_l"
                    return show_details(chosen_op, temporaly_results[chosen_option], index)
                else:
                    break
            if not str(key).find("dls_") == -1:
                index = int(str(key).replace("dls_", ""))
                if chosen_option in temporaly_results:
                    chosen_op = chosen_option + "_s"
                    return show_details(chosen_op, temporaly_results[chosen_option], index)
                else:
                    break
    methods = get_methods()  # gens, loads, scores
    temporaly_results[chosen_option] = methods
    return render_template('get_method.html', title="Methods", reference="/get_method.html", ERROR=error,
                           HEADERS_TABLE_GENERAL=["ID", "Method type", "Method name"],
                           HEADERS_TABLE_LOADS=["ID", "Method type", "Load function"],
                           HEADERS_TABLE_SCORES=["ID", "Method type", "Score function"],
                           RESULTS_TABLE_GENERAL=methods[0], RESULTS_TABLE_LOADS=methods[1],
                           RESULTS_TABLE_SCORES=methods[2])


@app.route("/get_selection.html", methods=['GET', 'POST'])
@login_required
def logged_get_selection():
    error = None
    if request.method == 'POST':
        if "download" == request.form['button']:
            data_set_id = request.form['data_set_id']
            selection_id = request.form['selection_id']
            data_path = get_path(0, "file")
            data = {
                "task_subtype": "selection",
                "task_type": "get",
                "id_dataset": data_set_id,
                "id_selection": selection_id,
                "output_file": data_path
            }
            vsb.pc_service(data, None)
            zip_path = get_path(0, "zip")
            with zipfile.ZipFile(zip_path, 'w') as myzip:
                myzip.write(data_path, arcname=os.path.basename(data_path))
            buttons = [["Zip file with data about selection", os.path.basename(zip_path), "selection_data.zip"]]
            return render_template("result_get_one.html", DOWNLOADS=buttons)
        elif "show" == request.form['button']:
            data_set_id = request.form['data_set_id']
            selection_id = request.form['selection_id']
            url = url_for("show_selection_description", data_set_id=data_set_id, selection_id=selection_id)
            return redirect(url)
    buttons = [["download", "Download"], ["show", "Show"]]
    return render_template('get_selection.html', ERROR=error, reference="/get_selection.html",
                           title="Get selection",
                           buttons=buttons, selections=get_selections(), DATASETS=get_data_sets())


@app.route("/get_data_set.html", methods=['GET', 'POST'])
@login_required
def logged_get_data_set():
    global data_set_added
    error = None
    chosen_option = "get_datasets"
    rows = []
    assays = get_data_sets()
    if request.method == 'POST':
        # try read data from page
        for key in request.form:
            if not str(key).find("csv_") == -1:
                if chosen_option in temporaly_results:
                    option = str(key).replace("csv_", "")
                    csv_path = get_path(0, "csv")
                    header = ["Name", "# active", "# inactive", "# selections"]
                    rows = temporaly_results[chosen_option]
                    search_word = request.form['searchInput']
                    with open(csv_path, "w", newline="\n") as csv_file:
                        c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                        c_writer.writerow(header)
                        for m in rows:
                            found = False
                            for mx in m:
                                if not str(mx).find(search_word) == -1:
                                    found = True
                                    break
                            if found:
                                c_writer.writerow(m)
                    return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
                else:
                    break
            if not str(key).find("bt_") == -1:
                index = int(str(key).replace("bt_", ""))
                if chosen_option in temporaly_results:
                    return show_details(chosen_option, temporaly_results[chosen_option], index)
                else:
                    break
    if not chosen_option in temporaly_results or data_set_added:
        data_set_added = False
        try:
            for assay in assays:
                data = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "dataset",
                    "metadata": True,
                    "id_dataset": assay
                }
                r_value, metadata = vsb.pc_service(data, None)
                if "meta" in metadata:
                    metadata = metadata['meta']
                data = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "dataset",
                    "selection_number": True,
                    "id_dataset": assay
                }
                r_value, sel_number = vsb.pc_service(data, None)
                if metadata is not None and "numberActives" in metadata:
                    number_actives = metadata['numberActives']
                else:
                    number_actives = "No information"
                if metadata is not None and "numberInactives" in metadata:
                    number_inactives = metadata['numberInactives']
                else:
                    number_inactives = "No information"
                if sel_number is None:
                    sel_number = "No information"
                row = [assay, number_actives, number_inactives, str(sel_number)]
                rows.append(row)
            temporaly_results[chosen_option] = rows
        except Exception as e:
            return render_template("information_page.html", text=str(e))
    header = ["Name", "# active", "# inactive", "# selections"]
    return render_template('get_main.html', title="Data-sets", ERROR=error, reference="/get_data_set.html",
                           HEADERS_TABLE=header, RESULTS_TABLE=temporaly_results[chosen_option])


@app.route("/get_test.html", methods=['GET', 'POST'])
@login_required
def logged_get_test():
    global temporaly_results
    chosen_option = "test"
    error = None
    if request.method == 'POST':
        # try read data from page
        for key in request.form:
            if not str(key).find("bt_") == -1:
                index = int(str(key).replace("bt_", ""))
                if chosen_option in temporaly_results:
                    return show_details(chosen_option, temporaly_results[chosen_option], index)
                else:
                    break
            if not str(key).find("meds_") == -1:
                index = int(str(key).replace("meds_", ""))
                if chosen_option in temporaly_results:
                    return show_details("method_fr", get_methods("tests"), index)
                else:
                    break
            if not str(key).find("csv_") == -1:
                if chosen_option in temporaly_results:
                    option = str(key).replace("csv_", "")
                    csv_path = get_path(0, "csv")
                    header = ["ID", "Method type", "Load function", "Score function"]
                    if option == "tests":
                        ms, header = get_tests()
                        search_word = request.form['searchInput1']
                    else:
                        ms = get_methods("tests")
                        search_word = request.form['searchInput2']
                    with open(csv_path, "w", newline="\n") as csv_file:
                        c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                        c_writer.writerow(header)
                        for m in ms:
                            found = False
                            for mx in m:
                                if not str(mx).find(search_word) == -1:
                                    found = True
                                    break
                            if found:
                                c_writer.writerow(m)
                    return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
                else:
                    break
    tests, names = get_tests()
    methods = get_methods("tests")
    temporaly_results[chosen_option] = tests
    return render_template('get_tests.html', ERROR=error, HEADERS_TABLE_TESTS=names, RESULTS_TABLE_TESTS=tests,
                           HEADERS_TABLE_METHODS=["ID", "Method type", "Load function", "Score function"],
                           RESULTS_TABLE_METHODS=methods)


# analysis
@app.route("/choose_selection.html", methods=['GET', 'POST'])
@login_required
def choose_selection():
    error = None
    if request.method == 'POST':
        data_set_id = request.form['data_set_id']
        selection_id = request.form['selection_id']
        if request.form["button"] == "selections":
            url = "/show_methods_{0}_{1}.html".format(data_set_id, selection_id)
        else:
            url = "/choose_methods_{0}_{1}.html".format(data_set_id, selection_id)
        return redirect(url)
    buttons = [["methods", "Compare methods on selection"], ["selections", "Compare selection on methods"]]
    return render_template('get_selection.html', ERROR=error, reference="/choose_selection.html",
                           title="Choose selection",
                           buttons=buttons,
                           selections=get_selections(), DATASETS=get_data_sets())


# analysis
@app.route("/show_export_analysis/", methods=['GET', 'POST'])
@login_required
def show_export_analysis():
    error = ""
    if request.method == 'POST':
        if "file" in request.files:
            file = request.files['file']
            file_path = get_path(0, "file")
            file.save(file_path)
            with open(file_path) as stream:
                inp = json.load(stream)
                action_type = inp['action']
                if action_type == "m_d_n_m":
                    rows = inp['rows']
                    analyze_names = inp['analyze_names']
                    method_names = inp['method_names']
                    datasets = inp['datasets']
                    temporaly_results[action_type] = [rows, analyze_names, method_names, -1, datasets]
                    methods_array = transform_for_graph(method_names, rows, analyze_names[0])
                    return render_template('method_dataset.html', ERROR=error, typ_option=["max", "min", 'avg'],
                                           method_names=method_names, analyze_names=analyze_names,
                                           analyze_results=rows, typ="max", an="AUC",
                                           datasets_names=inp['datasets_names'],
                                           methods_ids=inp['methods_ids'], methods=methods_array, datasets=datasets,
                                           amount=inp['number_molecules'],
                                           reference=url_for('show_export_analysis'), external=True)
                if action_type == "method_comparison":
                    data_set_selection_values = inp['data_set_selection_values']
                    analyze_names = inp['analyze_names']
                    mms_names = inp['mms_names']
                    methods = inp['methods']
                    max_analyze_names = inp['max_analyze_names']
                    return render_template("ds_s_methods.html", data_set_selection_values=data_set_selection_values,
                                           analyze_names=analyze_names, mms_names=mms_names, methods=methods,
                                           max_analyze_names=max_analyze_names)
                if action_type == "collapse_fingerprints" or action_type == "analysis" or \
                                action_type == "relative_scores" or action_type == "inactive_before_active" or \
                                action_type == "known_active_same_places" or action_type == "relative_order" or \
                                action_type == "leading_first_k":
                    clusters = inp['cluster']
                    temporaly_results['cluster'] = clusters
                    if action_type == "collapse_fingerprints":
                        title = "collapse fingerprints"
                    if action_type == "analysis":
                        title = "subtest analysis"
                    if action_type == "relative_scores":
                        title = "relative difference in scores of actives"
                    if action_type == "inactive_before_active":
                        title = "count of inactives before actives "
                    if action_type == "known_active_same_places":
                        title = "known active with same values on same places"
                    if action_type == "relative_order":
                        title = "relative order of active molecules"
                    if action_type == "leading_first_k":
                        k_value = inp['k_value']
                        title = "count of molecules which are leaded from known active in first " + str(k_value)
                    dataset_option = inp["dataset_option"]
                    if len(clusters) == 0:
                        return abort
                    else:
                        datasets = list(clusters.keys())
                        # create selection
                        selections = {}
                        for ds in datasets:
                            selections[ds] = list(clusters[ds].keys())
                        dataset = datasets[0]
                        if len(clusters[dataset]) == 0:
                            return abort
                        else:
                            sls = list(clusters[dataset].keys())
                            selection = sls[0]
                        if not dataset_option:
                            cluster = clusters[dataset][selection]
                            return render_template("collapse_fingerprints.html", chosen_dataset=dataset,
                                                   chosen_selection=selection, datasets=datasets, selections=selections,
                                                   data_tree=cluster, title=title, action_type=action_type)
                        else:
                            pattern = clusters[dataset][selection]
                            pattern_selections = pattern[0]
                            pattern_cluster = pattern[1]
                            data = {
                                "web": True,
                                "task_type": "get",
                                "task_subtype": "dataset",
                                "selection_number": True,
                                "id_dataset": dataset
                            }
                            r_value, number = vsb.pc_service(data, None)
                            selections_information = []
                            selections_information.append(["Selections: ", ", ".join(pattern_selections)])
                            selections_information.append(["Number of selection: ", str(len(pattern_selections))])
                            selections_information.append(
                                ["The relative portion", str(len(pattern_selections) / number)])
                            return render_template("dataset_cluster.html", chosen_dataset=dataset,
                                                   chosen_selection=selection, datasets=datasets, selections=selections,
                                                   data_tree=pattern_cluster, title=title, action_type=action_type,
                                                   selections_information=selections_information,
                                                   dataset_option=dataset_option)
                if action_type == "k_sames":
                    spliting = inp['sames']
                    k_value = inp['k_value']
                    temporaly_results['k_sames'] = spliting
                    if len(spliting) == 0:
                        return abort
                    else:
                        datasets = list(spliting.keys())
                        # create selection
                        selections = {}
                        for ds in datasets:
                            selections[ds] = list(spliting[ds].keys())
                        dataset = datasets[0]
                        if len(spliting[dataset]) == 0:
                            return abort
                        else:
                            sls = list(spliting[dataset].keys())
                            selection = sls[0]
                            sames = spliting[dataset][selection]
                            methods_names = list(sames.keys())
                            rows = []
                            for name in methods_names:
                                column = []
                                column.append(name)
                                methods_info = sames[name]
                                for name1 in methods_names:
                                    if name1 == name:
                                        column.append([])
                                        column.append([])
                                    else:
                                        column.append(methods_info[name1]['actives'])
                                        column.append(methods_info[name1]['inactives'])
                                rows.append(column)
                        return render_template("k_sames.html", rows=rows, method_names=methods_names, k_value=k_value,
                                               chosen_dataset=dataset, chosen_selection=selection, datasets=datasets,
                                               selections=selections, action_type=action_type)
                if action_type == "known_actives":
                    known_actives = inp["known_actives"]
                    k_value = int(inp['k_value'])
                    dataset_name = inp['dataset_name']
                    methods_names = []
                    for selection in known_actives:
                        active_molecules = known_actives[selection]
                        for active_molecule in active_molecules:
                            methods = active_molecules[active_molecule]
                            for method in methods:
                                methods_names.append(method)
                            break
                        break
                    temporaly_results[action_type] = known_actives
                    if len(known_actives) == 0:
                        return abort
                    else:
                        return render_template("kn_actives.html", k_value=k_value,
                                               dataset_name=dataset_name, method_names=methods_names,
                                               selections=known_actives, action_type=action_type)
        elif "change_content" in request.form:
            chosen_option = "m_d_n_m"
            an = request.form['an_option']
            typ = request.form['typ_option']
            if chosen_option in temporaly_results:
                rows = temporaly_results[chosen_option][0]
                analyze_names = temporaly_results[chosen_option][1]
                method_names = temporaly_results[chosen_option][2]
                datasets = temporaly_results[chosen_option][4]
                methods_array = transform_for_graph(method_names, rows, an)
                for index, item in enumerate(analyze_names):
                    if item == an:
                        break
                datasets_names = []
                methods_ids = []
                for key in request.form:
                    if str(key).startswith("dataset_"):
                        datasets_names.append(str(key).replace("dataset_", ""))
                    elif str(key).startswith("method_"):
                        methods_ids.append(int(str(key).replace("method_", "")))
                return render_template('method_dataset.html', ERROR=error, typ_option=["max", "min", 'avg'],
                                       method_names=method_names, analyze_names=analyze_names,
                                       analyze_results=rows, typ=typ, an=an, datasets_names=datasets_names,
                                       methods_ids=methods_ids, methods=methods_array, datasets=datasets,
                                       reference=url_for('show_export_analysis'), external=True)
        elif "cl_fps_an_change_content" in request.form:
            chosen_option = "cluster"
            data_set_id = request.form['data_set_id']
            s = request.form['selection_id']
            action_type = request.form['action_type']
            dataset_option = request.form['dataset_option']
            if action_type == "collapse_fingerprints":
                title = "collapse fingerprints"
            if action_type == "analysis":
                title = "subtest analysis"
            if action_type == "relative_scores":
                title = "relative scores"
            if action_type == "inactive_before_active":
                title = "count of inactive before i-th active"
            if action_type == "known_active_same_places":
                title = "known actives on same places in ordered score"
            if action_type == "relative_order":
                title = "relative order"
            if action_type == "leading_first_k":
                title = "number of molecules which are leading by known active in alphabetical order"
            if chosen_option in temporaly_results:
                clusters = temporaly_results['cluster']
                if len(clusters) == 0:
                    return abort
                else:
                    datasets = list(clusters.keys())
                    # create selection
                    selections = {}
                    for ds in datasets:
                        selections[ds] = list(clusters[ds].keys())
                    if not dataset_option:
                        cluster = clusters[data_set_id][s]
                        return render_template("collapse_fingerprints.html", chosen_dataset=data_set_id,
                                               chosen_selection=s, datasets=datasets, selections=selections,
                                               data_tree=cluster, title=title, action_type=action_type)
                    else:
                        pattern = clusters[data_set_id][s]
                        pattern_selections = pattern[0]
                        pattern_cluster = pattern[1]
                        data = {
                            "web": True,
                            "task_type": "get",
                            "task_subtype": "dataset",
                            "selection_number": True,
                            "id_dataset": data_set_id
                        }
                        r_value, number = vsb.pc_service(data, None)
                        selections_information = []
                        selections_information.append(["Selections: ", ", ".join(pattern_selections)])
                        selections_information.append(["Number of selection: ", str(len(pattern_selections))])
                        selections_information.append(["The relative portion", str(len(pattern_selections) / number)])
                        return render_template("dataset_cluster.html", chosen_dataset=data_set_id,
                                               chosen_selection=s, datasets=datasets, selections=selections,
                                               data_tree=pattern_cluster, title=title, action_type=action_type,
                                               selections_information=selections_information,
                                               dataset_option=dataset_option)
        elif "k_sames_change_content" in request.form:
            spliting = temporaly_results['k_sames']
            data_set_id = request.form['data_set_id']
            s = request.form['selection_id']
            k_value = request.form['k_value']
            action_type = request.form['action_type']
            if len(spliting) == 0:
                return abort
            else:
                datasets = list(spliting.keys())
                # create selection
                selections = {}
                for ds in datasets:
                    selections[ds] = list(spliting[ds].keys())
                dataset = datasets[0]
                if len(spliting[dataset]) == 0:
                    return abort
                else:
                    sls = list(spliting[dataset].keys())
                    sames = spliting[data_set_id][s]
                    methods_names = list(sames.keys())
                    rows = []
                    for name in methods_names:
                        column = []
                        column.append(name)
                        methods_info = sames[name]
                        for name1 in methods_names:
                            if name1 == name:
                                column.append([])
                                column.append([])
                            else:
                                column.append(methods_info[name1]['actives'])
                                column.append(methods_info[name1]['inactives'])
                        rows.append(column)
                return render_template("k_sames.html", rows=rows, method_names=methods_names, k_value=k_value,
                                       chosen_dataset=data_set_id, chosen_selection=s, datasets=datasets,
                                       selections=selections, action_type=action_type)
    return render_template('show_export_analysis.html')


@app.route("/method_dataset/datasets~<datasets_names>/methods~<methods_ids>", methods=['GET', 'POST'])
@login_required
def method_dataset(datasets_names, methods_ids):
    global temporaly_results
    error = None
    m_ids = methods_ids
    a_names = datasets_names
    methods_ids = str(methods_ids).split("#")
    ids = []
    for method in methods_ids:
        ids.append(int(method))
    datasets_names = str(datasets_names).split("#")
    chosen_option = "method_dataset"
    if request.method == 'POST':
        an = request.form['an_option']
        typ = request.form['typ_option']
        amount = request.form['inputMolAmount']
        am_int = None
        try:
            am_int = int(amount)
        except:
            pass
        if amount == "" or am_int is None:
            am_int = -1
        if "export_analysis" in request.form:
            t_path = get_path(0, "file")
            with open(t_path, "w") as stream:
                json.dump({
                    "action": "m_d_n_m",
                    "methods_ids": methods_ids,
                    "datasets_names": datasets_names,
                    "number_molecules": am_int
                }, stream, indent=2)
            return send_from_directory(get_temp_path(), os.path.basename(t_path), as_attachment=True)
        if "change_content" in request.form:
            if chosen_option in temporaly_results:
                rows = temporaly_results[chosen_option][0]
                analyze_names = temporaly_results[chosen_option][1]
                method_names = temporaly_results[chosen_option][2]
                datasets = temporaly_results[chosen_option][4]
                methods_array = transform_for_graph(method_names, rows, an)
                for index, item in enumerate(analyze_names):
                    if item == an:
                        break
                return render_template('method_dataset.html', ERROR=error, typ_option=["max", "min", 'avg'],
                                       method_names=method_names, analyze_names=analyze_names,
                                       analyze_results=rows, typ=typ, an=an, datasets_names=a_names,
                                       methods_ids=m_ids, methods=methods_array, datasets=datasets, amount=am_int,
                                       reference=url_for('method_dataset', datasets_names=a_names, methods_ids=m_ids),
                                       external=False)
        if "csv_things" in request.form:
            if chosen_option in temporaly_results:
                rows = temporaly_results[chosen_option][0]
                analyze_names = temporaly_results[chosen_option][1]
                for index, item in enumerate(analyze_names):
                    if item == an:
                        break
                method_names = temporaly_results[chosen_option][2]
                csv_path = get_path(0, "csv")
                with open(csv_path, "w", newline="\n") as csv_file:
                    c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                    c_writer.writerow(['Data-set name'] + method_names)
                    for m in rows:
                        row = [m[0]]
                        for i in range(1, len(m)):
                            row += [m[i][analyze_names[index]][typ]]
                        c_writer.writerow(row)
                return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
    # calculate all requested
    method_names, analyze_names, rows, datasets = calculate_analysis_1(methods_ids, datasets_names)
    methods_array = transform_for_graph(method_names, rows, analyze_names[0])
    if not len(analyze_names) == 0:
        temporaly_results[chosen_option] = [rows, analyze_names, method_names, -1, datasets]
        return render_template('method_dataset.html', ERROR=error, typ_option=["max", "min", 'avg'],
                               method_names=method_names, analyze_names=analyze_names,
                               analyze_results=rows, typ="max", an="AUC", datasets_names=a_names,
                               methods_ids=m_ids, methods=methods_array, datasets=datasets, amount=-1,
                               reference=url_for('method_dataset', datasets_names=a_names, methods_ids=m_ids),
                               external=False)
    return render_template('method_dataset.html', ERROR=error, typ_option=["max", "min", 'avg'],
                           method_names=method_names, analyze_names=analyze_names,
                           analyze_results=rows, typ="max", an="AUC", datasets_names=a_names,
                           methods_ids=m_ids, methods=methods_array, datasets=datasets, amount=-1,
                           reference=url_for('method_dataset', datasets_names=a_names, methods_ids=m_ids),
                           external=False)


def calculate_analysis_1(methods_ids, datasets_names):
    """
    Calculates analysis.

    :param methods_ids: IDS of methods.
    :param datasets_names: Names of data-sets.
    :return: method_names, analyze_names, rows, datasets
    """
    method_names = []
    method_informations = {}
    rows = []
    datasets = []
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "analyze",
        "id": 1
    }
    _, analyze_names = vsb.pc_service(data, None)
    analyze_names = analyze_names[2:]
    for i in methods_ids:
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "method",
            "methods_analysis": True,
            "method_id": i
        }
        met_infos, met_names = vsb.pc_service(data, None)
        if len(met_infos) == 0:
            continue
        for met_info in met_infos:
            if met_info[2] == "general":
                method_name = met_info[3]
            else:
                method_name = met_info[3] + "/" + met_info[4]
            if method_name not in method_names:
                method_names.append(method_name)
            ds = met_info[5]
            if ds in datasets_names:
                if ds not in method_informations:
                    method_informations[ds] = {method_name: [met_info]}
                else:
                    if method_name not in method_informations[ds]:
                        method_informations[ds][method_name] = [met_info]
                    else:
                        method_informations[ds][method_name].append(met_info)
    if not len(method_informations) == 0:
        for dataset in method_informations:  # add row
            columns = []
            for name in method_names:  # add column
                if name in method_informations[dataset]:
                    method_infos = method_informations[dataset][name]
                    count = len(method_infos)
                    max_array = [-1 for _ in range(7, len(met_names))]
                    min_array = [10000000000000000000000 for _ in range(7, len(met_names))]
                    avg_array = [0 for _ in range(7, len(met_names))]
                    for info in method_infos:  # count cell value
                        for i in range(7, len(met_names)):
                            avg_array[i - 7] += info[i]
                            if max_array[i - 7] < info[i]:
                                max_array[i - 7] = info[i]
                            if info[i] < min_array[i - 7]:
                                min_array[i - 7] = info[i]
                    cell = {}
                    for i in range(7, len(met_names)):
                        avg_array[i - 7] /= count
                        cell[met_names[i]] = {"max": max_array[i - 7], "min": min_array[i - 7], "avg": avg_array[i - 7]}
                else:
                    cell = {}
                    for i in range(7, len(met_names)):
                        cell[met_names[i]] = {"max": 0, "min": 0, "avg": 0}
                columns.append(cell)
            rows.append([dataset] + columns)
        for key in method_informations:
            datasets.append(key)
    return method_names, analyze_names, rows, datasets


def transform_for_graph(method_names, rows, analyze_name):
    """
    Transform data for graph.

    :param method_names: Methods names.
    :param rows: Rows with results
    :param analyze_names: Names of analysis metrics.
    :return:
    """
    methods_array = []
    for i, m in enumerate(method_names):
        values = []
        for dataset in rows:
            values.append([dataset[0], dataset[i + 1][analyze_name]])
        methods_array.append({
            "name": m,
            "values": values
        })
    return methods_array


@app.route("/choose_methods_<data_set_id>_<selection_id>.html", methods=['GET', 'POST'])
@login_required
def choose_methods(data_set_id, selection_id):
    chosen_option = "choose_methods"
    error = None
    if request.method == 'POST':
        index = []
        for key in request.form:
            if not str(key).find("checkbox_") == -1:
                index.append(int(str(key).replace("checkbox_", "")))
        if chosen_option in temporaly_results and len(index) > 0:
            return show_details(chosen_option, temporaly_results[chosen_option], index, dataset_id=data_set_id,
                                selection_id=selection_id)
    data = {
        "task_subtype": "method",
        "task_type": "get",
        "data_set_id": data_set_id,
        "selection_id": selection_id,
        "choose_methods": True,
        "web": True
    }
    methods, names = vsb.pc_service(data, None)
    url = "/choose_methods_{0}_{1}.html".format(data_set_id, selection_id)
    temporaly_results[chosen_option] = methods
    return render_template("choose_methods.html", HEADERS_TABLE=names, RESULTS_TABLE=methods, error=error,
                           reference=url)


@app.route("/external_analysis/", methods=['GET', 'POST'])
@login_required
def some_analysis():
    chosen_option = "some_analysis"
    error = None
    if request.method == 'POST':
        index = []
        for key in request.form:
            if not str(key).find("checkbox_") == -1:
                index.append(int(str(key).replace("checkbox_", "")))
        if chosen_option in temporaly_results and len(index) > 0:
            choice = request.form['analysisType']
            input_dict = {}
            if choice == "collapse_fingerprints":
                input_dict['window_size'] = request.form['window_size']
                input_dict['windows_count'] = request.form['windows_count']
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "k_sames":
                input_dict['k_value'] = request.form['k_value']
            elif choice == "method_comparison":
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "relative_scores":
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "inactive_before_active":
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "known_active_same_places":
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "analysis":
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "relative_order":
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            elif choice == "leading_first_k":
                input_dict['k_value'] = request.form['k_value']
                if "dataset_option" in request.form:
                    dataset_option = True
                else:
                    dataset_option = False
                input_dict["dataset_option"] = dataset_option
            else:
                input_dict['k_value'] = request.form['k_value']
                input_dict['dataset_name'] = request.form['data_set_id']
            input_dict['action'] = choice
            indexes = []
            for i in index:
                indexes.append(int(temporaly_results[chosen_option][i][0]))
            input_dict['ids'] = indexes
            file_path = get_path(0, "file")
            with open(file_path, "w") as stream:
                json.dump(input_dict, stream, indent=2)
            return send_from_directory(get_temp_path(), os.path.basename(os.path.basename(file_path)),
                                       as_attachment=True)
    methods = get_methods("tests")
    temporaly_results[chosen_option] = methods
    return render_template("some_analysis.html",
                           HEADERS_TABLE=["ID", "Method type", "Load function", "Score function"],
                           RESULTS_TABLE=methods, error=error,
                           datasets=get_data_sets())


@app.route("/choose_datasets.html", methods=['GET', 'POST'])
@login_required
def choose_datasets():
    error = None
    chosen_option = "get_datasets"
    rows = []
    if request.method == 'POST':
        # try read data from page
        indexes = []
        for key in request.form:
            if not str(key).find("bt_") == -1:
                indexes.append(int(str(key).replace("bt_", "")))
        if chosen_option in temporaly_results:
            assays = temporaly_results[chosen_option]
            chosen_assays = []
            for i in range(0, len(assays)):
                if i in indexes:
                    chosen_assays.append(assays[i][0])
            temporaly_results['chosen_assays'] = chosen_assays
            return redirect(url_for('choose_specific_methods'))
    assays = get_data_sets()
    for assay in assays:
        data = {
            "web": True,
            "task_type": "get",
            "subtask_type": "dataset",
            "metadata": True,
            "id_dataset": assay
        }
        metadata = vsb.pc_service(data, None)
        data = {
            "web": True,
            "task_type": "get",
            "subtask_type": "dataset",
            "selection_number": True,
            "id_dataset": assay
        }
        sel_number = vsb.pc_service(data, None)
        if metadata is not None and "numberActives" in metadata:
            number_actives = metadata['numberActives']
        else:
            number_actives = "No information"
        if metadata is not None and "numberInactives" in metadata:
            number_inactives = metadata['numberInactives']
        else:
            number_inactives = "No information"
        if sel_number is None:
            sel_number = "No information"
        row = [assay, number_actives, number_inactives, str(sel_number)]
        rows.append(row)
    header = ["Name", "# active", "# inactive", "# selections"]
    temporaly_results[chosen_option] = rows
    return render_template('choose_datasets.html', ERROR=error, reference=url_for('choose_datasets'),
                           HEADERS_TABLE=header, title="Choose data-sets:", button_name="Next", RESULTS_TABLE=rows)


@app.route("/choose_tests.html", methods=['GET', 'POST'])
@login_required
def choose_tests():
    error = None
    chosen_option = "choose_tests"
    if request.method == 'POST':
        # try read data from page
        indexes = []
        for key in request.form:
            if not str(key).find("bt_") == -1:
                indexes.append(int(str(key).replace("bt_", "")))
        tests, _ = get_tests()
        analysis = []
        for i in range(0, len(tests)):
            if i in indexes:
                test_order = tests[i][0]
                data = {
                    "web": True,
                    "test_id": test_order,
                    "task_type": "get",
                    "task_subtype": "subtest"
                }
                res, res_names = vsb.pc_service(data, None)
                for r in res:
                    subtest_id = r[0]
                    data = {
                        "web": True,
                        "subtest_id": subtest_id,
                        "task_type": "get",
                        "task_subtype": "analyze"
                    }
                    ans, ans_names = vsb.pc_service(data, None)
                    if ans is not None and len(ans) > 0:
                        analysis.append(list(r) + list(ans[0]))
        if len(analysis) > 0:
            csv_path = get_path(0, "csv")
            header = list(res_names) + list(ans_names)
            ms = analysis
            with open(csv_path, "w", newline="\n") as csv_file:
                c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                c_writer.writerow(header)
                for m in ms:
                    c_writer.writerow(m)
            return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
    tests, names = get_tests()
    return render_template('choose_datasets.html', ERROR=error, reference=url_for('choose_tests'),
                           HEADERS_TABLE=names, title="Choose tests:", button_name="Export chosen", RESULTS_TABLE=tests)


@app.route("/choose_specific_methods.html", methods=['GET', 'POST'])
@login_required
def choose_specific_methods():
    chosen_option = "choose_methods_n"
    error = None
    if request.method == 'POST':
        index = []
        for key in request.form:
            if not str(key).find("checkbox_") == -1:
                index.append(int(str(key).replace("checkbox_", "")))
        if chosen_option in temporaly_results and len(index) > 0:
            if "chosen_assays" in temporaly_results:
                s_assays = ""
                for a in temporaly_results['chosen_assays']:
                    s_assays += a + "#"
                s_assays = s_assays[:len(s_assays) - 1]
                s_methods = ""
                methods = get_methods("tests")
                for m in range(0, len(methods)):
                    if m in index:
                        s_methods += str(methods[m][0]) + "#"
                s_methods = s_methods[:len(s_methods) - 1]
                # datasets_names, methods_ids
                return redirect(url_for('method_dataset', datasets_names=s_assays, methods_ids=s_methods))

    methods = get_methods("tests")
    url = url_for('choose_specific_methods')
    temporaly_results[chosen_option] = methods
    return render_template("choose_methods.html",
                           HEADERS_TABLE=["ID", "Method type", "Load function", "Score function"],
                           RESULTS_TABLE=methods, error=error,
                           reference=url)


@app.route("/choose_n_selections.html", methods=['GET', 'POST'])
@login_required
def choose_n_selections():
    chosen_option = "choose_n_selections"
    error = None
    if request.method == 'POST':
        d_s = reading_ds(request.form)
        return show_details(chosen_option, d_s, None)
    return render_template("choose_n_selections.html", selections=get_selections())


@app.route("/show_methods_<data_set_id>_<selection_id>.html", methods=['GET', 'POST'])
@login_required
def show_methods_on_selection(data_set_id, selection_id):
    chosen_option = "show_methods_on_selection"
    error = None
    if request.method == 'POST':
        return "OK"
    data = {
        "task_subtype": "method",
        "task_type": "get",
        "data_set_id": data_set_id,
        "selection_id": selection_id,
        "methods_analysis": True,
        "web": True
    }
    methods, names = vsb.pc_service(data, None)
    url = "/show_methods_{0}_{1}.html".format(data_set_id, selection_id)
    temporaly_results[chosen_option] = methods
    return render_template("selection_comparison.html", HEADERS_TABLE=names, RESULTS_TABLE=methods, error=error,
                           reference=url, data_set_id=data_set_id, selection_id=selection_id)


@app.route("/selection_description_<data_set_id>_<selection_id>.html", methods=['GET', 'POST'])
@login_required
def show_selection_description(data_set_id, selection_id):
    error = None
    if request.method == 'POST':
        if "download" in request.form:
            data_set_id = request.form['data_set_id']
            selection_id = request.form['selection_id']
            data_path = get_path(0, "file")
            data = {
                "task_subtype": "selection",
                "task_type": "get",
                "id_dataset": data_set_id,
                "id_selection": selection_id,
                "output_file": data_path
            }
            vsb.pc_service(data, None)
            zip_path = get_path(0, "zip")
            with zipfile.ZipFile(zip_path, 'w') as myzip:
                myzip.write(data_path, arcname=os.path.basename(data_path))
            buttons = [["Zip file with data about selection", os.path.basename(zip_path), "selection_data.zip"]]
            return render_template("result_get_one.html", DOWNLOADS=buttons)
        else:
            for key in request.form:
                # chose method
                if not str(key).find("med_") == -1:
                    index = int(str(key).replace("med_", ""))
                    chosen_option = "method_n"
                    if chosen_option in temporaly_results:
                        return show_details(chosen_option, temporaly_results[chosen_option], index)
                    else:
                        break
                elif not str(key).find("dat_") == -1:
                    index = int(str(key).replace("dat_", ""))
                    chosen_option = "method_n"
                    if chosen_option in temporaly_results:
                        return show_details("method_details", temporaly_results[chosen_option], index)
                    else:
                        break
                elif not str(key).find("csv_") == -1:
                    if "method_n" in temporaly_results:
                        option = str(key).replace("csv_", "")
                        csv_path = get_path(0, "csv")
                        if option == "methods":
                            header = ["Subtest id", "Method id", "Method type", "Load function / Method",
                                      "Score function"]
                            ms = temporaly_results["method_n"]
                        with open(csv_path, "w", newline="\n") as csv_file:
                            c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                            c_writer.writerow(header)
                            for m in ms:
                                c_writer.writerow(m)
                        return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
                    else:
                        break
    data = {
        "task_subtype": "selection",
        "task_type": "get",
        "id_dataset": data_set_id,
        "id_selection": selection_id,
        "web": True
    }
    d = vsb.pc_service(data, None)
    # metadata
    metadata = []
    for meta in d['meta']:
        metadata.append([meta, d['meta'][meta]])
    data = {
        "task_subtype": "method",
        "task_type": "get",
        "data_set_id": data_set_id,
        "choose_methods": True,
        "selection_id": selection_id,
        "web": True
    }
    methods, names = vsb.pc_service(data, None)
    temporaly_results['method_n'] = methods
    an_res = None
    count = 0
    an_dict = {}
    an = None
    # analysis
    for met in methods:
        data = {
            "task_type": "get",
            "task_subtype": "analyze",
            "subtest_id": met[0],
            "web": True
        }
        ad, an = vsb.pc_service(data, None)
        if ad is not None and len(ad) > 0:
            if an_res is None:
                an_res = []
                for ts in range(2, len(ad[0])):
                    an_res.append(0)
            count += 1
            for ts in range(2, len(ad[0])):
                an_res[ts - 2] += ad[0][ts]
    if an is not None:
        for ts in range(2, len(an)):
            an_dict["Average " + an[ts]] = an_res[ts - 2] / count
    else:
        an_dict["Information"] = "No data to process"
    return render_template("selection_description.html", train_active=d["train"]['active'], error=error,
                           train_inactive=d["train"]['inactive'], validation_active=d["valid"]['active'],
                           validation_inactive=d["valid"]['inactive'], test=d["test"], selection_id=selection_id,
                           data_set_id=data_set_id, metadata=metadata, HEADERS_TABLE_MED=names,
                           RESULTS_TABLE_MED=methods, ds_details=an_dict)


md_translation = {
    "numberInactives": "Number of inactives",
    "date": "Date of creation",
    "datasetName": "Name of data-set",
    "numberActives": "Number of actives",
    "targetName": "Target molecule",
    "datasetID": "ID of data-set"
}


@app.route("/data_set_description_<data_set_id>.html", methods=['GET', 'POST'])
@login_required
def show_data_set_description(data_set_id):
    global temporaly_results
    error = None
    if request.method == 'POST':
        if "sdf" in request.form:
            sdf_path = get_path(data_set_id, "sdf")
            data = {
                "task_subtype": "dataset",
                "task_type": "get",
                "id_dataset": data_set_id,
                "sdf_file": True,
                "output_file": sdf_path
            }
            vsb.pc_service(data, None)
            return send_from_directory(get_temp_path(), os.path.basename(sdf_path), as_attachment=True)
        elif "info" in request.form:
            data_path = get_path(data_set_id, "file")
            data = {
                "task_subtype": "dataset",
                "task_type": "get",
                "id_dataset": data_set_id,
                "output_file": data_path
            }
            vsb.pc_service(data, None)
            return send_from_directory(get_temp_path(), os.path.basename(data_path), as_attachment=True)
        elif "add" in request.form:
            with open("data/import_selection_example.json", "r") as stream:
                example = stream.read()
            return render_template('put_selection.html', ERROR=error, EXAMPLE=example, data_set_id=data_set_id)
        else:
            for key in request.form:
                # chose method
                if not str(key).find("med_") == -1:
                    index = int(str(key).replace("med_", ""))
                    chosen_option = "method_n"
                    if chosen_option in temporaly_results:
                        return show_details(chosen_option, temporaly_results[chosen_option], index)
                    else:
                        break
                # chose selection
                elif not str(key).find("sel_") == -1:
                    index = int(str(key).replace("sel_", ""))
                    chosen_option = "data_set_selection"
                    if chosen_option in temporaly_results:
                        result = temporaly_results[chosen_option][index]
                        selection_name = result[0]
                        return redirect(
                            url_for('show_selection_description', data_set_id=data_set_id, selection_id=selection_name))
                elif not str(key).find("dat_") == -1:
                    index = int(str(key).replace("dat_", ""))
                    chosen_option = "method_n"
                    if chosen_option in temporaly_results:
                        return show_details("method_details", temporaly_results[chosen_option], index)
                    else:
                        break
                elif not str(key).find("csv_") == -1:
                    if "data_set_selection" in temporaly_results and "method_n" in temporaly_results:
                        option = str(key).replace("csv_", "")
                        csv_path = get_path(0, "csv")
                        if option == "selections":
                            header = ["ID", "Type", "Loading function/ General method", "Score method"]
                            ms = temporaly_results["data_set_selection"]
                        elif option == "methods":
                            header = ["Subtest id", "Method id", "Method type", "Load function / Method",
                                      "Score function"]
                            ms = temporaly_results["method_n"]
                        with open(csv_path, "w", newline="\n") as csv_file:
                            c_writer = csv.writer(csv_file, delimiter=";", quotechar="|", quoting=csv.QUOTE_MINIMAL)
                            c_writer.writerow(header)
                            for m in ms:
                                c_writer.writerow(m)
                        return send_from_directory(get_temp_path(), os.path.basename(csv_path), as_attachment=True)
                    else:
                        break
    data = {
        "task_subtype": "dataset",
        "task_type": "get",
        "id_dataset": data_set_id,
        "web": True
    }
    value, d = vsb.pc_service(data, None)
    # metadata
    metadata_dataset = []
    for meta in d['meta']:
        if meta in md_translation:
            metadata_dataset.append([md_translation[meta], d['meta'][meta]])
        else:
            metadata_dataset.append([meta, d['meta'][meta]])
    # get methods
    data = {
        "task_subtype": "method",
        "task_type": "get",
        "data_set_id": data_set_id,
        "choose_methods": True,
        "web": True
    }
    methods, names = vsb.pc_service(data, None)
    temporaly_results['method_n'] = methods
    # get selections
    selections = get_selections()
    found = False
    for selection in selections:
        if selection[0] == data_set_id:
            selections = selection[1]
            found = True
    if not found:
        selections = []
    sels = []
    for selection in selections:
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "selection",
            "metadata": True,
            "id_dataset": data_set_id,
            "id_selection": selection
        }
        r_value, metadata = vsb.pc_service(data, None)
        if "meta" in metadata:
            metadata = metadata['meta']
        if metadata is not None and "train_active_number" in metadata:
            train_active_number = metadata['train_active_number']
        else:
            train_active_number = "No information"
        if metadata is not None and "train_inactive_number" in metadata:
            train_inactive_number = metadata['train_inactive_number']
        else:
            train_inactive_number = "No information"
        if metadata is not None and "validation_active_number" in metadata:
            validation_active_number = metadata['validation_active_number']
        else:
            validation_active_number = "No information"
        if metadata is not None and "validation_inactive_number" in metadata:
            validation_inactive_number = metadata['validation_inactive_number']
        else:
            validation_inactive_number = "No information"
        if metadata is not None and "test_number" in metadata:
            test_number = metadata['test_number']
        else:
            test_number = "No information"
        sels.append([selection, train_active_number, train_inactive_number, validation_active_number,
                     validation_inactive_number, test_number])
    header_sels = ["Name", "# Train active", "# Train inactive", "# Validation active", "# Validation inactive",
                   "# test"]
    temporaly_results['data_set_selection'] = sels
    an_res = None
    count = 0
    an_dict = {}
    an = None
    # analysis
    for met in methods:
        data = {
            "task_type": "get",
            "task_subtype": "analyze",
            "subtest_id": met[0],
            "web": True
        }
        ad, an = vsb.pc_service(data, None)
        if ad is not None and len(ad) > 0:
            if an_res is None:
                an_res = []
                for ts in range(2, len(ad[0])):
                    an_res.append(0)
            count += 1
            for ts in range(2, len(ad[0])):
                an_res[ts - 2] += ad[0][ts]
    if an is not None:
        for ts in range(2, len(an)):
            an_dict["Average " + an[ts]] = an_res[ts - 2] / count
    else:
        an_dict["Information"] = "No data to process"
    return render_template("data_set_description.html", active=d['active'], error=error, metadata=metadata_dataset,
                           inactive=d['inactive'], data_set_id=data_set_id,
                           HEADERS_TABLE_SEL=header_sels,
                           RESULTS_TABLE_SEL=sels, HEADERS_TABLE_MED=names, RESULTS_TABLE_MED=methods,
                           ds_details=an_dict)


# results
@app.route("/query_done.html", methods=['GET', 'POST'])
@login_required
def query_done_site():
    error = None
    if request.method == 'POST':
        # try read data from page
        id = request.form['id']
        id = str(id).strip()
        value, answer, _ = is_end_of_task(id)
        if value:
            result_file = get_task_information(id)["output_file"]
            try:
                return redirect_to_result_page(id, result_file)
            except IOError as ioe:
                error = "File not found, task not found"
            except Exception as e:
                print(e)
                raise e
        else:
            error = answer
    tests = []
    names = ["ID", "Type", "Date", "Taskstatus"]
    for id_value in actual_tasks:
        infos = get_task_information(id_value)
        value, answer, _ = is_end_of_task(id_value)
        tests.append([id_value, infos["type"], infos["date"], answer])
    return render_template('query_done.html', ERROR=error, names=names, tests=tests)


@app.route("/test_basic_info_<test_id>.html", methods=['GET', 'POST'])
@login_required
def test_basic_info(test_id):
    error = None
    # try read data from page
    value, answer, _ = is_end_of_task(test_id)
    if value:
        result_file = get_task_information(test_id)["output_file"]
        try:
            return redirect_to_result_page(test_id, result_file)
        except IOError as ioe:
            error = "File not found, task not found"
        except Exception as e:
            print(e)
            raise e
    else:
        error = answer
    tests = []
    names = ["ID", "Type", "Date", "Status", "Info"]
    for id_value in actual_tasks:
        infos = get_task_information(id_value)
        value, answer, _ = is_end_of_task(id_value)
        tests.append([id_value, infos["type"], infos["date"], value, answer])
    return render_template('query_done.html', ERROR=error, names=names, tests=tests)


@app.route('/download/<file_name>', methods=['POST'])
@login_required
def download_file(file_name):
    return send_from_directory(get_temp_path(), file_name, as_attachment=True)


@app.route('/download/<file_name>', methods=['POST'])
@login_required
def support_download(file_name):
    return send_from_directory(get_temp_path(), filename=file_name, as_attachment=True)


# helpers
@app.route("/documentation.html", methods=['GET'])
def documantation():
    error = None
    if "logged_in" in session and session['logged_in'] == True:
        logged = "yes"
    else:
        logged = "no"
    return render_template('documentation.html', ERROR=error, LIBRARIES=LIBRARIES, LOGGED=logged)


from flask import jsonify


# rest
@app.route("/vsb/v1.0/api", methods=['GET'])
def get_api():
    pass


@app.route("/vsb/v1.0/method/id~<m_id>", methods=['DELETE'])
def delete_method(m_id):
    data = {
        "web": True,
        "task_type": "delete",
        "task_subtype": "method",
        "method_id": m_id
    }
    r_value = vsb.pc_service(data, None)
    return jsonify({"success": r_value}), 201


@app.route("/vsb/v1.0/ds/id~<ds_id>", methods=['DELETE'])
def delete_dataset(ds_id):
    data = {
        "web": True,
        "task_type": "delete",
        "task_subtype": "dataset",
        "dataset_name": ds_id
    }
    r_value = vsb.pc_service(data, None)
    return jsonify({"success": r_value}), 201


@app.route("/vsb/v1.0/s/sid~<sid>/name~<name>", methods=['DELETE'])
def delete_selection(sid, name):
    data = {
        "web": True,
        "task_type": "delete",
        "task_subtype": "selection",
        "selection_id": sid,
        "data_set_name": name
    }
    r_value = vsb.pc_service(data, None)
    return jsonify({"success": r_value}), 201


@app.route("/vsb/v1.0/test/id~<t_id>", methods=['DELETE'])
def delete_test(t_id):
    data = {
        "web": True,
        "task_type": "delete",
        "task_subtype": "test",
        "test_id": t_id
    }
    r_value = vsb.pc_service(data, None)
    return jsonify({"success": r_value}), 201


@app.route("/vsb/v1.0/method/", methods=['POST'])
@json_required
@files_required
def post_method():
    json_data = request.get_json()
    # type py/g
    type = json_data['type']
    subtype = None
    if type == "py":
        # py - type LF/SF
        subtype = json_data['subtype']
    # method file
    method = request.files['method']
    if method:
        filename = werkzeug.secure_filename(method.filename)
        method.save(os.path.join(get_temp_path(), filename))
    else:
        return jsonify({"error": "method key wans't found"}), 404
    method_zip_path = os.path.join(get_temp_path(), filename)
    # unzip file
    with zipfile.ZipFile(method_zip_path) as myzip:
        i = 0
        for file in myzip.namelist():
            splited = os.path.split(file)
            dir_part = splited[0]
            file_part = splited[1]
            splited_two = os.path.split(dir_part)
            dir_part_two = splited_two[0]
            file_part_two = splited_two[1]
            if not (not dir_part == "" and not file_part == "") and not (
                        not dir_part_two == "" and not file_part_two == ""):
                i += 1
        if not i == 1:
            return jsonify({"error": "More members in Zip file"}), 404
        info = myzip.infolist()[0]
        myzip.extractall(path=get_temp_path())
        method_path = os.path.abspath(os.path.join(get_temp_path(), info.filename))
    data = {
        "web": True,
        "task_type": "put",
        "task_subtype": "method",
        "method_type": type,
        "file_path": method_path
    }
    if subtype is not None:
        data["py_m_type_option"] = subtype
    id = vsb.pc_service(data, None)
    return jsonify({"id": id}), 201


@app.route("/vsb/v1.0/ds/", methods=['POST'])
@json_required
@files_required
def post_data_set():
    # save SDF file
    sdf = request.files['sdf']
    if sdf:
        sdf_filename = werkzeug.secure_filename(sdf.filename)
        sdf.save(os.path.join(get_temp_path(), sdf_filename))
    else:
        return jsonify({"error": "sdf key wans't found"}), 404
    # save info.json
    info = request.files['info']
    if info:
        info_filename = werkzeug.secure_filename(info.filename)
        info.save(os.path.join(get_temp_path(), info_filename))
    else:
        return jsonify({"error": "info key wans't found"}), 404
    data = {
        "web": True,
        "task_type": "put",
        "task_subtype": "dataset",
        "sdf_file": os.path.join(get_temp_path(), sdf_filename),
        "data_file": os.path.join(get_temp_path(), info_filename)
    }
    id = vsb.pc_service(data, None)
    return jsonify({"id": id}), 201


@app.route("/vsb/v1.0/s/id~<ds_id>", methods=['POST'])
@files_required
def post_selection(ds_id):
    # save selection file
    info = request.files['info']
    if info:
        filename = werkzeug.secure_filename(info.filename)
        info.save(os.path.join(get_temp_path(), filename))
    else:
        return jsonify({"error": "info key wans't found"}), 404
    data = {
        "web": True,
        "task_type": "put",
        "task_subtype": "selection",
        "id_dataset": ds_id,
        "data_file": os.path.join(get_temp_path(), filename)
    }
    id = vsb.pc_service(data, None)
    return jsonify({"id": id}), 201


@app.route("/vsb/v1.0/test/", methods=['POST'])
@json_required
def start_test():
    # test type LS/R/M
    # ID user
    # next information
    # function type py/g
    # py - optim, IDs functions
    # g . exec command, ID of method
    # LS - parallel selection
    # R - pcs to use
    # M - log data, front-end-nodes
    input_dict = {"task_type": "run", "repeat": 5}
    try:
        test_type = request.json['test_type']
        if test_type not in ["ls", "r", "m"]:
            return jsonify({"error": "test_type can only be ls/r/m"}), 404
        input_dict['user'] = request.json['user_id']
        input_dict['information'] = request.json.get("informations", "")
        input_dict['typefile'] = method_type = request.json['method_type']
        input_dict['d/s'] = [
            {
                "dataset": request.json['dataset_id']
            }
        ]
        if method_type not in ['python', "general"]:
            return jsonify({"error": "method_type can only be python or general"}), 404
        if method_type == "python":
            input_dict['optimalization'] = request.json['opt']
            input_dict['repr_method'] = "&" + request.json['lf_id']
            input_dict['sim_method'] = "&" + request.json['sf_id']
        else:
            input_dict['execute_command'] = request.json['exec_command']
            input_dict['file'] = "&" + request.json['m_id']
        if test_type == "ls":
            input_dict['parallel_selections'] = request.json['par_sel']
        elif test_type == "r":
            input_dict['parallel_pc'] = True
            input_dict['pc_to_use'] = request.json['pc_to_use_path']
        else:
            input_dict['metacentrum'] = True
            input_dict['front_ends_nodes'] = request.json['front_nodes_path']
            input_dict['login_data'] = request.json['login_data_path']
    except Exception as e:
        return jsonify({"error": "Key {0} not found".format(str(e))}), 404
    id = new_task_run(input_dict, type)
    return jsonify({"task_id": id}), 201


@app.route("/vsb/v1.0/test/id~<int:task_id>", methods=['GET'])
def get_end_test(task_id):
    """
    Checks end of test.

    :param task_id: Id of task in web memory.
    :return:
    """
    value, answer, error = is_end_of_task(task_id)
    if value:
        result_file = get_task_information(id)["output_file"]
        if os.path.exists(result_file):
            try:
                subtests, test_info = parse_run_result(result_file)
                test_id = test_info['test_id']
                success = True
            except Exception as e:
                test_id = -1
                success = False
                print("Exception occurred by reading a result file: " + str(e))
            finally:
                clean_temp(id)
                delete_task(id)
        else:
            success = False
            test_id = -1
        return jsonify({'done': value, 'success': success, 'code': 201, 'test_id': test_id}), 201
    if error:
        return jsonify({'done': value, 'code': 201}), 201
    else:
        return jsonify({'done': value, 'code': 404}), 404


@app.route("/vsb/v1.0/analysis/id~<int:test_id>", methods=['GET'])
def get_analysis(test_id):
    """
    Returns json with analysis results.

    :param test_id: ID of test.
    :return:
    """
    data = {
        "task_type": "get",
        "task_subtype": "subtest",
        "test_id": test_id,
        "web": True
    }
    analysis = {}
    subtests, names = vsb.pc_service(data, None)
    if subtests is not None:
        for subtest in subtests:
            subtest_id = subtest[0]
            data = {
                "task_type": "get",
                "task_subtype": "analyze",
                "subtest_id": subtest_id,
                "web": True
            }
            ans, names = vsb.pc_service(data, None)
            analysis[subtest_id] = ans
    return jsonify({"analysis": analysis}), 201


@app.route("/vsb/v1.0/molecules/id~<int:test_id>", methods=['GET'])
def get_molecules(test_id):
    """
    Returns json file with molecules.

    :param test_id: ID of test.
    :return:
    """
    data = {
        "task_type": "get",
        "task_subtype": "subtest",
        "test_id": test_id,
        "web": True
    }
    molecules = {}
    subtests, names = vsb.pc_service(data, None)
    if subtests is not None:
        for subtest in subtests:
            subtest_id = subtest[0]
            data = {
                "task_type": "get",
                "task_subtype": "molecule",
                "subtest_id": subtest_id,
                "web": True
            }
            mols, names = vsb.pc_service(data, None)
            molecules[subtest_id] = mols
    return jsonify({"molecules": molecules}), 201


# next functions
def check_user(username):
    """
    Checks if a user with a given username and a given password in DB exists
    :param username: A given username
    :return: True, id of user/False, error description
    """
    if username == "host":
        return True, 1
    else:
        data = {
            "task_type": "front_end",
            "username": username,
            "task_subtype": "check_user",
            "web": True
        }
        id = vsb.pc_service(data, None)
        if id is not None and isinstance(id, int):
            return True, id
        else:
            return False, id


def add_new_user(username, details):
    """
    Adds a new user into DB.
    :param username: A given username
    :param details: Details about user
    :return: True, user_id or False, error desription
    """
    data = {
        "task_type": "put",
        "task_subtype": "user",
        "username": username,
        "information": details,
        "web": True
    }
    id = vsb.pc_service(data, None)
    if isinstance(id, int):
        return True, id
    else:
        return False, id


def redirect_to_result_page(id, result_file_path):
    # run type
    try:
        subtests, test_info = parse_run_result(result_file_path)
    except Exception as e:
        return "Exception occurred by reading a result file: " + str(e)
    finally:
        clean_temp(id)
        delete_task(id)
    return render_template("result_run.html", SUBTESTS=subtests, FIELDS=test_info)


def parse_run_result(result_file_path):
    """
    Makes a result page for run task.

    :param test_id: ID of taken place test
    :return: template
    """
    error = None
    with open(result_file_path, "r") as stream:
        js = json.load(stream)
    success = js['success']
    if not success:
        return str("Test didn't succeed")
    data = js['data']
    test_info = []
    subtests = []
    for key in data.keys():
        if not str(key).startswith("subtest"):
            test_info.append(data[key])
        else:
            subtests.append(data[key])
    return subtests, test_info


@app.template_test(name="values_ending")
def ends_with_values(field):
    """
    Returns true if a given value ends with substring "values"
    :param field: Value
    :return: True/False
    """
    return str(field).endswith("values")


@app.template_test(name="names_ending")
def ends_with_names(field):
    """
    Returns true if a given value ends with substring "names"
    :param field: Value
    :return: True/False
    """
    return str(field).endswith("names")


def reading_run_options(form_dicitonary, files, user_id):
    dict = {}
    type = str(form_dicitonary['type_option']).replace("opt_", "")
    if "optim_option" in form_dicitonary:
        optim = str(form_dicitonary['optim_option']).replace("opt_", "")
    if 'p_sels_option' in form_dicitonary:
        dict['parallel_selections'] = str(form_dicitonary['p_sels_option']).replace("opt_", "")
    dict['information'] = str(form_dicitonary['added_information'])
    dict['typefile'] = type
    dict['repeat'] = 1
    dict['task_type'] = "run"
    dict['user'] = user_id
    if type == "python":
        # repr file
        # read id
        repr_method_content = str(form_dicitonary['file_repr_method'])
        repr_method_id = repr_method_content.split(":")[0].strip()
        dict['repr_method'] = "&" + repr_method_id
        # sim file
        # read id
        sim_method_content = str(form_dicitonary['sim_method'])
        sim_method_id = sim_method_content.split(":")[0].strip()
        dict['sim_method'] = "&" + sim_method_id
        # optimization
        dict['optimalization'] = optim
    else:
        # file file
        # read id
        file_method_content = str(form_dicitonary['file_repr_method'])
        file_id = file_method_content.split(":")[0].strip()
        dict['file'] = "&" + file_id
    # d/s
    d_s = []
    for key in form_dicitonary:
        if str(key).startswith("d_s_input_"):
            values = str(form_dicitonary[key]).split("/")
            data_set = values[0]
            selection = values[1]
            d_s.append({
                "dataset": data_set,
                "selection": selection
            })
    dict["d/s"] = [{
        "dataset": form_dicitonary['data_set_id']
    }]
    return True, dict


def reading_ds(form_dict):
    """
    Reads d/s from given form.
    :param form_dict: Form dictionary.
    :return:
    """
    # d/s
    d_s = []
    for key in form_dict:
        if str(key).startswith("d_s_input_"):
            values = str(form_dict[key]).split("/")
            data_set = values[0]
            selection = values[1]
            d_s.append({
                "dataset": data_set,
                "selection": selection
            })
    return d_s


def add_metacentrum_infos(form_dicitonary, config_dict):
    username = form_dicitonary['input_login']
    pas = form_dicitonary['input_password']
    if username == "" or pas == "":
        return False, config_dict
    else:
        path = create_mc_log_data(username, pas)
        config_dict['login_data'] = os.path.abspath(path)
        config_dict['metacentrum'] = "True"
        config_dict['front_ends_nodes'] = get_path_front_nodes(form_dicitonary['select_fen'])
        return True, config_dict


def add_rc_infos(form_dicitonary, config_dict):
    # remote pcs
    config_dict["pc_to_use"] = os.path.join(path_remote_configurations, form_dicitonary['configuration_name'])
    config_dict['parallel_pc'] = "True"
    return True, config_dict


def task_id():
    global i
    i += 1
    return str(i)


def show_details(chosen_option, results, chosen_results, **args):
    error = ""
    if chosen_option == "method_n_m":
        result = results[0][chosen_results]
        method_id = result[0]
        return redirect(url_for('details_on_method', method_id=method_id))
    elif chosen_option == "method_n":
        result = results[chosen_results]
        method_id = result[1]
        return redirect(url_for('details_on_method', method_id=method_id))
    elif chosen_option == "method_fr":
        result = results[chosen_results]
        method_id = result[0]
        return redirect(url_for('details_on_method', method_id=method_id))
    elif chosen_option == "method_s":
        result = results[2][chosen_results]
        method_id = result[0]
        return redirect(url_for('details_on_method', method_id=method_id))
    elif chosen_option == "method_l":
        result = results[1][chosen_results]
        method_id = result[0]
        return redirect(url_for('details_on_method', method_id=method_id))
    elif chosen_option == "test":
        result = results[chosen_results]
        test_id = result[0]
        data = {
            "task_type": "get",
            "task_subtype": "test",
            "full_info": True,
            "test_id": test_id,
            "web": True
        }
        test = vsb.pc_service(data, None)
        test_info = []
        subtests = []
        for key in test.keys():
            if not str(key).startswith("subtest"):
                test_info.append(test[key])
            else:
                subtests.append(test[key])
        # clean_temp(id)
        return render_template("result_run.html", SUBTESTS=subtests, FIELDS=test_info)
    elif chosen_option == "method_details":
        result = results[chosen_results]
        subtest_id = result[0]
        return redirect(url_for('details_on_subtest', subtest_id=subtest_id))
    elif chosen_option == "choose_methods":
        methods = []
        analysis_names = []
        molecule_names = []
        # getting ids
        # getting names
        ids = []
        names = []
        for i in chosen_results:
            l = int(i)
            ids.append(results[l][0])
            names.append(results[l][2] + "; " + results[l][3])
        if 4 < len(ids):
            num = 1
        else:
            num = len(ids)
        if 0 < num:
            expands = int(12 / num)
        else:
            expands = 12
        for i in range(0, num):
            # getting analysis
            data = {
                "web": True,
                "task_type": "get",
                "task_subtype": "analyze",
                "subtest_id": ids[i]
            }
            analysis, analysis_names = vsb.pc_service(data, None)
            if not len(analysis) > 0:
                analyze = []
            else:
                analyze = analysis[0]
            # getting molecules
            data = {
                "web": True,
                "task_type": "get",
                "task_subtype": "molecule",
                "subtest_id": ids[i]
            }
            molecules, molecule_names = vsb.pc_service(data, None)
            molecules = sorted(molecules,
                               key=lambda m: m[3],
                               reverse=True)
            if not len(molecules) > 0:
                mols = [[]]
            else:
                mols = molecules
            methods.append([names[i], analyze, mols])
        return render_template("methods_comparison.html", analysis_names=analysis_names, molecule_names=molecule_names,
                               methods=methods, data_set_id=args["dataset_id"], selection_id=args["selection_id"],
                               expands=expands)
    elif chosen_option == "get_datasets":
        result = results[chosen_results]
        data_set_name = result[0]
        return redirect(url_for('show_data_set_description', data_set_id=data_set_name))
    elif chosen_option == "choose_n_selections":
        data_set_selection = {}
        met_names = []
        for result in results:
            data = {
                "web": True,
                "task_type": "get",
                "task_subtype": "method",
                "methods_analysis": True,
                "method_data": True,
                "data_set_id": result["dataset"],
                "selection_id": result["selection"]
            }
            met_infos, met_names = vsb.pc_service(data, None)
            if len(met_infos) == 0:
                continue
            for met_info in met_infos:
                ds = result["dataset"]
                s = result["selection"]
                key = ds + "/" + s
                if key not in data_set_selection:
                    data_set_selection[key] = []
                data_set_selection[key].append(met_info)
        return analysis_n_items(data_set_selection, met_names)


def analysis_n_items(data_set_selection, met_names):
    if len(met_names) > 0:
        # process data
        data_set_selection_values = {}
        methods = {}
        analyze_len = len(met_names) - 7
        mms_names = []
        for i in range(7, len(met_names)):
            mms_names.append(met_names[i])
        analyze_names = [met_names[2], met_names[3], met_names[4]]
        for i in range(7, len(met_names)):
            analyze_names.append(met_names[i])
        for key in data_set_selection:
            maxim_values = []
            maxim_names = []
            minim_values = []
            minim_names = []
            sum_values = []
            count = len(data_set_selection[key])
            # init values
            for _ in range(0, analyze_len):
                maxim_names.append("")
                minim_values.append(10000000000000000000000000)
                minim_names.append("")
                maxim_values.append(-1)
                sum_values.append(0)
            # max, min, avg analyze
            analyze_rows = []
            molecules_rows = {}
            molecule_function = []
            # computing max, min, avg scores for dataset, selection
            for dat in data_set_selection[key]:
                row = [dat[2], dat[3], dat[4]]
                for i in range(7, len(dat)):
                    row.append(dat[i])
                    if minim_values[i - 7] > dat[i]:
                        minim_values[i - 7] = dat[i]
                        minim_names[i - 7] = str(dat[3]) + "/" + str(dat[4])
                    if maxim_values[i - 7] < dat[i]:
                        maxim_values[i - 7] = dat[i]
                        maxim_names[i - 7] = str(dat[3]) + "/" + str(dat[4])
                    sum_values[i - 7] += dat[i]
                analyze_rows.append(row)
                molecule_function.append(str(dat[3]) + "/" + str(dat[4]))
                data = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "molecule",
                    "subtest_id": dat[1]
                }
                molecules, mol_names = vsb.pc_service(data, None)
                if molecules is not None:
                    for mol in molecules:
                        if mol[2] not in molecules_rows:
                            molecules_rows[mol[2]] = [mol[3]]
                        else:
                            molecules_rows[mol[2]].append(mol[3])
            for i in range(0, len(sum_values)):
                sum_values[i] /= count
            # assignment of values for data-set and selection
            data_set_selection_values[key] = {
                "maxim_values": maxim_values,
                "maxim_names": maxim_names,
                "minim_values": minim_values,
                "minim_names": minim_names,
                "avg_values": sum_values,
                "analyze_rows": analyze_rows,
                'molecules_rows': molecules_rows,
                'molecules_function': molecule_function
            }
            ns = {}
            for n in maxim_names:
                if n not in ns:
                    ns[n] = 1
                else:
                    ns[n] += 1
            maximum_name = ""
            maximum_value = 0
            for k in ns:
                if maximum_value < ns[k]:
                    maximum_value = ns[k]
                    maximum_name = k
            max_analyze = None
            for row in analyze_rows:
                if str(row[1]) + "/" + str(row[2]) == maximum_name:
                    max_analyze = row
                    break
            # computing difference between method score and max score
            an_balances = []
            for row in analyze_rows:
                r = [row[0], row[1], row[2]]
                for pst in range(3, len(row)):
                    r.append(row[pst] - max_analyze[pst])
                an_balances.append(r)
            max_analyze = max_analyze[3:]
            max_analyze_names = analyze_names[3:]
            if maximum_name not in methods:
                methods[maximum_name] = {
                    "information": {},
                    "method_hit": 1,
                    "datasets": {
                        key: [max_analyze, an_balances]
                    }
                }
            else:
                methods[maximum_name]["method_hit"] += 1
                methods[maximum_name]["datasets"][key] = [max_analyze, an_balances]
        return render_template("ds_s_methods.html", data_set_selection_values=data_set_selection_values,
                               analyze_names=analyze_names, mms_names=mms_names, methods=methods,
                               max_analyze_names=max_analyze_names)


def get_selections():
    """
    Gets all selections in VSB.

    :return: List [data-set name, selections_names]
    """
    data = {
        "task_type": "get",
        "task_subtype": "selection",
        "all": "y",
        "web": True
    }
    results = vsb.pc_service(data, None)
    datasets = []
    for dataset in results:
        datasets.append([dataset, results[dataset]])
    datasets.sort()
    return datasets


def get_methods(type="list"):
    """
    Gets all methods in VSB.

    :return: List [[id, type, fname, sname], ...]
    """
    data = {
        "task_type": "get",
        "task_subtype": "method",
        "all": "y",
        "web": True
    }
    results = vsb.pc_service(data, None)
    methods = []
    loads = []
    scores = []
    mes = []
    gens = []
    for method in results:
        if method[1] == "py":
            method_type = "Python"
        else:
            method_type = "General"
        if type == "list":
            if method[1] == "py" and not method[4].strip().lower() == "empty" and method[
                5].strip().lower() == "empty":
                loads.append([method[0], method_type, method[4]])
            if method[1] == "general":
                gens.append([method[0], method_type, method[4]])
        elif type == "run":
            if (method[1] == "py" and not method[4].strip().lower() == "empty" and method[
                5].strip().lower() == "empty") or method[1] == "general":
                loads.append([method[0], method_type, method[4]])
        if method[1] == "py" and not method[5].strip().lower() == "empty" and method[4].strip().lower() == "empty":
            scores.append([method[0], method_type, method[5]])
        if (method[1] == "py" and not method[4].strip().lower() == "empty" and not method[
            5].strip().lower() == "empty"):
            mes.append([method[0], method_type, method[4], method[5]])
        if method[1] == "general":
            mes.append([method[0], method_type, method[4], "Doesn't exist"])
    if type == "list":
        return [gens, loads, scores]
    elif type == "run":
        return [loads, scores]
    elif type == "tests":
        return mes
    return methods


def get_data_sets():
    data = {
        "task_type": "get",
        "task_subtype": "dataset",
        "all": "y",
        "web": True
    }
    datasets = vsb.pc_service(data, None)
    return datasets


def get_tests():
    data = {
        "task_type": "get",
        "task_subtype": "test",
        "all": "y",
        "web": True
    }
    tests, names = vsb.pc_service(data, None)
    repaired_tests_names = []
    # ls = Local computer serial, lp = Local computer parallel, mc = Metacentrum, r = Remote computers
    for test in tests:
        repaired_test = []
        item_i = 0
        for item in test:
            item_i += 1
            if item_i == 4:
                if item == "ls":
                    test_type = "Local computer serial"
                elif item == "lp":
                    test_type = "Local computer parallel"
                elif item == "mc":
                    test_type = "Metacentrum"
                elif item == "r":
                    test_type = "Remote computers"
                repaired_test.append(test_type)
            else:
                repaired_test.append(item)
        repaired_tests_names.append(repaired_test)
    return repaired_tests_names, names


def get_task_information(id):
    """
    Returns information about task.

    :param id: Id of the task
    :return: List [task_type, output_file, task_subtype, p]
    """
    global actual_tasks
    id = str(id).strip()
    if id in actual_tasks:
        return actual_tasks[id]
    else:
        return None


def get_path(id, type):
    global l
    if type == "sdf":
        suffix = ".sdf"
    elif type == "zip":
        suffix = ".zip"
    elif type == "png":
        suffix = ".png"
        l += 1
        prefix = "t_file_" + str(l) + "_"
        file_name = prefix + str(id) + suffix
        path = os.path.join(get_image_path(), file_name)
        return os.path.abspath(path)
    elif type == "csv":
        suffix = ".csv"
    else:
        suffix = ".json"
    if type == "test":
        prefix = 'query_'
    elif type == "result":
        prefix = "result_"
    else:
        l += 1
        prefix = "t_file_" + str(l) + "_"
    file_name = prefix + str(id) + suffix
    path = os.path.join(get_temp_path(), file_name)
    return os.path.abspath(path)


def get_temp_path():
    path = 'temp'
    if not os.path.exists(path):
        os.mkdir(path)
    return path


def get_image_path():
    path = "static/images"
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def set_task_information(id, task_type, output_file, _, type, task_subtype=None, p=None):
    global actual_tasks
    import datetime
    date = datetime.datetime.utcnow()
    if type == "ls":
        test_type = "Local computer serial"
    elif type == "lp":
        test_type = "Local computer parallel"
    elif type == "mc":
        test_type = "Metacentrum"
    else:
        test_type = "Remote computers"
    actual_tasks[id] = {"task_type": task_type,
                        "output_file": output_file,
                        "date": date,
                        "type": test_type,
                        "task_subtype": task_subtype,
                        "p": p}


def new_test(test_config, input_path):
    c_d = {"test": {"n1": test_config},
           "metadata": {
               "test_count": 1,
               "ignore": [],
               "parallel_tests": "False"
           }}
    with open(input_path, "w") as stream:
        json.dump(c_d, stream, indent=2)
    line_arguments = {
        "mode_a": True,
        "file": input_path,
        "mode": "a"
    }
    p = multiprocessing.Process(target=vsb.main, args=(line_arguments,))
    p.start()
    return p


# active test

def new_task_run(dictionary, type):
    import datetime
    date = datetime.datetime.utcnow()
    id = task_id()
    task_type = dictionary['task_type']
    query_path = get_path(id, 'test')
    result_path = get_path(id, 'result')
    dictionary['fc_ofile'] = result_path
    p = new_test(dictionary, query_path)
    set_task_information(id, task_type, result_path, date, type, p=p)
    return id


def is_end_of_task(id):
    infs = get_task_information(str(id).strip())
    if infs is None:
        return False, "Task doesn't exist", False
    p = infs["p"]
    if p.exitcode is None:
        return False, "Task is still running", True
    else:
        return True, "Done", True


def get_front_nodes():
    names = []
    with open("data/front_nodes.json", "r") as stream:
        js = json.load(stream)
        for node in js['nodes']:
            names.append(node['name'])
    return names


def get_path_front_nodes(name):
    d = {}
    with open("data/front_nodes.json", "r") as stream:
        js = json.load(stream)
        for node in js['nodes']:
            if node['name'] == name:
                d['nodes'] = [node]
    t_file = get_path(0, "file")
    with open(t_file, "w") as stream:
        json.dump(d, stream, indent=2)
    return t_file


def create_mc_log_data(username, password):
    t_file = get_path(0, "t")
    dict = {"metacentrum": {
        "login": username,
        "password": password
    }
    }
    with open(t_file, "w") as stream:
        json.dump(dict, stream)
    return t_file


def create_rc_pcs_data(pcs):
    t_file = get_path(0, "t")
    dict = {"pcs": pcs}
    with open(t_file, "w") as stream:
        json.dump(dict, stream)
    return t_file


def allowed_file(filename):
    ALLOWED_EXTENSIONS = ['json', 'sdf']
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def test_init():
    set_task_information("1", "run", os.path.abspath(os.path.join(get_temp_path(), "result_" + str(1) + ".json")))
    set_task_information("2", "get", os.path.abspath(os.path.join(get_temp_path(), 't_file_2_2.sdf')), "dataset")


def _read_config(path="../configurations/main_conf.json"):
    global config
    if path is None:
        path = "../configurations/main_conf.json"
    with open(path, "r") as stream:
        config_file = json.load(stream)
        if "flask" in config_file:
            config = config_file["flask"]
        else:
            config["host"] = "localhost"
            config["port"] = 5000
        if "debug" not in config_file["flask"]:
            config["debug"] = False
        else:
            if config_file["flask"]["debug"] == "True":
                config["debug"] = True
            else:
                config["debug"] = False


def clean_temp(id):
    query_path = get_path(id, "test")
    result_path = get_path(id, "result")
    try:
        os.remove(query_path)
    except Exception as e:
        pass
    try:
        os.remove(result_path)
    except Exception as e:
        pass


def delete_task(id):
    global actual_tasks
    actual_tasks.pop(id, None)


def clean():
    print("Cleaning folders")
    temp_path = get_temp_path()
    image_path = get_image_path()
    import shutil
    try:
        shutil.rmtree(temp_path)
    except Exception as e:
        print(e)
    try:
        shutil.rmtree(image_path)
    except Exception as e:
        print(e)


def main(configuration_path):
    cur_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(cur_path)
    sys.path.append(cur_path)
    print("Reading configuration")
    _read_config(configuration_path)
    import atexit
    atexit.register(clean)
    # import webbrowser
    # webbrowser.open("http://localhost:5000", new=2)
    app.run(host=config["host"], port=config["port"], debug=config["debug"])


if __name__ == "__main__":
    main()
