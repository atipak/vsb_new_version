import json
import os

f_db = None


def read_data_set_skoda(path):
    """
    Reads a file containing data set from database on SIRET server.
    :param path: Path to data set file
    :return: active, inactive, metadata
    """
    with open(path, "r") as stream:
        js = json.load(stream)
        active = []
        for name_dic in js['data']['ligands']:
            name = name_dic['name']
            active.append(name)
        inactive = []
        for name_dic in js['data']['decoys']:
            name = name_dic['name']
            inactive.append(name)
        metadata = js['metadata']
    return active, inactive, metadata


def read_selection_skoda(path):
    """
    Reads a file containing selection from database on SIRET server.
    :param path: Path to selection file
    :return: metadata, train_active, train_inactive, test
    """
    with open(path, "r") as stream:
        js = json.load(stream)
        metadata = js['metadata']
        train_active = []
        for name_dic in js['data']['train']['ligands']:
            name = name_dic['name']
            train_active.append(name)
        train_inactive = []
        for name_dic in js['data']['train']['decoys']:
            name = name_dic['name']
            train_inactive.append(name)
        test = []
        for name_dic in js['data']['test']:
            name = name_dic['name']
            test.append(name)
    return metadata, train_active, train_inactive, test


def go_through_location(root_folder):
    """
    Goes through directory hierarchy of database from SIRET database server and adds all data set and selection which it founds
    :param root_folder: Location of root folder
    :return: Nothing
    """

    num_sel = 0
    num_data_sets = 0
    if os.path.exists(root_folder):
        sub_folders = os.listdir(root_folder)
        for folder in sub_folders:
            folder = os.path.join(root_folder, folder)
            sdf_folder = os.path.join(folder, "molecules", "sdf")
            if not os.path.exists(sdf_folder):
                continue
            selection_folder = os.path.join(folder, "selections")
            if not os.path.exists(selection_folder):
                continue
            random_folders = os.listdir(selection_folder)
            for rf in random_folders:
                random_folder = os.path.join(selection_folder, rf)
                folders = os.listdir(random_folder)
                for f in folders:
                    f_folder = os.path.join(random_folder, f)
                    selections = os.listdir(f_folder)
                    name = ""
                    for file in selections:
                        file_path = os.path.join(f_folder, file)
                        if file == "group.json":
                            active, inactive, metadata = read_data_set_skoda(file_path)
                            ligand_file_name = os.path.basename(f_folder) + "_Ligands.sdf"
                            decoys_file_name = os.path.basename(f_folder) + "_Decoys.sdf"
                            ligands_path = os.path.join(sdf_folder, ligand_file_name)
                            decoys_path = os.path.join(sdf_folder, decoys_file_name)
                            sdf_string = ""
                            with open(ligands_path) as stream:
                                sdf_string = stream.read()
                            with open(decoys_path) as stream:
                                sdf_string = sdf_string + stream.read()
                            basic_ds_name = str(os.path.basename(folder)) + " " + str(rf) + " " + os.path.basename(f_folder)
                            name, value = f_db.add_data_set(active, inactive, metadata, sdf_string,
                                                              basic_ds_name, True)
                            print("Data-set with name {0} was added".format(name))
                            num_data_sets += 1
                            break
                    if not value:
                        print("Error by creating data-set {0}: {1}".format(os.path.basename(f_folder), name))
                    if name == "":
                        continue
                    for file in selections:
                        file_path = os.path.join(f_folder, file)
                        if file.startswith("s_"):
                            metadata, train_active, train_inactive, test = read_selection_skoda(file_path)
                            s_name, value = f_db.add_selection(name, metadata, train_active, train_inactive, test)
                            if not value:
                                print("Error in data-set {0} and selection {1}: {2}".format(name, file, s_name))
                            num_sel += 1
    print("{0} data-sets were added".format(num_data_sets))
    print("{0} selections were added".format(num_sel))


def base():
    import sys
    sys.path.append("../src")
    global f_db
    f_db = __import__("file_database")
    print("Insert a path to the folder with database: ")
    path = str(input("Path: ")).strip().replace("\"", "").replace("\'", "")
    #path = "E:\Benchmark\VSB data\datasets"
    if os.path.exists(path):
        go_through_location(path)


if __name__ == '__main__':
    base()
