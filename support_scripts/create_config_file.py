import os
import json

file_name = "main_conf.json"
folder_path = "../configurations"


def main():
    """
    Function which creates a new configuration file for application VSB.

    :return: Nothing
    """
    print("Hello")
    print("This scripts is a guide for creating a new config file.")
    print("Do you want to create a new main_conf.json in configurations folder? (if this file already exists, it will be overwrite!!)")
    a = False
    while True:
        ans = str(input("Answer (y/n): ")).strip()
        if ans == "y":
            a = True
            file_path = os.path.join(folder_path, file_name)
            break
        elif ans == "n":
            a = False
            break
        else:
            print("Insert y or n!")
            continue
    if not a:
        print("Insert path where you want to write a file in: (for actual folder and name main_conf.json press only enter)")
        print("Example: my/folder/subfolder/confs/my_config_file.json")
        while True:
            file_path = str(input("Path: ")).strip()
            if file_path == "":
                file_path = os.path.join(".", file_name)
                break
            else:
                if os.path.exists(file_path):
                    print("File on address {0} exists. Do you want to overwrite it (y/n)?".format(file_path))
                    while True:
                        ans = str(input("Answer: ")).strip()
                        if ans == "y":
                            a = True
                            break
                        elif ans == "n":
                            a = False
                            break
                        else:
                            print("Insert y or n!")
                            continue
                    if a:
                        break
                else:
                    if not os.path.exists(os.path.dirname(file_path)):
                        os.makedirs(os.path.dirname(file_path))
                        break

    print("\n")
    # basic paths
    adds = {
        "zip_database_adrress": "https://www.dropbox.com/s/2x92p8g5wrp08ln/assay.zip?dl=1",
        "version_adrress": "https://www.dropbox.com/s/z4ofa2gbgw109cm/version.txt?dl=1"
    }
    # data_conf
    print("Do you want to change a path to database folder? (y/n)")
    a = False
    while True:
        ans = str(input("Answer: ")).strip()
        if ans == "y":
            a = True
            break
        elif ans == "n":
            a = False
            break
        else:
            print("Insert y or n!")
            continue
    if a:
        print("Insert a new path: ")
        while True:
            pth = str(input("Path: ")).strip()
            print("Confirm path: {0} (y/n)".format(pth))
            while True:
                ans = str(input("Answer: ")).strip()
                if ans == "y":
                    a = True
                    break
                elif ans == "n":
                    a = False
                    break
                else:
                    print("Insert y or n!")
                    continue
            if a:
                break
        data = {
            "main_folder": pth
        }
    else:
        data = {
            "main_folder": "../database/"
        }
    # sql_conf
    print("Do you have MySQL server (y/n)")
    a = False
    while True:
        ans = str(input("Answer: ")).strip()
        if ans == "y":
            a = True
            break
        elif ans == "n":
            a = False
            sql = {
                "host": "{0}".format("localhost"),
                "database": "{0}".format("benchmark"),
                "user": "{0}".format("?"),
                "password": "{0}".format("?")
            }
            break
        else:
            print("Insert y or n!")
            continue
    if a:
        while True:
            print("Now insert your data to SQL database: ")
            host = str(input("Host: ")).strip()
            port = str(input("Port (-1 == no specific port): ")).strip()
            db = str(input("Database: ")).strip()
            user = str(input("User: ")).strip()
            pas = str(input("Password: ")).strip()
            print("\n\nDo you agree with following data? (y/n)")
            print("Host: {0}".format(host))
            print("Port: {0}".format(port))
            print("Database: {0}".format(db))
            print("User: {0}".format(user))
            print("Password: {0}".format(pas))
            while True:
                ans = str(input("Answer: ")).strip()
                if ans == "y":
                    a = True
                    break
                elif ans == "n":
                    a = False
                    break
                else:
                    print("Insert y or n!")
                    continue
            if a:
                break
        sql = {
            "host": "{0}".format(host),
            "database": "{0}".format(db),
            "user": "{0}".format(user),
            "password": "{0}".format(pas)
        }
        if not port == str(-1) or not port == "":
            sql["port"] = int(port)
    # metacetrum
    print("\n")
    print(
        "Do you have an account on the infrastructure Metacentrum and do you want to insert your loging data into the configuration file? (y/n)")
    while True:
        ans = str(input("Answer: ")).strip()
        if ans == "y":
            mc = True
            break
        elif ans == "n":
            mc = False
            break
        else:
            print("Insert y or n!")
            continue
    print("\n")
    if mc:
        while True:
            print("Now insert your data to Metacentrum: ")
            user = str(input("Username: ")).strip()
            pas = str(input("Password: ")).strip()
            print("\n")
            print("\n")
            print("Do you agree with following data? (y/n)")
            print("Username: {0}".format(user))
            print("Password: {0}".format(pas))
            while True:
                ans = str(input("Answer: ")).strip()
                if ans == "y":
                    a = True
                    break
                elif ans == "n":
                    a = False
                    break
                else:
                    print("Insert y or n!")
                    continue
            if a:
                break
        mc_data = {
            "login": "{0}".format(user),
            "password": "{0}".format(pas)
        }

    # flask
    print("\n")
    print(
        "Do you want to change the host and port properties of web pages (default host='localhost', port=5000)? (y/n)")
    while True:
        ans = str(input("Answer: ")).strip()
        if ans == "y":
            while True:
                print("Insert your required data: ")
                user = str(input("Host: ")).strip()
                pas = str(input("Port: ")).strip()
                print("\n")
                print("\n")
                print("Do you agree with following data? (y/n)")
                print("Username: {0}".format(user))
                print("Password: {0}".format(pas))
                while True:
                    ans = str(input("Answer: ")).strip()
                    if ans == "y":
                        a = True
                        break
                    elif ans == "n":
                        a = False
                        break
                    else:
                        print("Insert y or n!")
                        continue
                if a:
                    break
            fl_data = {
                "host": "{0}".format(user),
                "port": "{0}".format(int(pas))
            }
            break
        elif ans == "n":
            fl_data = {
                "host": "{0}".format("localhost"),
                "port": "{0}".format(int(5000))
            }
            break
        else:
            print("Insert y or n!")
            continue
    # writing file
    dict = {
        "adresses": adds,
        "data_conf": data,
        "sql_conf": sql,
        "flask": fl_data
    }
    if mc:
        dict['metacentrum'] = mc_data
    print("Do you want to set number of cores used by parallel launching? (y/n)")
    number = -1
    while True:
        ans = str(input("Answer: ")).strip()
        if ans == "y":
            while True:
                print("Insert number:")
                print("Example: 4")
                number = input(">>>")
                try:
                    number = int(number)
                except Exception as e:
                    print("Your input caused an exception: " + str(e))
                break
            break
        elif ans == "n":
            break
        else:
            print("Insert y or n!")
            continue
    dict["cores"] = number
    with open(file_path, "w") as stream:
        json.dump(dict, stream, indent=2)


if __name__ == '__main__':
    main()
