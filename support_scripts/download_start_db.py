import urllib.request
import urllib.error
import urllib.parse
import os
import zipfile
import json
import shutil

config = None
f_db = None


def getresult(url):
    try:
        connection = urllib.request.urlopen(url)
    except urllib.error.HTTPError:
        return ""
    else:
        return connection.read().rstrip()


def update_dataset_database():
    """
    Check if there is a need to update the file database. It's needed Internet connection.

    :return: True if there is a need else False, date of last updating database, date of the upload of files on the Internet, version of the upload of files on the Internet

    """
    need_update = False
    try:
        getresult("http://www.google.com")
    except:
        raise Exception("No connection to the Internet, please connect your PC to the Internet.")
    import datetime
    site = str((getresult(get_url_with_saved_database("v"))).decode(encoding='utf-8'))
    site_parts = site.splitlines()
    try:
        ver = int(site_parts[0])
    except Exception as en:
        print(en)
        ver = 0
    try:
        date = site_parts[1]
    except Exception as en:
        date = datetime.datetime(2015, 6, 1)
    # databaze na pc existuje
    if os.path.exists(config['database_folder']):
        ver_path = config['database_version_file']
        # soubor s verzi databaze na pc existuje
        if os.path.exists(ver_path):
            with open(ver_path, "r") as ver_file:
                # ziskani hodnot ze souboru
                file_parts = ver_file.readlines()
                version = None
                date_file = None
                try:
                    version = file_parts[0]
                    date_file = file_parts[1]
                except:
                    print("Version file doesn't exist or it is bad formatted. Information completed.")
                # verze se souboru
                if version == "" or version is None:
                    version = 0
                else:
                    try:
                        version = float(version)
                    except:
                        print("Database version wasn't recognized. Version was set on 0")
                        version = 0
                # datum ze souboru
                start_epoch = datetime.datetime.fromtimestamp(0)
                now = datetime.datetime.utcnow() - start_epoch
                if date_file == "" or date_file is None:
                    date_file = datetime.datetime(2015, 6, 1)
                else:
                    try:
                        date_file = datetime.datetime.strptime(date_file, '%d.%m.%Y')
                    except:
                        print("Date of database wasn't recognized. Date was set on 01.06.2015")
                        date_file = datetime.datetime(2015, 6, 1)

            # verze je vyssi
            if version >= ver:
                print("Database is actual")
                need_update = False
            else:
                need_update = True
        else:
            need_update = True
            date_file = date
    else:
        try:
            need_update = True
            date_file = date
        except:
            raise IOError("Database couldn't be created.")
    return need_update, date_file, date, ver


def get_temp_folder_path():
    """
    Returns a path of the temp folder.

    :return: The path
    """
    return "../temp"


def show_progress(file_name, size):
    """
    Shows a progress in downloading.
    :param file_name: Name of controlled file
    :param size: End size
    :return:
    """
    import time
    act_procent = 0
    print("Downloaded: {0}%".format(act_procent))
    loops_number = 0
    existed = False
    while True:
        try:
            fsize = os.stat(file_name).st_size
            if 100 <= fsize / size * 100:
                break
            if act_procent < fsize / size * 100:
                while act_procent < fsize / size * 100:
                    act_procent += 10
                print("Downloaded: <{0}%".format(act_procent))
            existed = True
            time.sleep(10)
        except:
            if not existed:
                time.sleep(5)
                loops_number += 1
                if loops_number == 10:
                    break
                pass
            else:
                break


def get_unzip_file(url, dest="."):
    """
    Downloads a file from a given location and unzips this file.
    :param url: source URL
    :param dest: destination path
    :return: Nothing
    """
    if not os.path.exists(get_temp_folder_path()):
        os.mkdir(get_temp_folder_path())
    zip_name = os.path.join(get_temp_folder_path(), "database.zip")
    try:
        import shutil
        import threading
        print("Database downloading...")
        with urllib.request.urlopen(url) as response, open(zip_name, 'wb') as out_file:
            t = threading.Thread(target=show_progress, args=(zip_name, int(response.info()['Content-Length'])))
            t.start()
            shutil.copyfileobj(response, out_file)
        print("Downloaded: 100%")
        print("Database downloaded")
    except urllib.error.HTTPError:
        print("Chyba ve stahovani")
    try:
        z = zipfile.ZipFile(zip_name, "r")
        print("Extracting files from ZIP")
        z.extractall(path=dest)
        z.close()
        print("Files extracted")
    except zipfile.BadZipFile:
        print("Chyba v zip souboru")
    os.remove(zip_name)


def updating_db(local_last_upload_date, net_db_date, new_version):
    """
    Updates file database on local computer.

    :param new_version: Number of a new version
    :param local_last_upload_date: A date when the database was updated last.
    :param net_db_date: A date of database on the internet.
    :return: Nothing
    """
    print("Updating database ...")
    import datetime
    ver_path = config['database_version_file']
    db_folder_path = config['database_folder']
    get_unzip_file(get_url_with_saved_database("d"), config['extract_files_path'])
    # prochazeni a cteni datumu assayi, jejich nasledne pridani a jejich selection
    print("Importing data-sets and selections ...")
    base_path = config["assays_folder"]
    from stat import S_ISDIR
    for folder in os.listdir(base_path):
        path = os.path.join(base_path, folder)
        mode = os.stat(path).st_mode
        if S_ISDIR(mode):
            path_info = path + os.sep + "info.json"
            new_assay = False
            with open(path_info, "r") as stream:
                js = json.load(stream)
                dt = js['metadata']['date']
                # "17:27,4.5.2016"
                assay_date = datetime.datetime.strptime(dt, '%H:%M,%d.%m.%Y')
                if local_last_upload_date < assay_date:
                    # pridani data setu
                    path_sdf = path + os.sep + "mols.sdf"
                    with open(path_sdf, "r") as fi:
                        string = fi.read()
                    name, b_value = f_db.add_data_set(js['data']['molecules']['active'],
                                                      js['data']['molecules']['inactive'],
                                                      js['metadata'], string, os.path.basename(folder), True)
            # pridani vyberu
            if b_value:
                sels_path = path + os.sep + "selection"
                i = 0
                for sel in os.listdir(sels_path):
                    path_sel = os.path.join(sels_path, sel)
                    path_sel_info = path_sel + os.sep + "info.json"
                    try:
                        if os.path.exists(path_sel_info):
                            i += 1
                            with open(path_sel_info, "r") as stream:
                                js = json.load(stream)
                                train_active = js['data']['molecules']['train']['actives']
                                train_inactive = js['data']['molecules']['train']['inactives']
                                test = js['data']['molecules']['test']
                                validation_active = []
                                validation_inactive = []
                                if "validation" in js['data']['molecules']:
                                    if "actives" in js['data']['molecules']['validation']:
                                        validation_active = js['data']['molecules']['validation']['actives']
                                    if "inactives" in js['data']['molecules']['validation']:
                                        validation_inactive = js['data']['molecules']['validation']['inactives']
                                f_db.add_selection(name, js['metadata'], train_active, train_inactive, test,
                                                   validation_active, validation_inactive)
                    except Exception as e:
                        print("Selection with ID {0} couldn't be added becouse of {1}.".format(sel, e))
    print("Creating a new version file")
    with open(ver_path, "w") as ver_file:
        ver_file.write(str(str(new_version) + "\n"))
        ver_file.write(str(str(net_db_date.day) + "." + str(net_db_date.month) + "." + str(net_db_date.year)))
    print("End of the database updating")
    shutil.rmtree(config['extract_files_path'])


def main():
    import sys
    sys.path.append("../src")
    global f_db
    f_db = __import__("file_database")
    fill_config()
    fill_download_paths()
    update_need, local_date, net_date, net_version = update_dataset_database()
    if update_need:
        updating_db(local_date, net_date, net_version)


def get_url_with_saved_database(type):
    """
    Returns an url of web site where database or version file is saved.
    :param type v = version url, else database url
    :return: Web site url
    """
    if type == "v":
        return config["version_adrress"]
    else:
        return config["zip_database_adrress"]


def fill_config():
    """
    Fill variable config with basic paths which are used in other methods.
    :param config: Variable which should be filled.
    :return: Filled config variable
    """

    global config
    config = {}
    config['temp_folder'] = "../temp"
    config['database_folder'] = "../database"
    config['assays_folder'] = config['database_folder'] + os.sep + 'assay'
    config['results_folder'] = config['database_folder'] + os.sep + 'results'
    config['log_file'] = config['database_folder'] + os.sep + 'log.txt'
    config['assays_info_file'] = config['assays_folder'] + os.sep + "info.json"
    config['results_info_file'] = config['results_folder'] + os.sep + "info.json"
    config['database_version_file'] = config['database_folder'] + os.sep + "version.txt"
    config['methods_folder'] = config['database_folder'] + os.sep + "methods"
    config['temp_folder'] = "../temp"
    config['extract_files_path'] = os.path.join(config['temp_folder'], "extract_files")
    config['assays_folder'] = os.path.join(config['extract_files_path'], 'assay')
    config['assays_info_file'] = os.path.join(config['assays_folder'], "info.json")


def fill_download_paths():
    """
    Stores download paths.
    :return:
    """
    global config
    config["zip_database_adrress"] = "https://www.dropbox.com/s/2x92p8g5wrp08ln/assay.zip?dl=1"
    config["version_adrress"] = "https://www.dropbox.com/s/z4ofa2gbgw109cm/version.txt?dl=1"


if __name__ == '__main__':
    main()
