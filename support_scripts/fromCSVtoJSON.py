import csv, json, os, datetime

metadata = {
    "datasetID": "0",
    "datasetName": "My example dataset",
    "targetName": ""
}


def main():
    """
    Function which transform PubChem CSV information to VSB format.

    :return: Nothing
    """
    global metadata
    print("Hello")
    print("Please insert a path to CSV file downloaded from PubChem")
    path = str(input("Path: ")).strip("\"")
    while True:
        if not os.path.exists(path):
            print("Your path doesn't exist. Please insert the path again")
        else:
            break
    print("Making transformation. Result file will be saved in actual folder.")
    actives = []
    inactives = []
    with open(path, 'r') as csvfile:
        molecules_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        skip = True
        for row in molecules_reader:
            if skip:
                skip = False
                continue
            activity = row[3]
            cid = row[2]
            if not cid == "":
                if activity == "Active":
                    actives.append(cid)
                else:
                    inactives.append(cid)
    metadata["numberInactives"] = len(inactives)
    metadata["numberActives"] = len(actives)
    metadata["date"] = datetime.datetime.now().isoformat()
    with open("newDataset.json", "w") as stream:
        json.dump({
            "metadata": metadata,
            "data": {
                "inactive": inactives,
                "active": actives
            }
        }, stream, indent=2)


if __name__ == '__main__':
    main()
