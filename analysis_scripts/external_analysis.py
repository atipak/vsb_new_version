import argparse, json

vsb = None
general = None


def main(arguments):
    general.dbs_initialization(None, arguments["cPath"])
    with open(arguments['p']) as stream:
        inp = json.load(stream)
        action_type = inp['action']
        if action_type == "m_d_n_m":
            print("Computing...")
            method_names, analyze_names, rows, datasets = calculate_m_d_n_m_analysis(inp['methods_ids'],
                                                                                     inp['datasets_names'],
                                                                                     inp['number_molecules'])
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "action": action_type,
                    "method_names": method_names,
                    "analyze_names": analyze_names,
                    "rows": rows,
                    "datasets": datasets,
                    "methods_ids": inp['methods_ids'],
                    "datasets_names": inp['datasets_names'],
                    "number_molecules": inp['number_molecules']
                }, ostream, indent=2)
        if action_type == "method_comparison":
            print("Computing...")
            ids = inp['ids']
            dataset_option = inp['dataset_option']
            data_set_selection = {}
            met_names = []
            for i in range(len(ids)):
                data = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "method",
                    "methods_analysis": True,
                    "method_id": ids[i]
                }
                met_infos, met_names = vsb.pc_service(data, None)
                if len(met_infos) == 0:
                    continue
                for met_info in met_infos:
                    ds = met_info[5]
                    s = met_info[6]
                    if dataset_option:
                        key = ds
                    else:
                        key = ds + "/" + s
                    if key not in data_set_selection:
                        data_set_selection[key] = []
                    data_set_selection[key].append(met_info)
            data_set_selection_values, analyze_names, mms_names, methods, max_analyze_names = analysis_n_items(
                data_set_selection, met_names)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    'data_set_selection_values': data_set_selection_values,
                    'analyze_names': analyze_names,
                    'mms_names': mms_names,
                    'methods': methods,
                    'max_analyze_names': max_analyze_names,
                    'action': action_type
                }, ostream, indent=2)
        if action_type == "collapse_fingerprints":
            print("Computing...")
            ids = inp['ids']
            window_size = int(inp['window_size'])
            windows_count = int(inp['windows_count'])
            cluster = make_clustering_on_collapse_fingerprints(ids, window_size, windows_count)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    "dataset_option": dataset_option
                }, ostream, indent=2)
        if action_type == "k_sames":
            print("Computing...")
            ids = inp['ids']
            k_value = int(inp['k_value'])
            sames = find_k_sames(ids, k_value)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "sames": sames,
                    'action': action_type,
                    'k_value': int(inp['k_value'])
                }, ostream, indent=2)
        if action_type == "analysis":
            print("Computing...")
            ids = inp['ids']
            cluster = make_clustering_on_analysis(ids)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    "dataset_option": dataset_option
                }, ostream, indent=2)
        if action_type == "known_actives":
            print("Computing...")
            ids = inp['ids']
            k_value = int(inp['k_value'])
            dataset_name = inp['dataset_name']
            known_actives = known_actives_counts(dataset_name, ids, k_value)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "known_actives": known_actives,
                    "k_value": int(inp['k_value']),
                    "dataset_name": inp['dataset_name'],
                    'action': action_type
                }, ostream, indent=2)
        if action_type == "leading_first_k":
            print("Computing...")
            ids = inp['ids']
            k_value = int(inp['k_value'])
            cluster = count_of_leading_in_first_k(ids, k_value)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    'k_value': int(inp['k_value']),
                    "dataset_option": dataset_option
                }, ostream, indent=2)
        if action_type == "relative_scores":
            print("Computing...")
            ids = inp['ids']
            cluster = relative_differences_in_scores(ids)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    "dataset_option": dataset_option
                }, ostream, indent=2)
        if action_type == "inactive_before_active":
            print("Computing...")
            ids = inp['ids']
            cluster = count_of_inactive_before_active(ids)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    "dataset_option": dataset_option
                }, ostream, indent=2)
        if action_type == "known_active_same_places":
            print("Computing...")
            ids = inp['ids']
            cluster = known_active_on_same_places(ids)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    "dataset_option": dataset_option
                }, ostream, indent=2)
        if action_type == "relative_order":
            print("Computing...")
            ids = inp['ids']
            cluster = relative_order_of_active(ids)
            dataset_option = inp['dataset_option']
            if dataset_option:
                cluster = make_patterns(cluster)
            with open(arguments['op'], "w") as ostream:
                json.dump({
                    "cluster": cluster,
                    'action': action_type,
                    "dataset_option": dataset_option
                }, ostream, indent=2)


def method_informs(methods_ids, datasets_names=None):
    method_informations = {}
    method_names = []
    met_names = ()
    for i in methods_ids:
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "method",
            "methods_analysis": True,
            "method_id": i
        }
        met_infos, met_names = vsb.pc_service(data, None)
        if len(met_infos) == 0:
            continue
        for met_info in met_infos:
            if met_info[2] == "general":
                method_name = met_info[3]
            else:
                method_name = met_info[3] + "/" + met_info[4]
            if method_name not in method_names:
                method_names.append(method_name)
            ds = met_info[5]
            if datasets_names is not None:
                if ds in datasets_names:
                    append = True
                else:
                    append = False
            else:
                append = True
            if append:
                if ds not in method_informations:
                    method_informations[ds] = {method_name: [met_info]}
                else:
                    if method_name not in method_informations[ds]:
                        method_informations[ds][method_name] = [met_info]
                    else:
                        method_informations[ds][method_name].append(met_info)
    return method_informations, method_names, met_names


def calculate_m_d_n_m_analysis(methods_ids, datasets_names, number_molecules=-1):
    """
    Calculates analysis.

    :param methods_ids: IDS of methods.
    :param datasets_names: Names of data-sets.
    :return: method_names, analyze_names, rows, datasets
    """
    rows = []
    datasets = []
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "analyze",
        "id": 1
    }
    _, analyze_names = vsb.pc_service(data, None)
    analyze_names = analyze_names[2:]
    method_informations, method_names, met_names = method_informs(methods_ids, datasets_names)
    if not len(method_informations) == 0:
        for dataset in method_informations:  # add row
            columns = []
            for name in method_names:  # add column
                method_infos = method_informations[dataset][name]
                count = len(method_infos)
                max_array = [-1 for _ in range(7, len(met_names))]
                min_array = [10000000000000000000000 for _ in range(7, len(met_names))]
                avg_array = [0 for _ in range(7, len(met_names))]
                for info in method_infos:  # count cell value
                    data = {
                        "web": True,
                        "task_type": "front_end",
                        "task_subtype": "calculate",
                        "subtest_id": info[1],
                        "molecules_number": number_molecules
                    }
                    analysis_of_subtest = vsb.pc_service(data, None)
                    for i in range(7, len(met_names)):
                        avg_array[i - 7] += analysis_of_subtest[met_names[i]]
                        if max_array[i - 7] < analysis_of_subtest[met_names[i]]:
                            max_array[i - 7] = analysis_of_subtest[met_names[i]]
                        if analysis_of_subtest[met_names[i]] < min_array[i - 7]:
                            min_array[i - 7] = analysis_of_subtest[met_names[i]]
                cell = {}
                for i in range(7, len(met_names)):
                    avg_array[i - 7] /= count
                    cell[met_names[i]] = {"max": max_array[i - 7], "min": min_array[i - 7], "avg": avg_array[i - 7]}
                columns.append(cell)
            rows.append([dataset] + columns)
        for key in method_informations:
            datasets.append(key)
    return method_names, analyze_names, rows, datasets


def make_clustering_on_vectors(data, metric):
    # dataFormat= data-set : selection : method : data
    clusters_array = {}
    for dataset in data:
        data_set = data[dataset]
        clusters_array[dataset] = {}
        for selection in data_set:
            select = data_set[selection]
            clusters = []
            # insert data like clusters
            for method in select:
                clusters.append([{"name": method}, {method: select[method]}])
            while len(clusters) > 1:
                minimum = None
                minimum_cluster1 = None
                minimum_cluster2 = None
                for i in range(len(clusters)):
                    cluster1 = clusters[i][1]
                    for l in range(len(clusters)):
                        if i == l:
                            continue
                        cluster2 = clusters[l][1]
                        for method1 in cluster1:
                            for method2 in cluster2:
                                dist = metric(cluster1[method1], cluster2[method2])
                                if minimum is None:
                                    minimum = dist
                                    minimum_cluster1 = i
                                    minimum_cluster2 = l
                                else:
                                    if minimum > dist:
                                        minimum = dist
                                        minimum_cluster1 = i
                                        minimum_cluster2 = l
                new_clusters = []
                for i in range(len(clusters)):
                    if i == minimum_cluster1 or i == minimum_cluster2:
                        continue
                    else:
                        new_clusters.append(clusters[i])
                new_clusters.append(
                    [{"name": "", "children": [clusters[minimum_cluster1][0]] + [clusters[minimum_cluster2][0]]},
                     dict(clusters[minimum_cluster1][1], **clusters[minimum_cluster2][1])])
                clusters = new_clusters
            clusters_array[dataset][selection] = clusters[0][0]
    return clusters_array


def ds_s_methods(methods_ids):
    # "Method id", "Subtest id", "Method type", "Load function / Method", "Score function", "Data-set",
    # "Selection") + tuple(analysis)
    method_names = []
    method_informations = {}
    for i in methods_ids:
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "method",
            "methods_analysis": True,
            "method_id": i
        }
        met_infos, met_names = vsb.pc_service(data, None)
        if len(met_infos) == 0:
            continue
        for met_info in met_infos:
            if met_info[2] == "general":
                method_name = met_info[3]
            else:
                method_name = met_info[3] + "/" + met_info[4]
            if method_name not in method_names:
                method_names.append(method_name)
            ds = met_info[5]
            s = met_info[6]
            if ds not in method_informations:
                method_informations[ds] = {}  # {method_name: [met_info]}
            if s not in method_informations[ds]:
                method_informations[ds][s] = {}
            if method_name not in method_informations[ds][s]:
                method_informations[ds][s][method_name] = [met_info]
            else:
                method_informations[ds][s][method_name].append(met_info)
    return method_informations, method_names


def line_parsing():
    """
    Parses the command line arguments.
    :return: Dictionary with arguments
    """
    # parsing arguments
    parser = argparse.ArgumentParser(
        description='External analysis')
    parser.add_argument('-ip ', type=str, dest='p',
                        help='A path to a input file',
                        required=True)
    parser.add_argument('-op', type=str, dest='op',
                        help='A path to a output file',
                        required=False)
    parser.add_argument('-cPath ', type=str, dest='cPath',
                        help='A path to a main configuration file.',
                        required=False)
    arguments = vars(parser.parse_args())
    if "op" not in arguments or arguments['op'] is None:
        arguments["op"] = "./o.json"
    if "cPath" not in arguments or arguments['cPath'] is None:
        arguments["cPath"] = None
    return arguments


# fingerprints
def create_collapse_fingerprints(methods_ids, window_size, windows_count):
    method_informations, method_names = ds_s_methods(methods_ids)
    informations = {}
    for dataset in method_informations:
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "dataset",
            "id_dataset": dataset
        }
        r_value, dataset_info = vsb.pc_service(data, None)
        if isinstance(dataset_info, dict):
            dataset_active = dataset_info['active']
        else:
            return informations
        informations[dataset] = {}
        for selection in method_informations[dataset]:
            informations[dataset][selection] = {}
            for m in method_names:
                if m in method_informations[dataset][selection]:
                    # get molecules
                    data = {
                        "web": True,
                        "task_type": "get",
                        "task_subtype": "molecule",
                        "subtest_id": method_informations[dataset][selection][m][0][1]
                    }
                    molecules, mol_names = vsb.pc_service(data, None)
                    informations[dataset][selection][m] = create_collapse_fingerprint(molecules, window_size,
                                                                                      windows_count, dataset_active)
                else:
                    informations[dataset][selection][m] = create_collapse_fingerprint([], window_size,
                                                                                      windows_count, dataset_active)
    return informations


def create_collapse_fingerprint(molecules, window_size, windows_count, actives):
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    molecules_size = len(molecules)
    fingerprint = []
    for i in range(windows_count):
        found_actives = 0
        found_inactives = 0
        for l in range(i * window_size, (i + 1) * window_size):
            if l >= molecules_size:
                fingerprint.append(found_actives)
                fingerprint.append(found_inactives)
                break
            else:
                if molecules[i][2] in actives:
                    found_actives += 1
                else:
                    found_inactives += 1
        fingerprint.append(found_actives)
        fingerprint.append(found_inactives)
    return fingerprint


def create_known_active_vector(molecules, train_active, train_inactive):
    """
    Creates vector of number of alphabetical order of known_actives.

    :param molecules:
    :param train_active:
    :param train_inactive:
    :return: Vector
    """
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    known_molecules = list(train_active + train_inactive)
    known_molecules.sort()
    known_molecules_dict = {}
    i = 0
    for known_molecule in known_molecules:
        i += 1
        known_molecules_dict[known_molecule] = i
    vector = []
    for molecule in molecules:
        if molecule[4] in known_molecules_dict:
            vector.append(known_molecules_dict[molecule[4]])
        else:
            vector.append(0)
    return vector


def create_leading_known_active_vector(molecules, train_active, k):
    """
    Creates vector of number of leading of known actives in first K..

    :param molecules:
    :param train_active:
    :param k: Variable first K
    :return: Vector
    """
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    train_active.sort()
    known_molecules_dict = {}
    for known_active in train_active:
        known_molecules_dict[known_active] = 0
    vector = []
    xk = 1
    for molecule in molecules:
        if molecule[4] in known_molecules_dict:
            known_molecules_dict[molecule[4]] += 1
        if xk == k:
            break
        xk += 1
    for known_active in train_active:
        vector.append(known_molecules_dict[known_active])
    return vector


def create_relative_order_of_active(molecules, actives):
    """
    Creates a fingerprint of realative order of actives in tested molecules.

    :param molecules:
    :param actives:
    :return:
    """
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    i = 1
    last_i = 1
    vector = []
    for molecule in molecules:
        if molecule[2] in actives:
            vector.append(i - last_i)
            last_i = i
        i += 1
    return vector


def create_vector_of_inactive_before_active(molecules, actives):
    """
    Creates a vector of number of inactive molecules before next active one.

    :param molecules:
    :param actives:
    :return:
    """
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    number_inactive = 0
    vector = []
    for molecule in molecules:
        if molecule[2] in actives:
            vector.append(number_inactive)
            number_inactive = 0
        else:
            number_inactive += 1
    return vector


def create_vector_of_relative_scores(molecules, actives):
    """
    Creates a vector of relative differences between two following active molecules.

    :param molecules:
    :param actives:
    :return:
    """
    molecules = sorted(molecules,
                       key=lambda m: m[3],
                       reverse=True)
    if len(molecules) < 2:
        return None
    maximum = molecules[0][3]
    minimum = molecules[len(molecules) - 1][3]
    difference = maximum - minimum
    if difference == 0:
        difference = 1
    vector = []
    last_score = None
    for molecule in molecules:
        if molecule[2] in actives:
            if last_score is None:
                last_score = molecule[3]
            else:
                vector.append((last_score - molecule[3]) / difference)
    return vector


# metrics
def same_numbers_on_same_places(vector1, vector2):
    """
    Return count of numbers, which their values correspond on the same places by. Zero is not counted

    :param vector1:
    :param vector2:
    :return:
    """

    count = 0
    vector_length = len(vector1)
    for i in range(vector_length):
        if vector1[i] == 0 or vector2[i] == 0 or not vector1[i] == vector2[i]:
            count += 1
    return count


def euclid_metric(v1, v2):
    sm = 0
    if len(v1) < len(v2):
        length = len(v2)
        for i in range(len(v2) - len(v1)):
            v1.append(0)
    else:
        length = len(v1)
        for i in range(len(v1) - len(v2)):
            v2.append(0)
    for i in range(length):
        sm = (v1[i] - v2[i]) ** 2
    return sm ** (1 / 2)


# computing analysis

def find_k_sames(methods_ids, k):
    method_informations, method_names = ds_s_methods(methods_ids)
    informations = {}
    for dataset in method_informations:
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "dataset",
            "id_dataset": dataset
        }
        r_value, dataset_info = vsb.pc_service(data, None)
        if isinstance(dataset_info, dict):
            dataset_active = dataset_info['active']
        else:
            return None
        informations[dataset] = {}
        for selection in method_informations[dataset]:
            informations[dataset][selection] = {}
            for m in method_names:
                informations[dataset][selection][m] = {}
                if m not in method_informations[dataset][selection]:
                    for ma in method_names:
                        if ma == m:
                            continue
                        informations[dataset][selection][m][ma] = {"actives": [], "inactives": []}
                else:
                    for ma in method_names:
                        if ma == m:
                            continue
                        if ma not in method_informations[dataset][selection]:
                            informations[dataset][selection][m][ma] = {"actives": [], "inactives": []}
                        else:
                            f_id = method_informations[dataset][selection][m][0][1]
                            s_id = method_informations[dataset][selection][ma][0][1]
                            informations[dataset][selection][m][ma] = k_sames(f_id, s_id, k,
                                                                              dataset_active)
    return informations


def k_sames(subtest_id1, subtest_id2, k, actives):
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "molecule",
        "subtest_id": subtest_id1
    }
    molecules1, mol_names = vsb.pc_service(data, None)
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "molecule",
        "subtest_id": subtest_id2
    }
    molecules2, mol_names = vsb.pc_service(data, None)
    molecules1 = sorted(molecules1,
                        key=lambda m: m[3],
                        reverse=True)
    molecules2 = sorted(molecules2,
                        key=lambda m: m[3],
                        reverse=True)
    # "Molecule SQL ID", "ID of subtest", "molecule's name", "score", "similar molecule", "added description"
    actives_in_st = []
    inactives_in_st = []
    for i in range(k):
        mol1 = molecules1[i]
        for d in range(k):
            mol2 = molecules2[d]
            if mol1[2] == mol2[2]:
                if mol1[2] in actives:
                    actives_in_st.append(mol1[2])
                else:
                    inactives_in_st.append(mol1[2])
                break
    return {"actives": actives_in_st, "inactives": inactives_in_st}


def known_actives_counts(dataset_name, methods_ids, k):
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "dataset",
        "id_dataset": dataset_name
    }
    r_value, dataset_info = vsb.pc_service(data, None)
    if isinstance(dataset_info, dict):
        dataset_active = dataset_info['active']
    else:
        return None
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "selection",
        "dataset_selection": True,
        "id_dataset": dataset_name
    }
    r_value, selections = vsb.pc_service(data, None)
    selections_results = {}
    for selection in selections:
        selections_results[selection] = {}
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "selection",
            "id_selection": selection,
            "id_dataset": dataset_name
        }
        selection_info = vsb.pc_service(data, None)
        if isinstance(selection_info, dict):
            train = selection_info['train']
            if "active" in train:
                train_active = train["active"]
                data = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "method",
                    "methods_analysis": True,
                    "method_data": True,
                    "data_set_id": dataset_name,
                    "selection_id": selection
                }
                method_infos, method_names = vsb.pc_service(data, None)
                for active in train_active:
                    selections_results[selection][active] = {}
                    for method_info in method_infos:
                        if method_info[0] in methods_ids:
                            if method_info[2] == "general":
                                method_name = method_info[3]
                            else:
                                method_name = method_info[3] + "/" + method_info[4]
                            selections_results[selection][active][method_name] = {}
                            data = {
                                "web": True,
                                "task_type": "get",
                                "task_subtype": "molecule",
                                "subtest_id": method_info[1]
                            }
                            molecules, mol_names = vsb.pc_service(data, None)
                            molecules = sorted(molecules,
                                               key=lambda m: m[3],
                                               reverse=True)
                            if k > len(molecules):
                                k1 = len(molecules)
                            else:
                                k1 = k
                            active_count = []
                            inactive_count = []
                            for pls in range(k1):
                                if molecules[pls][4] == active:
                                    if molecules[pls][2] in dataset_active:
                                        active_count.append([pls, molecules[pls][2]])
                                    else:
                                        inactive_count.append([pls, molecules[pls][2]])
                            selections_results[selection][active][method_name]['actives'] = active_count
                            selections_results[selection][active][method_name]['inactives'] = inactive_count
            else:
                continue
    return selections_results


def analysis_n_items(data_set_selection, met_names):
    if len(met_names) > 0:
        # process data
        data_set_selection_values = {}
        methods = {}
        analyze_len = len(met_names) - 7
        mms_names = []
        for i in range(7, len(met_names)):
            mms_names.append(met_names[i])
        analyze_names = [met_names[2], met_names[3], met_names[4]]
        for i in range(7, len(met_names)):
            analyze_names.append(met_names[i])
        for key in data_set_selection:
            maxim_values = []
            maxim_names = []
            minim_values = []
            minim_names = []
            sum_values = []
            count = len(data_set_selection[key])
            # init values
            for _ in range(0, analyze_len):
                maxim_names.append("")
                minim_values.append(10000000000000000000000000)
                minim_names.append("")
                maxim_values.append(-1)
                sum_values.append(0)
            # max, min, avg analyze
            analyze_rows = []
            molecules_rows = {}
            molecule_function = []
            for dat in data_set_selection[key]:
                row = [dat[2], dat[3], dat[4]]
                for i in range(7, len(dat)):
                    row.append(dat[i])
                    if minim_values[i - 7] > dat[i]:
                        minim_values[i - 7] = dat[i]
                        minim_names[i - 7] = str(dat[3]) + "/" + str(dat[4])
                    if maxim_values[i - 7] < dat[i]:
                        maxim_values[i - 7] = dat[i]
                        maxim_names[i - 7] = str(dat[3]) + "/" + str(dat[4])
                    sum_values[i - 7] += dat[i]
                analyze_rows.append(row)
                molecule_function.append(str(dat[3]) + "/" + str(dat[4]))
                data = {
                    "web": True,
                    "task_type": "get",
                    "task_subtype": "molecule",
                    "subtest_id": dat[1]
                }
                molecules, mol_names = vsb.pc_service(data, None)
                if molecules is not None:
                    for mol in molecules:
                        if mol[2] not in molecules_rows:
                            molecules_rows[mol[2]] = [mol[3]]
                        else:
                            molecules_rows[mol[2]].append(mol[3])
            for i in range(0, len(sum_values)):
                sum_values[i] /= count
            data_set_selection_values[key] = {
                "maxim_values": maxim_values,
                "maxim_names": maxim_names,
                "minim_values": minim_values,
                "minim_names": minim_names,
                "avg_values": sum_values,
                "analyze_rows": analyze_rows,
                'molecules_rows': molecules_rows,
                'molecules_function': molecule_function
            }
            ns = {}
            for n in maxim_names:
                if n not in ns:
                    ns[n] = 1
                else:
                    ns[n] += 1
            maximum_name = ""
            maximum_value = 0
            for k in ns:
                if maximum_value < ns[k]:
                    maximum_value = ns[k]
                    maximum_name = k
            max_analyze = None
            for row in analyze_rows:
                if str(row[1]) + "/" + str(row[2]) == maximum_name:
                    max_analyze = row
                    break
            an_balances = []
            for row in analyze_rows:
                r = [row[0], row[1], row[2]]
                for pst in range(3, len(row)):
                    r.append(row[pst] - max_analyze[pst])
                an_balances.append(r)
            max_analyze = max_analyze[3:]
            max_analyze_names = analyze_names[3:]
            if maximum_name not in methods:
                methods[maximum_name] = {
                    "information": {},
                    "method_hit": 1,
                    "datasets": {
                        key: [max_analyze, an_balances]
                    }
                }
            else:
                methods[maximum_name]["method_hit"] += 1
                methods[maximum_name]["datasets"][key] = [max_analyze, an_balances]
        return data_set_selection_values, analyze_names, mms_names, methods, max_analyze_names


def make_clustering_on_collapse_fingerprints(methods_ids, window_size, windows_count):
    fingerprints = create_collapse_fingerprints(methods_ids, window_size, windows_count)
    return make_clustering_on_vectors(fingerprints, euclid_metric)


def make_clustering_on_analysis(methods_ids):
    method_informations, method_names = ds_s_methods(methods_ids)
    informations = {}
    data = {
        "web": True,
        "task_type": "get",
        "task_subtype": "analyze",
        "id": 1
    }
    _, analyze_names = vsb.pc_service(data, None)
    analysis_length = len(analyze_names)
    default_analysis = [0 for i in range(analysis_length)]
    for dataset in method_informations:
        informations[dataset] = {}
        for selection in method_informations[dataset]:
            informations[dataset][selection] = {}
            for m in method_names:
                if m in method_informations[dataset][selection]:
                    informations[dataset][selection][m] = method_informations[dataset][selection][m][7:]
                else:
                    informations[dataset][selection][m] = default_analysis
    return make_clustering_on_vectors(informations, euclid_metric)


def known_active_on_same_places(methods_ids):
    """
    Return cluster ordered by known actives on same places.

    :param methods_ids:
    :return: cluster
    """
    method_informations, method_names = ds_s_methods(methods_ids)
    informations = {}
    for dataset in method_informations:
        informations[dataset] = {}
        for selection in method_informations[dataset]:
            informations[dataset][selection] = {}
            data = {
                "web": True,
                "task_type": "get",
                "task_subtype": "selection",
                "id_dataset": dataset,
                "id_selection": selection
            }
            sel = vsb.pc_service(data, None)
            train_active = sel["train"]["active"]
            train_inactive = sel["train"]["inactive"]
            test = sel["test"]
            for m in method_names:
                if m in method_informations[dataset][selection]:
                    data = {
                        "web": True,
                        "task_type": "get",
                        "task_subtype": "molecule",
                        "subtest_id": method_informations[dataset][selection][m][0][1]
                    }
                    molecules, mol_names = vsb.pc_service(data, None)
                    fingerprint = create_known_active_vector(molecules, train_active, train_inactive)
                else:
                    fingerprint = []
                    for _ in test:
                        fingerprint.append(0)
                informations[dataset][selection][m] = fingerprint
    return make_clustering_on_vectors(informations, same_numbers_on_same_places)


def count_of_leading_in_first_k(methods_ids, k):
    """
    Return cluster created by count of leading of known actives in first k molecules.

    :param methods_ids:
    :param k: Variable K
    :return:
    """
    method_informations, method_names = ds_s_methods(methods_ids)
    informations = {}
    for dataset in method_informations:
        informations[dataset] = {}
        for selection in method_informations[dataset]:
            informations[dataset][selection] = {}
            data = {
                "web": True,
                "task_type": "get",
                "task_subtype": "selection",
                "id_dataset": dataset,
                "id_selection": selection
            }
            sel = vsb.pc_service(data, None)
            train_active = sel["train"]["active"]
            for m in method_names:
                if m in method_informations[dataset][selection]:
                    data = {
                        "web": True,
                        "task_type": "get",
                        "task_subtype": "molecule",
                        "subtest_id": method_informations[dataset][selection][m][0][1]
                    }
                    molecules, mol_names = vsb.pc_service(data, None)
                    fingerprint = create_leading_known_active_vector(molecules, train_active, k)
                else:
                    fingerprint = []
                    for _ in train_active:
                        fingerprint.append(0)
                informations[dataset][selection][m] = fingerprint
    return make_clustering_on_vectors(informations, euclid_metric)


def relative_order_of_active(methods_ids):
    """
    Return cluster created by relative order of active molecules.

    :param methods_ids:
    :return:
    """
    informations = common_computing_with_actives(methods_ids, create_relative_order_of_active)
    return make_clustering_on_vectors(informations, euclid_metric)


def count_of_inactive_before_active(methods_ids):
    """
    Returns cluster created by count of inactive molecules before ith active.

    :param methods_ids:
    :return:
    """
    informations = common_computing_with_actives(methods_ids, create_vector_of_inactive_before_active)
    return make_clustering_on_vectors(informations, euclid_metric)


def relative_differences_in_scores(methods_ids):
    """
    Returns cluster created by differences between scores of two following active molecules.

    :param methods_ids:
    :return:
    """
    informations = common_computing_with_actives(methods_ids, create_vector_of_relative_scores)
    return make_clustering_on_vectors(informations, euclid_metric)


def common_computing_with_actives(methods_ids, fingerprint_creator):
    """
    Returns dictionary with fingerprints of all selection and methods, which were launched.

    :param methods_ids:
    :param fingerprint_creator: Function for creating vectors.
    :return:
    """
    method_informations, method_names = ds_s_methods(methods_ids)
    informations = {}
    for dataset in method_informations:
        informations[dataset] = {}
        data = {
            "web": True,
            "task_type": "get",
            "task_subtype": "dataset",
            "id_dataset": dataset
        }
        r_value, d = vsb.pc_service(data, None)
        actives = d["active"]
        for selection in method_informations[dataset]:
            informations[dataset][selection] = {}
            for m in method_names:
                if m in method_informations[dataset][selection]:
                    data = {
                        "web": True,
                        "task_type": "get",
                        "task_subtype": "molecule",
                        "subtest_id": method_informations[dataset][selection][m][0][1]
                    }
                    molecules, mol_names = vsb.pc_service(data, None)
                    fingerprint = fingerprint_creator(molecules, actives)
                else:
                    fingerprint = []
                    for _ in actives:
                        fingerprint.append(0)
                informations[dataset][selection][m] = fingerprint
    return informations


def make_patterns(cluster):
    """
    Takes a cluster a creates patterns from selections.


    :param cluster:
    :return:
    """
    pattern_text = "Pattern "
    datasets_patterns = {}
    for dataset in cluster:
        dataset_cluster = cluster[dataset]
        l = 0
        # dataset_pattern = [selections, pattern]
        dataset_patterns = {}
        for selection in dataset_cluster:
            selection_cluster = dataset_cluster[selection]
            inserted = False
            for key in dataset_patterns:
                dataset_pattern = dataset_patterns[key]
                if selection_cluster == dataset_pattern[1]:
                    dataset_pattern[0].append(selection)
                    inserted = True
                    break
            if not inserted:
                key = pattern_text + str(l)
                l += 1
                dataset_patterns[key] = ([[selection], selection_cluster])
        datasets_patterns[dataset] = dataset_patterns
    return datasets_patterns


def name_main():
    global vsb
    global general
    import sys
    sys.path.append("../src")
    vsb = __import__("main")
    general = __import__("general")
    arguments = line_parsing()
    main(arguments)


if __name__ == '__main__':
    name_main()
