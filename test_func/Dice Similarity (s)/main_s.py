from rdkit import DataStructs


def scoring(repr, train, valid, test):
    """
    A method for computing of scores: DiceSimilarity

    :param repr: A dictionary with representations of molecules from loading function
    :param train: A dictionary with lists of active, inactive train molecules names
    :param valid: A dictionary with lists of active, inactive validation molecules names
    :param test: A list with test molecules names
    :return: A list with dictionaries, where keys are "name", "similarity"[, "score_molecule", "description"]
    """
    active = train['active']
    result = []
    for t in test:
        molecule_max = None
        value_max = -1
        for act in active:
            sim = DataStructs.DiceSimilarity(repr[t], repr[act])
            if value_max < sim:
                value_max = sim
                molecule_max = act
        result.append({
            'name': t,
            'similarity': value_max,
            'score_molecule': molecule_max
        })
    return result
