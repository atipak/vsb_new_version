from rdkit.Chem.AtomPairs import Torsions


def loading(molecule):
    """
    A method for representation of molecules:  TopologicalTorsionFingerprint

    :param molecule: A RDkit molecule object
    :return: Representation of molecule.
    """
    if molecule is None:
        return None
    fp = Torsions.GetTopologicalTorsionFingerprintAsIntVect(molecule)
    return fp
