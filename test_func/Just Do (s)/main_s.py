

def scoring(repr, train, valid, test):
    """
    Test method for testing the benchmark on computers without an access to library RDkit.
    There are some loops which full fill processor unit time. After that test data are randomly evaluated by values 0 and 1.
    Results are getting back.

    :param repr: A dictionary with representations of molecules from loading function
    :param train: A dictionary with lists of active, inactive train molecules names
    :param valid: A dictionary with lists of active, inactive validation molecules names
    :param test: A list with test molecules names
    :return: A list with dictionaries, where keys are "name", "similarity"[, "score_molecule", "description"]
    """
    active = train['active']
    import random
    actives = []
    for act in active:
        actives.append(act)
    rs = ["Nice day", "Hello world", "Good day", "Good night", "Korytnacka"]
    result = []
    somset = []
    for t in test:
        for act in actives:
            k = int(random.randint(5, 1000200)) * 1000000 / 20000 + 3
            k += 1
            k += 2
            if k < 2005202:
                somset.append(True)
            else:
                somset.append(False)
        result.append({
            'name': t,
            'similarity': random.randint(0, 1),
            'score_molecule': actives[random.randint(0, len(actives) - 1)],
            'description': rs[random.randint(0, len(rs) - 1)]
        })
    return result
