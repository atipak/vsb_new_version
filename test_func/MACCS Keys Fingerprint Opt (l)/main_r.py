from rdkit.Chem import AllChem


def loading(molecule):
    """
    A method for representation of molecules: MACCSKeysFingerprint

    :param molecule: A RDkit molecule object
    :return: Representation of molecule.
    """
    if molecule is None:
        return None
    fp = AllChem.GetMACCSKeysFingerprint(molecule)
    return fp
