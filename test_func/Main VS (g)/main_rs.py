#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

"""

import argparse
import os
import json
import logging
import time
import rdkit
import rdkit.Chem
from rdkit.ML.Scoring import Scoring

__author__ = 'Petr skoda'
__license__ = 'X11'
__version__ = '1.0.1'


# region Representation and similarity computation

def _create_representation(molecule):
    """Create molecular representation.

    :param molecule:
    :return:
    """
    # For each index we will store a number of occurrences.
    representation = {}
    # Get connected pairs of atoms.
    pattern = rdkit.Chem.MolFromSmarts('*~*~*')
    fragments = molecule.GetSubstructMatches(pattern)
    # For each file compute anm index object.
    for atoms in fragments:
        # We use hybridization as a feature.
        values = [molecule.GetAtomWithIdx(atoms[0]).GetHybridization(),
                  molecule.GetAtomWithIdx(atoms[1]).GetHybridization(),
                  molecule.GetAtomWithIdx(atoms[2]).GetHybridization()]
        values = sorted(values)
        # Hybridization has values <0, 7> so we use 4 bits to represent each
        # atom.
        index = values[0] + 16 * values[1] + 256 * values[2]
        if index not in representation:
            representation[index] = 0
        representation[index] += 1
    return representation


def _similarity(left, right):
    """Computes molecule similarity.

    :param left: Output of :func:'_create_representation'
    :param right: Output of :func:'_create_representation'
    :return: Value from range <0,1>
    """
    # The representation is number of indexes in common
    # divided by the larger number of indexes. This value is for sure
    # smaller than 1 and is 1 if they are the same.
    in_common = sum([min(left[item], right[item])
                     for item in left if item in right])
    total = max(sum([left[key] for key in left]),
                sum([right[key] for key in right]))
    return float(in_common) / float(total)


# endregion

def _load_molecules(sdf_path):
    """Load molecules from SDF file.

    :param sdf_path:
    :return: Molecules in dictionary under their names.
    """
    result = {}
    # Convert path to str os in come cases (Python 2.7, Win 7) the sdf_path
    # can be of type unicode and tha call would fail on invalid
    # argument type.
    for molecule in rdkit.Chem.SDMolSupplier(str(sdf_path)):
        if molecule is None:
            logging.error("Can't load molecule.")
            print("Can't load molecule.")
            continue
        result[molecule.GetProp('_Name')] = molecule
    return result


def screening(input_directory, output_path=None):
    """Perform a virtual screening.

    :param input_directory:
    :param output_path:
    :return:
    """
    with open(input_directory) as input_stream:
        definition = json.load(input_stream)
    # Load molecules.
    logging.info('Loading molecules ...')
    molecules = {}
    for name in definition['data']['files']:
        path = name['path']
        if not os.path.exists(path):
            logging.error('Missing file: %s' % name)
            raise Exception('Missing file.')
        molecules.update(_load_molecules(path))
    # Create representation of active molecules.
    actives = []
    for name in definition['data']['train']['actives']:
        if name not in molecules:
            print(name, ' not found')
            continue
        actives.append(_create_representation(molecules[name]))
    # Screening.
    logging.info('Screening ...')
    scores = []
    counter = 0
    counter_max = len(definition['data']['test'])
    counter_step = counter_max / 10.0
    time_begin = time.clock()
    for item in definition['data']['test']:
        if item not in molecules:
            print(item, ' not found')
            continue
        query = _create_representation(molecules[item])
        similarity = max([_similarity(query, active) for active in actives])
        scores.append({
            'name': item,
            'similarity': similarity,
        })
        if counter % counter_step == 0:
            logging.debug('%d/%d', counter, counter_max)
        counter += 1
    time_end = time.clock()
    # Evaluate screening.
    scores = sorted(scores,
                    key=lambda m: m['similarity'],
                    reverse=True)
    # Print results.
    print('Execution time : %.2fs' % (time_end - time_begin))
    # Write result to a file.
    if not output_path is None and not output_path == '':
        pathDir = os.path.dirname(output_path)
        if not os.path.exists(os.path.dirname(output_path)) and not pathDir == '':
            os.makedirs(os.path.dirname(output_path))
        with open(output_path, 'w') as output_stream:
            json.dump({
                'data': scores,
                'metadata': {                    
                    'fileName': os.path.basename(__file__),
                    'executionTime': time_end - time_begin,
                }
            }, output_stream, indent=2)


def _read_configuration():
    """Read command line arguments and return configuration object.

    :return:
    """
    parser = argparse.ArgumentParser(
        description='Perform a simple virtual screening.')
    parser.add_argument('-i', type=str, dest='input',
                        help='Path to input data directory (example_data).',
                        required=True)
    parser.add_argument('-o', type=str, dest='output',
                        help='Path to the output file (output.json).',
                        required=True)

    return vars(parser.parse_args())


def _main():
    """Application entry point.

    """
    # Initialize logging.
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s [%(levelname)s] %(module)s - %(message)s',
        datefmt='%H:%M:%S')
    #
    config = _read_configuration()
    screening(config['input'], config['output'])


if __name__ == '__main__':
    _main()
