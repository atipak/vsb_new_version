def loading(sdf_path):
    """
    Test method for testing the benchmark on computers without an access to library RDkit.
    There are some loops which full fill processor unit time. After that test data are randomly evaluated by values 0 and 1.
    Results are getting back.

    :param sdf_path: A path to a a SDF file with molecules description
    :return: A dictionary with representation of molecules from SDF file
    """
    repr = {}
    for item in range(1, 500):
        for i in range(100000):
            if i % 100 == 25:
                t = i * 37
                t /= 2
                t = i - i * t
                t = 23 - t
        repr[str(item)] = item
    return repr


