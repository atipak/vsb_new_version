import rdkit
from rdkit import Chem


def loading(sdf_path):
    """
    A method for representation of molecules: Count of atoms in molecule

    :param sdf_path: A path to a a SDF file with molecules description
    :return: A dictionary with representation of molecules from SDF file
    """
    repr = {}
    for molecule in rdkit.Chem.SDMolSupplier(sdf_path):
        if molecule is None:
            continue
        mol = [0 for i in range(0, 118)]
        for atom in molecule.GetAtoms():
            mol[atom.GetAtomicNum()] += 1
            mol[0] += atom.GetNumImplicitHs()
        repr[molecule.GetProp('_Name')] = mol
    return repr


