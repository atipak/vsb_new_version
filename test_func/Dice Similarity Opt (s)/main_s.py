from rdkit import DataStructs


def scoring(train_molecules, validation_molecules, test_repr, mol_name):
    """
    A method for computing of scores: DiceSimilarity

    :param train_molecules: A dictionary with lists of active, inactive train molecules like a dictionary with reprezentation key and name key
    :param validation_molecules: A dictionary with lists of active, inactive valid molecules like a dictionary with reprezentation key and name key
    :param test_repr: A test molecule in representation form
    :param mol_name: A name of test molecule
    :return: A dictionary with keys "name", "similarity"[, "score_molecule", "description"]
    """
    molecule_max = None
    value_max = -1
    for act in train_molecules['active']:
        sim = DataStructs.DiceSimilarity(test_repr, act['reprezentation'])
        if value_max < sim:
            value_max = sim
            molecule_max = act['name']
    return {
        'name': mol_name,
        'similarity': value_max,
        'score_molecule': molecule_max
    }
