import rdkit
from rdkit import Chem
from rdkit.Chem import AllChem



def loading(sdf_path):
    """
    A method for representation of molecules: Morgan fingerprints

    :param sdf_path: A path to a a SDF file with molecules description
    :return: A dictionary with representation of molecules from SDF file
    """
    repr = {}
    for molecule in rdkit.Chem.SDMolSupplier(sdf_path):
        if molecule is None:
            continue
        fp = AllChem.GetMorganFingerprint(molecule, 2)
        repr[molecule.GetProp('_Name')] = fp
    return repr


