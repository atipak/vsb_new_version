# imports

import multiprocessing
import argparse
from general import *

# main
i = 0


def main_local(test_input=None):
    """
    Loading of tests.
    :param test_input: Test input.
    :return: Nothing
    """
    # tasks file reading
    if test_input is None:
        if len(sys.argv) < 3:
            sys.tracebacklimit = None
            raise Exception("Not enough arguments for program executing")
        else:
            test_input = sys.argv[2]
    inp = read_input(test_input)
    tests = inp[1]
    metadata = inp[0]
    main_service(tests, metadata)


def main_service(tests, metadata):
    """
    Launches read tests.

    :param tests: Tests configurations
    :param metadata: Metadata to tests
    :return:
    """
    metadata['db_config_path'] = get_config_path()
    if 'parallel_tests' in metadata and metadata['parallel_tests'] == "True":
        act_test = 0
        processes = []
        while act_test < len(tests):
            processes, act_procs = active_processes(processes)
            if act_procs < multiprocessing.cpu_count():
                test = tests[act_test]
                if test['task_type'] == 'run':
                    test = id_for_test(test)
                proc = multiprocessing.Process(target=pc_service, args=(test, metadata))
                # proc.stdout = open("{0}_test".format(i), "w")
                proc.start()
                processes.append(proc)
                act_test += 1
            else:
                time.sleep(10)
    else:
        for test in tests:
            if test['task_type'] == 'run':
                test = id_for_test(test)
            pc_service(test, metadata)
    clean_temp_folder()


def pc_service(test, metadata):
    """
    Running of tasks
    :param test: The test configuration
    :param metadata: The test metadata.
    :return:
    """
    global i
    i += 1
    # sys.stdout = open("{0}_test".format(i), "w")

    if test['task_type'] == 'run':
        _ = False
        context = test['test_id']
        try:
            if "parallel_pc" in test and test["parallel_pc"] == "True" and 'pc_to_use' in test:
                import remote_launching
                _, context = remote_launching.multi_pc_service(test, metadata['db_config_path'])
            elif "metacentrum" in test and test["metacentrum"] == "True":
                import metacentrum
                _, context = metacentrum.metacentrum_services(test, metadata)
            else:
                import local_launching
                _, context = local_launching.local_pc_service(test, metadata['db_config_path'])
        except Exception as e:
            context = str(e)
            raise e
        finally:
            if "fc_ofile" in test:
                import middle_level_control
                middle_level_control.write_info_about_test(context, test['fc_ofile'], True)
            elif "web" in test:
                import middle_level_control
                return middle_level_control.write_info_about_test(context, "", False)

    else:
        import middle_level_control
        if "web" in test:
            return middle_level_control.database_actions(test, False)
        else:
            middle_level_control.database_actions(test, True)


def main_func(arguments):
    """
    Initialization od application.
    :param arguments:
    :return:
    """
    configuration_path = None
    if "cPath" in arguments:
        configuration_path = arguments["cPath"]
    dbs_initialization(arguments['mode'], configuration_path)
    # interactive mode will be activated
    if "mode_i" in arguments and arguments["mode_i"] == True:
        main_interactive()
    # auto mode will be activated
    elif 'mode_a' in arguments and "file" in arguments and arguments["mode_a"] == True:
        main_local(arguments["file"])
    elif 'mode_g' in arguments and arguments['mode_g'] == True:
        print("Importing web sources ...")
        import web_interface.web as web
        web.main(configuration_path)
    else:
        print("Argument wasn't recognized. Launched interactive mode. For next help put ? on the command line.")
        main_interactive()


def main_interactive():
    """
    Main function for interactive console mode.
    :return: Nothing
    """
    import interactive_control
    while True:
        print(
            "What would you like to do? For choice of an option insert a characteristic number or a word by the documentation:")
        print("g. get")
        print("p. put")
        print("r. run")
        print("e. exit")
        print("h. help")
        print("m. managing")
        print("d. delete")
        s = str(input('>>>')).strip().lower()
        if s == "?" or s == "h" or s == "help":
            interactive_control.show_help()
        elif s == "get" or s == "g":
            interactive_control.do_get()
        elif s == "add" or s == "a":
            interactive_control.do_put()
        elif s == "run" or s == "r":
            interactive_control.do_run()
        elif s == "exit" or s == "e":
            exit()
        elif s == "managing" or s == "m":
            interactive_control.do_man()
        elif s == "delete" or s == "d":
            interactive_control.do_delete()
        else:
            print("Command not recognized. For help write '?'")


def main(line_arguments=None):
    """
    Main function
    :return: Nothing
    """
    cur_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(cur_path)
    parent_path = os.path.dirname(cur_path)
    sys.path.append(cur_path)
    sys.path.append(parent_path)
    begin = time.time()
    main_func(line_arguments)
    print(time.time() - begin)


def line_parsing():
    """
    Parses the command line arguments.
    :return: Dictionary with arguments
    """
    # parsing arguments
    parser = argparse.ArgumentParser(
        description='Benchmark of virtual screening.')
    inter = parser.add_argument_group("An interactive mode")
    inter.add_argument('-i', action="store_true", dest='mode_i',
                       help='An interactive mode.',
                       required=False)
    automatic = parser.add_argument_group("An auto mode")
    automatic.add_argument('-a', action="store_true", dest='mode_a',
                           help='An automatic mode.',
                           required=False)
    automatic.add_argument('-file ', type=str, dest='file',
                           help='A path to a tasks file.',
                           required=False)
    parser.add_argument('-cPath ', type=str, dest='cPath',
                        help='A path to a main configuration file.',
                        required=False)
    parser.add_argument('-g', action="store_true", dest='mode_g',
                        help='A graphic mode.',
                        required=False)
    arguments = vars(parser.parse_args())
    # initialization of databases
    # path to configuration file wasn't changed
    mode = "i"
    if "mode_i" in arguments and arguments["mode_i"] == True:
        mode = "i"
    if 'mode_a' in arguments and arguments["mode_a"] == True:
        mode = "a"
    if 'mode_g' in arguments and arguments['mode_g'] == True:
        mode = "g"
    arguments['mode'] = mode
    return arguments


if __name__ == '__main__':
    arguments = line_parsing()
    main(arguments)
