import subprocess
import skeleton
import multiprocessing
from general import *

number_of_cores = -1


def local_pc_service(config, dbs_config_file):
    """
    Launches main testing. By configuration parallel tests or sequential tests for the given repeat number.

    :param dbs_config_file: A path to the configuration file for databases.
    :param config: Basic input configuration
    :return: Nothing, SQL ID of test
    """
    # spusteni vice vyberu najednou
    if config['parallel_selections'] == "True":
        par_sel = True
        test_type = sql_database.DatabaseHelper.Test_types.local_paralel
    # spousteni vyberu probiha sekvencne
    else:
        par_sel = False
        test_type = sql_database.DatabaseHelper.Test_types.local_series
    # nacteni zakladni konfigurace a informaci
    config, select, recid_test = start_and_check_test(config, test_type)
    check_temp_folder()
    if dbs_config_file is not None:
        prepare_databases(dbs_config_file)
    set_number_of_cores(dbs_config_file)
    ret_dict = {}
    succeded = 0
    running = 0
    ended = 0
    num_selected = len(select)
    max_number = get_max_repeat(config)
    if par_sel:
        ret_dict, processes = local_pc_parallel(config, select, config['first_result'], config['test_id'],
                                                config['test_path'],
                                                recid_test, dbs_config_file)
        # uspavat hlavni vlakno dokud existuje jeste nejaky spusteny test na vyberu
        processes, running = active_processes(processes)
        while not running == 0:
            print('Main thread is waiting next 5s for finishing spawned executing processes')
            time.sleep(5)
            processes, running = active_processes(processes)
    else:
        ret_dict = local_pc_sequential(config, select, config['first_result'], config['test_id'], config['test_path'],
                                       recid_test)
    # opakovat testy dokud neni dostatecny pocet vysledku
    succeded += get_successful_processes(ret_dict)
    ended += get_ended_processes(ret_dict)
    while actual_done(config, ended, succeded) < int(config['repeat']):
        # novy vyber vyberu
        selected = choose_selections_after(
            int(config['repeat']) - succeded, select)
        if par_sel:
            ret_dict, processes = local_pc_parallel(config, selected['new_selected'],
                                                    config['first_result'] + ended, config['test_id'],
                                                    config['test_path'], recid_test, dbs_config_file)
            processes, running = active_processes(processes)
            while not running == 0:
                print('Main thread is waiting next 5s for finishing spawned executing threads')
                time.sleep(5)
                processes, running = active_processes(processes)
        else:
            ret_dict = local_pc_sequential(config, selected['new_selected'],
                                           config['first_result'] + ended, config['test_id'],
                                           config['test_path'], recid_test)
        num_selected = len(selected['all'])
        succeded += get_successful_processes(ret_dict)
        ended += get_ended_processes(ret_dict)
        if max_number < num_selected:
            print("Too many selections used")
            return False, recid_test
    return True, recid_test


def local_pc_sequential(config, selections, first_result, id_test, test_path, recid_test):
    """
    Launches n tests on given selections sequentially in for cycle.

    :param config: Basic configuration.
    :param selections: Selections for testing
    :param first_result: Indicates first number which can be used for id of test
    :param id_test: ID of test in file database
    :param test_path: Path to the folder of test
    :param recid_test: ID of test in SQL database
    :return: Nothing
    """
    i = 0
    ret_dict = dict()
    for selection in selections:
        if run_on_selection(config, selection['dataset'], selection['selection'], first_result + i, id_test, test_path,
                            recid_test):
            ret_dict[str(i)] = True
        else:
            ret_dict[str(i)] = False
        i += 1
    return ret_dict


def local_pc_parallel(config, selections, first_result, id_test, test_path, recid_test, dbs_config_file):
    """
    Launches n tests on given selections parallelly. Count of task will be less than or equal to count of processors.
    :param config: Basic configuration.
    :param selections: Selections for testing
    :param first_result: Indicates first number which can be used for id of test
    :param id_test: ID of test in file database
    :param test_path: Path to the folder of test
    :param recid_test: ID of test in SQL database
    :param dbs_config_file: A path to a database configuration file
    :return: Multiprocessing return dictionary.
    """
    i = 0
    processes = []
    try:
        manager = multiprocessing.Manager()
    except Exception as e:
        print(e)
    ret_dict = manager.dict()
    while i < len(selections):
        processes, act_procs = active_processes(processes)
        if act_procs < get_number_of_cores():
            t = multiprocessing.Process(target=run_on_selection, args=(config, selections[i]['dataset'],
                                                                       selections[i]['selection'], first_result + i,
                                                                       id_test,
                                                                       test_path, recid_test, dbs_config_file,
                                                                       ret_dict))
            t.name = "process_" + str(i)
            t.start()
            processes.append(t)
            i += 1
        else:
            print("Number active threads:", len(ret_dict))
            time.sleep(10)
    return ret_dict, processes


def run_on_selection(config, dataset, selection, id_result_file, id_test, test_path, recid_test, dbs_config_file=None,
                     ret_dict=None):
    """
    Performs test on one selection.

    :param recid_test: SQL id of test
    :param ret_dict: A proxy dictionary for getting result
    :param config: Basic configuration.
    :param dataset: Test data set.
    :param selection: Test selection.
    :param id_result_file: ID of result.
    :param id_test: ID of test.
    :param test_path: Path to the folder from test.
    :param dbs_config_file: A path to a database configuration file
    :return: Nothing
    """
    # basic setting
    if dbs_config_file is not None:
        prepare_databases(dbs_config_file)
    # basic paths
    temp_path = get_temp_folder_path() + os.sep + str(id_test)
    name_of_result_file = 'test_' + str(id_result_file) + '.json'
    input_data_path = temp_path + os.sep + 'input_' + str(id_result_file) + '.json'
    result_file_path = temp_path + os.sep + name_of_result_file
    # getting data
    train = file_database.get_selection_train(dataset, selection)
    valid = file_database.get_selection_valid(dataset, selection)
    test = file_database.get_selection_test(dataset, selection)
    metadata = []
    # zapsani do konfiguracniho slovniku result_path a sdf_path
    # slouzi pouze pro vykonavani typu skeleton
    if config['typefile'] == "python":
        config['output'] = result_file_path
        config['sdf'] = file_database.get_path_sdf(dataset)
    # vytvoreni vstupu pro obecny soubor
    if config['typefile'] == "general":
        file_formats.to_json(train, valid, test, metadata, input_data_path, file_database.get_path_sdf(dataset))
    try:
        start_time = time.time()
        ### skeleton procedure
        if config['typefile'] == "python":
            print("Executing of skeleton on dataset ", str(dataset), " and selection ", selection)
            # optimization
            if config['optimalization'] == 'yes':
                ok = skeleton.execute_opt(config, train, valid, test)
                if not ok:
                    if ret_dict is not None:
                        ret_dict[multiprocessing.current_process().name] = False
                    return False
            # without optimization
            else:
                ok = skeleton.execute_no_opt(config, train, valid, test)
                if not ok:
                    if ret_dict is not None:
                        ret_dict[multiprocessing.current_process().name] = False
                    return False
        # common procedure
        elif config['typefile'] == "general":
            try:
                print("Executing of general file on dataset ", str(dataset), " and selection ", selection)
                r_value, filename = get_main_file(config['file_method_path'])
                file_module_path = os.path.join(config['file_method_path'], filename)
                completed_process = subprocess.run(
                    [config['execute_command'], file_module_path, '-i', input_data_path, '-o', result_file_path],
                    stdout=subprocess.PIPE,
                    universal_newlines=True)
                oput = completed_process.stdout
                print("The begin of the user program output")
                print(oput)
                print("The end of the user program output")
                completed_process.check_returncode()
            # if return code returned by method check_returncode is bad, Exception will be raised by this method
            except Exception as e:
                print(e)
                print("Error in executing!")
                if ret_dict is not None:
                    ret_dict[multiprocessing.current_process().name] = False
                return False
        runtime = int(time.time() - start_time)
        # inserting data to SQL database
        if insert_data_into_sql_database(result_file_path, id_test, config['typefile'], dataset, selection, test_path,
                                         id_result_file, recid_test, runtime):
            if ret_dict is not None:
                print("Process with name {0} succeeded.".format(multiprocessing.current_process().name))
                ret_dict[multiprocessing.current_process().name] = True
            return True
        else:
            if ret_dict is not None:
                print("Process with name {0} did not succeed.".format(multiprocessing.current_process().name))
                ret_dict[multiprocessing.current_process().name] = False
            return False
    except SystemExit or BaseException as s:
        print(s)
        exit(-1)
    except Exception as e:
        print('Problem in dataset ', dataset, ' and selection ', selection)
        print(e)
        if ret_dict is not None:
            ret_dict[multiprocessing.current_process().name] = False
        return False


def get_ended_processes(dict_processes):
    """
    Return count of ended processes.

    :param dict_processes: Dictionary with results of processes.
    :return: Count of ended processes.
    """
    return len(dict_processes)


def get_successful_processes(dict_processes):
    """
    Returns count of successful processes.

    :param dict_processes: Dictionary with results of processes.
    :return: Count of successful processes
    """
    s = 0
    keys = dict_processes.keys()
    for key in keys:
        if dict_processes[key]:
            s += 1
    return s


def get_main_file(root_of_tree):
    """
    Returns a main launching file.

    :param root_of_tree: Main folder of method.
    :return: True/False, filename
    """
    files = os.listdir(root_of_tree)
    for file in files:
        if not file.find("main") == -1:
            return True, os.path.basename(file)
    return False, ""


def get_number_of_cores():
    """
    Returns number of cores.

    :return:
    """
    global number_of_cores
    return number_of_cores


def set_number_of_cores(config_file_path):
    """
    Sets number of cores variable.

    :param config_file_path: Main config file path
    :return:
    """
    global number_of_cores
    if config_file_path is not None:
        with open(config_file_path) as stream:
            js = json.load(stream)
            if "cores" in js:
                if js["cores"] == -1:
                    number_of_cores = multiprocessing.cpu_count()
                else:
                    number_of_cores = js["cores"]
            else:
                number_of_cores = multiprocessing.cpu_count()
    else:
        number_of_cores = multiprocessing.cpu_count()

