import json
import sys
import zipfile
import os


def _read_dataset_from_json(json_name):
    """
    Read data set from json file.
    :param json_name: Name of the json file.
    :return: List which contains train and test lists of molecules
    """
    with open(json_name, "r") as file:
        json_file = json.load(file)
        train = json_file['data']['train']
        test = json_file['data']['test']
        validation = json_file['data']['validation']
        dataset = {"train": train, "test": test, "validation": validation}
        return dataset


def _read_config_from_json(json_name):
    """
    Read configuration from json file.
    :param json_name: Name of the json file.
    :return: Dictionary with configuration
    """
    with open(json_name, "r") as file:
        json_file = json.load(file)
        return json_file


def python_optimalization(config):
    """
    Reads data set from file and runs it on module skeleton with given configuration. Optimized version of test.
    :param config: Configuration of test
    :return: True or false on the basis of success of test.
    """
    try:
        import skeleton
        dataset = _read_dataset_from_json(config['f_selection'])
        ok = skeleton.execute_opt(config, dataset["train"], dataset["validation"], dataset["test"])
        return ok
    except Exception as e:
        print(e)
        return False


def python(config):
    """
    Reads data set from file and runs it on module skeleton with given configuration. Non-optimized version of test.
    :param config: Configuration of test
    :return: True or false on the basis of success of test.
    """
    try:
        import skeleton
        dataset = _read_dataset_from_json(config['f_selection'])
        ok = skeleton.execute_no_opt(config, dataset["train"], dataset["validation"], dataset["test"])
        return ok
    except Exception as e:
        print(e)
        return False


def general(config, grid=False):
    """
    Runs test in subprocess with given configuration.
    :param config: Configuration of test
    :return: True or false on the basis of success of test.
    """
    import subprocess
    r_value, filename = get_main_file(config['file_method_path'])
    file_module_path = os.path.join(os.path.basename(config['file_method_path']), filename)
    try:
        if not grid:
            completed_process = subprocess.Popen(
                [config['execute_command'], file_module_path, '-i', config['input'], '-o', config['result']],
                universal_newlines=True)
            ret_code = completed_process.wait()
            if ret_code == 0:
                return True
            else:
                return False
        else:
            completed_process = subprocess.Popen(
                [config['execute_command'], file_module_path, '-i', config['input'], '-o', config['result']],
                universal_newlines=True)
            (stdoutdata, stderrdata) = completed_process.communicate()
            print("Stdout: \n" + stdoutdata)
            print("Stderr: \n" + stderrdata)
            if completed_process.returncode == 0:
                return True
            else:
                return False
    except Exception as e:
        print(e)
        return False


def main():
    """
    Main function for launching a test on selection on a remote computer machine. Program data flow is driven by
    configuration.
    :return: Nothing
    """
    config_file = sys.argv[2]
    config = _read_config_from_json(config_file)
    if not sys.argv[1] == "-m":
        # redirecting standards outputs
        vsb_stderr = open(config['vsb_stderr'], "a")
        vsb_stdout = open(config['vsb_stdout'], "a")
        stderr = open(config['stderr'], "a")
        stdout = open(config['stdout'], "a")
        old_stderr = sys.stderr
        old_stdout = sys.stdout
        sys.stdout = vsb_stdout
        sys.stderr = vsb_stderr
    # launching
    if sys.argv[1] == '-s':
        print("Option s activated")
        success = False
        # extracting files
        print("Extracting files")
        zip_file = zipfile.ZipFile(config['zip_file'])
        zip_file.extractall(path=os.path.dirname(config['zip_file']))
        # set feature running
        print("Setting state")
        with open(config['state'], "w") as check_file:
            check_file.write('running')
        # launching
        print("Launching method")
        sys.stdout = stdout
        sys.stderr = stderr
        if config['file_type'] == 'python':
            if config['optimalization'] == 'yes':
                success = python_optimalization(config)
                with open(config['state'], "w") as check_file:
                    check_file.write('finished')
            else:
                success = python(config)
                with open(config['state'], "w") as check_file:
                    check_file.write('finished')
        elif config['file_type'] == "general":
            success = general(config)
            with open(config['state'], "w") as check_file:
                check_file.write('finished')
        sys.stdout = vsb_stdout
        sys.stderr = vsb_stderr
        print("Writing result of test")
        with open(config['success'], "w") as file:
            file.write(str(success))
    # test of running
    elif sys.argv[1] == '-z':
        print("Option z activated")
        if not os.path.exists(config['state']):
            print("State file wasn't found")
            exit()
        with open(config['state'], "r") as check_file:
            ans = check_file.read()
            sys.stdout = old_stdout
            if ans == "running":
                print("yes")
            elif ans == "finished":
                print("no")
            else:
                print("ERROR")
            sys.stdout = vsb_stdout
    # test of finish
    elif sys.argv[1] == '-u':
        print("Option u activated")
        with open(config['success'], "r") as s_file:
            res = s_file.read()
            sys.stdout = old_stdout
            if res == "False":
                print('no')
            elif res == "True":
                print('yes')
            else:
                print('ERROR')
            sys.stdout = vsb_stdout
    # metacentrum launching
    elif sys.argv[1] == '-m':
        print("Option m activated")
        success = False
        if config['file_type'] == 'python':
            if config['optimalization'] == 'yes':
                success = python_optimalization(config)
            else:
                success = python(config)
        elif config['file_type'] == "general":
            success = general(config)
        with open(config['success'], "w") as file:
            file.write(str(success))


def get_main_file(root_of_tree):
    """
    Returns a main launching file.

    :param root_of_tree: Main folder of method.
    :return: True/False, filename
    """
    files = os.listdir(root_of_tree)
    for file in files:
        if not file.find("main") == -1:
            return True, os.path.basename(file)
    return False, ""


if __name__ == '__main__':
    main()
