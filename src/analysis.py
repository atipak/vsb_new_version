import json
from rdkit.ML.Scoring import Scoring


def _auc_ef(scores):
    """
    Funkce, ktera vrati slovnik s vypoctenym AUC a EF na vstupu skores.
    :param scores: Slovnik, obsahujici skore molekul z testu na vyberu.
    :return: Slovnik, obsahujici klice AUC a EF vysledku analyzy.
    """
    scores = sorted(scores,
                    key=lambda m: m['similarity'],
                    reverse=True)
    auc = Scoring.CalcAUC(scores, 'activity')
    ef = Scoring.CalcEnrichment(scores, 'activity', [0.005, 0.01, 0.02, 0.05])
    return {'AUC': auc, 'EF': ef}


def analyze(molecules, active, inactive, output_file_path=None):
    """
    Funkce, ktera vypocita AUC a EF z vysledku testu na vyberu.
    :param molecules: List s molekulami ziskany pomoci RDkitu z SDF souboru.
    :param active: List s nazvy aktivnich molekul z datasetu.
    :param inactive: List s nazvy neaktivnich molekul z datasetu.
    :param output_file_path: If not None, write result of analyze into this file.
    :return: Slovnik s vysledky, ktery ma klice: 'EF0.005', 'EF0.01', 'EF0.02', 'EF0.05', 'AUC'.
    """
    scores = []
    for molecule in molecules:
        new_molecule = {}
        for key in molecule:
            new_molecule[key] = molecule[key]

        if molecule['name'] in active:
            new_molecule['activity'] = 1
        else:
            new_molecule['activity'] = 0
        scores.append(new_molecule)
    ae = _auc_ef(scores)
    auc = ae['AUC']
    ef = ae['EF']
    if output_file_path is not None:
        with open(output_file_path, "w") as w_file:
            if str(output_file_path).endswith(".json"):
                import json
                json.dump({
                    "AUC": auc,
                    "EF": ef
                }, w_file, indent=2)
            else:
                w_file.write(ae)
    return {'EF0.005': ef[0], 'EF0.01': ef[1], 'EF0.02': ef[2], 'EF0.05': ef[3], 'AUC': auc, 'scores': scores}


def analyze_and_write(input, outputfile, active, inactive):
    """
    Funkce, ktera provede analyzu vysledku testu na vyberu a zapise ji do vystupniho souboru. Analyza zahrnuje vypocet AUC a EF.
    :param input: Soubor se vstupnimi daty.
    :param outputfile: Cesta k souboru, do ktereho se zapise vysledek.
    :param active: List s nazvy aktivnich molekul z datasetu.
    :param inactive: List s nazvy neaktivnich molekul z datasetu.
    :return: Slovnik s vysledky, ktery ma klice: 'EF0.005', 'EF0.01', 'EF0.02', 'EF0.05', 'AUC'.
    """
    scores = []
    for item in input:
        if item['name'] in active:
            scores.append({
                'name': item['name'],
                'similarity': item['similarity'],
                'activity': 1
            })
        elif item['name'] in inactive:
            scores.append({
                'name': item['name'],
                'similarity': item['similarity'],
                'activity': 0
            })
    # for it in scores:
    #     print(it)
    ae = _auc_ef(scores)
    auc = ae['AUC']
    ef = ae['EF']
    # print(ae)
    with open(outputfile, 'w') as output:
        json.dump({
            'metadata': {
                'AUC': auc,
                'EF': {
                    '0.005': ef[0],
                    '0.01': ef[1],
                    '0.02': ef[2],
                    '0.05': ef[3]
                }
            }
        }, output, indent=2
        )

    return {'EF0.005': ef[0], 'EF0.01': ef[1], 'EF0.02': ef[2], 'EF0.05': ef[3], 'AUC': auc,}
