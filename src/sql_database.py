import json
# import pymysql
import file_database

separator = '\\'
config = None
conn = None
sqlite_db = False
mysql_db = False
sql_error = None


# classes

class DatabaseHelper:
    """
    Support database contains all needed to service database actions.
    """

    class TableFils:
        table_name = 'FILES'
        id = "_ID"
        subtest_id = 'SUBTID'
        result_file = 'RFILE'
        analyze_file = 'AFILE'
        column_names = ("Files SQL ID", "ID of subtest", "path to an analyze file", "path to a result file")

    class TableUs:
        table_name = 'USERS'
        id = "_ID"
        username = 'USERNAME'
        information = 'INFO'
        column_names = ("User SQL ID", "username", "information")

    class TableTest:
        table_name = 'TESTS'
        id = "_ID"
        user_id = "USERID"
        test_type = "TYPE"
        subtest_count = "SUBTCOUNT"
        launch_date = "LDATE"
        method_id = "METHODID"
        test_id = "TESTID"
        test_descripiton = "DESCRIPTION"
        column_names = (
            "Test SQL ID", "ID of user", "type of test", "count of subtests", "ID of the used method", "launch date",
            "ID of test", "Description")

    class TableSubtest:
        table_name = 'SUBTESTS'
        id = "_ID"
        test_id = "TESTID"  # not row id
        run_time = "RUNTIME"
        id_dataset = 'IDDATASET'
        id_selection = 'IDSELECTION'
        column_names = ("Subtest SQL ID", "ID of test", "ID of data set", "ID of selection", "Runtime")

    class TableAnalyze:
        table_name = 'ANALYZES'
        subtest_id = 'SUBTID'
        id = "_ID"
        auc = 'AUC'
        EF0_01 = 'EF0_01'
        EF0_02 = 'EF0_02'
        EF0_05 = 'EF0_05'
        EF0_005 = 'EF0_005'
        column_names = ("Analyze SQL ID", "ID of subtest", "AUC", "EF 0.01", "EF 0.02", "EF 0.05", "EF 0.005")

    class TableMol:
        table_name = 'MOLS'
        id = "_ID"
        subtest_id = 'SUBTID'
        molecule_name = 'MOLNAME'
        score = 'SCORE'
        similar_molecule = "SIMMOL"
        add_description = "ADDDESCRIPTION"
        column_names = (
            "Molecule SQL ID", "ID of subtest", "molecule's name", "score", "similar molecule", "added description")

    class TableMethod:
        table_name = 'METHODS'
        id = "_ID"
        method_type = "MTYPE"
        first_file_id = "FFILE"
        second_file_id = "SFILE"
        first_origin_name = "FORIGIN"
        second_origin_name = "SORIGIN"
        join_ids = "JIDS"
        column_names = (
            "ID", "Type of method", "ID name of first file", "ID name of second file", "Origin name of first file",
            "Origin name of second file", "Ids from joining")

    class SupportClass:
        def ret_primary_key(name_of_columns):
            names = ""
            if isinstance(name_of_columns, list):
                for name in name_of_columns:
                    names += name + " , "
                names = names[:len(names) - 2]
            else:
                names = str(name_of_columns)
            return "PRIMARY KEY (" + names + ")"

        def ret_foreign_key(name_of_column, parent_table, parent_column=None):
            cascade = " ON DELETE CASCADE"
            if parent_column == None:
                parent_column = "_ID"
            return "FOREIGN KEY (" + str(
                name_of_column) + ") REFERENCES " + parent_table + " (" + parent_column + ")" + cascade

        def ret_ss(number):
            if sqlite_db:
                ch = "?"
            else:
                ch = "%s"
            text = ""
            for n in range(number - 1):
                text = text + "{0},".format(ch)
            return text + "{0}".format(ch)

    class Test_types:
        local_series = "ls"
        local_paralel = "lp"
        remove = "r"
        metacentrum = "mc"
        not_given = "ng"
        set = {local_paralel, local_series, remove, metacentrum, not_given}

    class Clauses:
        class Where:
            class User:
                def by_id(id):
                    return " " + DatabaseHelper.TableUs.id + " = " + str(id)

                def by_username(username):
                    username = str(username).strip()
                    if not username.endswith("\""):
                        username = username + "\""
                    if not username.startswith("\""):
                        username = "\"" + username
                    return " " + DatabaseHelper.TableUs.username + " = " + username

                def by_email(email):
                    email = str(email).strip()
                    if not email.endswith("\""):
                        email = email + "\""
                    if not email.startswith("\""):
                        email = "\"" + email
                    return " " + DatabaseHelper.TableUs.email + " = " + email

            class Test:
                def by_id(id):
                    return " " + DatabaseHelper.TableTest.id + " = " + str(id)

                def by_user_id(user_id):
                    return " " + DatabaseHelper.TableTest.user_id + " = " + str(user_id)

                def by_method_id(method_id):
                    return " " + DatabaseHelper.TableTest.method_id + " = " + str(method_id)

                def by_test_id(test_id):
                    return " " + DatabaseHelper.TableTest.test_id + " = " + str(test_id)

                def by_test_type(test_type):
                    test_type = str(test_type).strip()
                    if not test_type.endswith("\""):
                        test_type = test_type + "\""
                    if not test_type.startswith("\""):
                        test_type = "\"" + test_type
                    return " " + DatabaseHelper.TableTest.test_type + " = " + test_type

                def by_launch_date(launch_date):
                    launch_date = str(launch_date).strip()
                    parts = launch_date.split(".")
                    try:
                        day = parts[0]
                        month = parts[1]
                        year = parts[2]
                        # launch_date = "datetime.date({0}, {1}, {2})".format(year, month, day)
                        launch_date = "{0}-{1}-{2}".format(year, month, day)
                        if not launch_date.endswith("\'"):
                            launch_date = launch_date + "\""
                        if not launch_date.startswith("\'"):
                            launch_date = "\"" + launch_date
                        return " " + DatabaseHelper.TableTest.launch_date + " = date(" + launch_date + ")"
                    except Exception:
                        raise Exception("Bad date format.")

                def by_between_dates(first_date, last_date):
                    return " " + DatabaseHelper.TableTest.launch_date + " >= date(" + first_date + ")" + " and " + \
                           DatabaseHelper.TableTest.launch_date + " <= date(" + last_date + ")"

            class Molecule:
                def by_id(id):
                    return " " + DatabaseHelper.TableMol.id + " = " + str(id)

                def by_molecule_name(molecule_name):
                    molecule_name = str(molecule_name).strip()
                    if not molecule_name.endswith("\""):
                        molecule_name = molecule_name + "\""
                    if not molecule_name.startswith("\""):
                        molecule_name = "\"" + molecule_name
                    return " " + DatabaseHelper.TableMol.molecule_name + " = " + molecule_name

                def by_subtest_id(subtest_id):
                    return " " + DatabaseHelper.TableMol.subtest_id + " = " + str(subtest_id)

            class Analyze:
                def by_id(id):
                    return " " + DatabaseHelper.TableAnalyze.id + " = " + str(id)

                def by_subtest_id(subtest_id):
                    return " " + DatabaseHelper.TableAnalyze.subtest_id + " = " + str(subtest_id)

            class File:
                def by_id(id):
                    return " " + DatabaseHelper.TableFils.id + " = " + str(id)

                def by_subtest_id(subtest_id):
                    return " " + DatabaseHelper.TableFils.subtest_id + " = " + str(subtest_id)

            class Subtest:
                def by_id(id):
                    return " " + DatabaseHelper.TableSubtest.id + " = " + str(id)

                def by_test_id(test_id):
                    return " " + DatabaseHelper.TableSubtest.test_id + " = " + str(test_id)

                def by_id_dataset(id_dataset):
                    return " " + DatabaseHelper.TableSubtest.id_dataset + " = \"" + str(id_dataset) + "\""

                def by_id_selection(id_selection):
                    return " " + DatabaseHelper.TableSubtest.id_selection + " = \"" + str(id_selection) + "\""

            class Method:
                def by_id(id):
                    return " " + DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id + " = " + str(
                        id)

                def by_method_type(method_type):
                    method_type = str(method_type).strip()
                    if not method_type.endswith("\""):
                        method_type = method_type + "\""
                    if not method_type.startswith("\""):
                        method_type = "\"" + method_type
                    return " " + DatabaseHelper.TableMethod.method_type + " = " + method_type

                def by_l_name(l_name):
                    l_name = str(l_name).replace("\"", "").replace("\'", "").strip()
                    l_name = "\"" + l_name + "\""
                    return " " + DatabaseHelper.TableMethod.first_origin_name + " = " + l_name

                def by_s_name(s_name):
                    s_name = str(s_name).replace("\"", "").replace("\'", "").strip()
                    s_name = "\"" + s_name + "\""
                    return " " + DatabaseHelper.TableMethod.second_origin_name + " = " + s_name

                def by_join_ids(ids):
                    return " " + DatabaseHelper.TableMethod.join_ids + " = \"" + ids + "\""

        class Join:
            def join_more(joins):
                pass

            def user_test(self):
                return DatabaseHelper.Clauses.Join._join(DatabaseHelper.TableUs.table_name,
                                                         DatabaseHelper.TableTest.table_name,
                                                         DatabaseHelper.TableUs.id, DatabaseHelper.TableTest.user_id)

            def method_test(self):
                return DatabaseHelper.Clauses.Join._join(DatabaseHelper.TableMethod.table_name,
                                                         DatabaseHelper.TableTest.table_name,
                                                         DatabaseHelper.TableMethod.id,
                                                         DatabaseHelper.TableTest.method_id)

            def test_subtest(self):
                return DatabaseHelper.Clauses.Join._join(DatabaseHelper.TableSubtest.table_name,
                                                         DatabaseHelper.TableTest.table_name,
                                                         DatabaseHelper.TableSubtest.test_id,
                                                         DatabaseHelper.TableTest.id)

            def subtest_file(self):
                return DatabaseHelper.Clauses.Join._join(DatabaseHelper.TableSubtest.table_name,
                                                         DatabaseHelper.TableFils.table_name,
                                                         DatabaseHelper.TableSubtest.id,
                                                         DatabaseHelper.TableFils.subtest_id)

            def subtest_analyze(self):
                return DatabaseHelper.Clauses.Join._join(DatabaseHelper.TableSubtest.table_name,
                                                         DatabaseHelper.TableAnalyze.table_name,
                                                         DatabaseHelper.TableSubtest.id,
                                                         DatabaseHelper.TableAnalyze.subtest_id)

            def subtest_molecule(self):
                return DatabaseHelper.Clauses.Join._join(DatabaseHelper.TableSubtest.table_name,
                                                         DatabaseHelper.TableMol.table_name,
                                                         DatabaseHelper.TableSubtest.id,
                                                         DatabaseHelper.TableMol.subtest_id)

            def _join(first_table, second_table, first_equality, second_equality):
                text = "{0} JOIN {1} ON {2} = {3}".format(first_table, second_table, first_equality, second_equality)
                return text

        class Special:

            class tests_basic_info:
                @staticmethod
                def column_names():
                    return ("ID", "Test order", "Launch date", "Test type", "User", "Subtest count", "Load function",
                            "Score function")

                @staticmethod
                def what():
                    s = ""
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.id + ", "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.test_id + ", "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.launch_date + ", "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.test_type + ", "
                    s += DatabaseHelper.TableUs.table_name + "." + DatabaseHelper.TableUs.username + ", "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.subtest_count + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.first_origin_name + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.second_origin_name
                    return s

                @staticmethod
                def join():
                    s = " INNER JOIN " + DatabaseHelper.TableMethod.table_name + " ON "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.method_id
                    s += " INNER JOIN " + DatabaseHelper.TableUs.table_name + " ON "
                    s += DatabaseHelper.TableUs.table_name + "." + DatabaseHelper.TableUs.id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.user_id
                    return s

            class molecule_info:
                @staticmethod
                def column_names():
                    return (
                        "Score", "Method type", "Load function / Method", "Score function",
                        "Subtest id")

                @staticmethod
                def what():
                    s = ""
                    s += DatabaseHelper.TableMol.table_name + "." + DatabaseHelper.TableMol.score + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.method_type + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.first_origin_name + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.second_origin_name + ", "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id
                    return s

                @staticmethod
                def join():
                    # subtest
                    s = " INNER JOIN " + DatabaseHelper.TableSubtest.table_name + " ON "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id
                    s += " = "
                    s += DatabaseHelper.TableMol.table_name + "." + DatabaseHelper.TableMol.subtest_id
                    # test
                    s += " INNER JOIN " + DatabaseHelper.TableTest.table_name + " ON "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.test_id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.id
                    # method
                    s += " INNER JOIN " + DatabaseHelper.TableMethod.table_name + " ON "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.method_id
                    return s

            class method_subtest_id:
                @staticmethod
                def column_names():
                    return (
                        "Subtest id", "Method id", "Method type", "Load function / Method", "Score function")

                @staticmethod
                def what():
                    s = ""
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.method_type + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.first_origin_name + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.second_origin_name
                    return s

                @staticmethod
                def join():
                    # test X method
                    s = " INNER JOIN " + DatabaseHelper.TableTest.table_name + " ON "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.method_id
                    # subtest X test
                    s += " INNER JOIN " + DatabaseHelper.TableSubtest.table_name + " ON "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.test_id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.id
                    return s

            class method_subtest_id_analysis:
                @staticmethod
                def column_names():
                    analysis = []
                    for i in range(2, len(DatabaseHelper.TableAnalyze.column_names)):
                        analysis.append(DatabaseHelper.TableAnalyze.column_names[i])
                    return (
                               "Subtest id", "Method type", "Load function / Method", "Score function") + tuple(
                        analysis)

                @staticmethod
                def what():
                    # column_names = ("ID", "ID of subtest", "AUC", "EF 0.005", "EF 0.01", "EF 0.02", "EF 0.05")
                    s = ""
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.method_type + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.first_origin_name + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.second_origin_name + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.auc + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_005 + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_01 + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_02 + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_05
                    return s

                @staticmethod
                def join():
                    # test X method
                    s = " INNER JOIN " + DatabaseHelper.TableTest.table_name + " ON "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.method_id
                    # subtest X test
                    s += " INNER JOIN " + DatabaseHelper.TableSubtest.table_name + " ON "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.test_id
                    s += " = "
                    s += DatabaseHelper.TableTest.table_name + "." + DatabaseHelper.TableTest.id
                    # subtest X analysis
                    s += " INNER JOIN " + DatabaseHelper.TableAnalyze.table_name + " ON "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id
                    s += " = "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.subtest_id
                    return s

            class method_data:
                @staticmethod
                def column_names():
                    analysis = []
                    for i in range(2, len(DatabaseHelper.TableAnalyze.column_names)):
                        analysis.append(DatabaseHelper.TableAnalyze.column_names[i])
                    return (
                               "Method id", "Subtest id", "Method type", "Load function / Method", "Score function",
                               "Data-set",
                               "Selection") + tuple(analysis)

                @staticmethod
                def what():
                    # column_names = ("ID", "ID of subtest", "AUC", "EF 0.005", "EF 0.01", "EF 0.02", "EF 0.05")
                    s = ""
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.id + ", "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.method_type + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.first_origin_name + ", "
                    s += DatabaseHelper.TableMethod.table_name + "." + DatabaseHelper.TableMethod.second_origin_name + ", "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id_dataset + ", "
                    s += DatabaseHelper.TableSubtest.table_name + "." + DatabaseHelper.TableSubtest.id_selection + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.auc + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_005 + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_01 + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_02 + ", "
                    s += DatabaseHelper.TableAnalyze.table_name + "." + DatabaseHelper.TableAnalyze.EF0_05
                    return s

                @staticmethod
                def join():
                    return DatabaseHelper.Clauses.Special.method_subtest_id_analysis.join()

    type_int = ' INT'
    type_integer = ' INTEGER'
    type_varchar_255 = ' VARCHAR(255)'
    type_varchar_5 = ' VARCHAR(5)'
    type_varchar_50 = ' VARCHAR(50)'
    type_varchar_25 = ' VARCHAR(25)'
    type_varchar_15 = ' VARCHAR(15)'
    type_varchar_20 = ' VARCHAR(20)'
    type_text = ' TEXT'
    type_float = ' FLOAT'
    type_date = " DATE"
    auto_increment = " AUTO_INCREMENT"
    not_null = " NOT NULL"

    comma = ' , '

    #
    #
    #
    #
    @staticmethod
    def record_id_string():
        type = DatabaseHelper.type_int
        ai = DatabaseHelper.auto_increment
        if sqlite_db:
            ai = ""
            type = DatabaseHelper.type_integer
        return DatabaseHelper.TableTest.id + type + ai + DatabaseHelper.comma

    @staticmethod
    def column_table_tests():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableTest.user_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.test_type + DatabaseHelper.type_varchar_5 + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.subtest_count + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.method_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.launch_date + DatabaseHelper.type_date + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.test_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.test_descripiton + DatabaseHelper.type_text + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(
                   DatabaseHelper.TableTest.id) + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_foreign_key(DatabaseHelper.TableTest.method_id,
                                                           DatabaseHelper.TableMethod.table_name) + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_foreign_key(DatabaseHelper.TableTest.user_id,
                                                           DatabaseHelper.TableUs.table_name)

    @staticmethod
    def column_table_us():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableUs.username + DatabaseHelper.type_varchar_15 + DatabaseHelper.comma + \
               DatabaseHelper.TableUs.information + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(DatabaseHelper.TableUs.id)

    @staticmethod
    def column_table_subtests():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableSubtest.test_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableSubtest.id_dataset + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableSubtest.id_selection + DatabaseHelper.type_varchar_5 + DatabaseHelper.comma + \
               DatabaseHelper.TableSubtest.run_time + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(DatabaseHelper.TableSubtest.id) + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_foreign_key(DatabaseHelper.TableSubtest.test_id,
                                                           DatabaseHelper.TableTest.table_name)

    @staticmethod
    def column_table_fils():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableFils.subtest_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableFils.analyze_file + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableFils.result_file + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(DatabaseHelper.TableFils.id) + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_foreign_key(DatabaseHelper.TableFils.subtest_id,
                                                           DatabaseHelper.TableSubtest.table_name)

    @staticmethod
    def column_table_methods():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableMethod.method_type + DatabaseHelper.type_varchar_15 + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.first_file_id + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.second_file_id + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.first_origin_name + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.second_origin_name + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.join_ids + DatabaseHelper.type_varchar_25 + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(DatabaseHelper.TableMethod.id)

    @staticmethod
    def column_table_analyzes():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableAnalyze.subtest_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.auc + DatabaseHelper.type_float + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_005 + DatabaseHelper.type_float + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_01 + DatabaseHelper.type_float + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_02 + DatabaseHelper.type_float + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_05 + DatabaseHelper.type_float + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(DatabaseHelper.TableAnalyze.id) + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_foreign_key(DatabaseHelper.TableAnalyze.subtest_id,
                                                           DatabaseHelper.TableSubtest.table_name)

    @staticmethod
    def column_table_mols():
        return DatabaseHelper.record_id_string() + \
               DatabaseHelper.TableMol.subtest_id + DatabaseHelper.type_int + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.molecule_name + DatabaseHelper.type_varchar_255 + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.score + DatabaseHelper.type_float + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.similar_molecule + DatabaseHelper.type_varchar_50 + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.add_description + DatabaseHelper.type_text + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_primary_key(DatabaseHelper.TableMol.id) + DatabaseHelper.comma + \
               DatabaseHelper.SupportClass.ret_foreign_key(DatabaseHelper.TableMol.subtest_id,
                                                           DatabaseHelper.TableSubtest.table_name)

    @staticmethod
    def insert_us():
        return "INSERT INTO " + DatabaseHelper.TableUs.table_name + "(" + DatabaseHelper.TableUs.username + DatabaseHelper.comma + \
               DatabaseHelper.TableUs.information + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(2) + ")"

    @staticmethod
    def insert_tests():
        return "INSERT INTO " + DatabaseHelper.TableTest.table_name + "(" + DatabaseHelper.TableTest.user_id + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.test_type + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.subtest_count + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.launch_date + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.method_id + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.test_id + DatabaseHelper.comma + \
               DatabaseHelper.TableTest.test_descripiton + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(7) + ")"

    @staticmethod
    def insert_mols():
        return "INSERT INTO " + DatabaseHelper.TableMol.table_name + "(" + DatabaseHelper.TableMol.molecule_name + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.subtest_id + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.score + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.similar_molecule + DatabaseHelper.comma + \
               DatabaseHelper.TableMol.add_description + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(5) + ")"

    @staticmethod
    def insert_analyzes():
        return "INSERT INTO " + DatabaseHelper.TableAnalyze.table_name + "(" + DatabaseHelper.TableAnalyze.auc + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_01 + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_02 + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_05 + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.EF0_005 + DatabaseHelper.comma + \
               DatabaseHelper.TableAnalyze.subtest_id + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(6) + ")"

    @staticmethod
    def insert_fils():
        return "INSERT INTO " + DatabaseHelper.TableFils.table_name + "(" + DatabaseHelper.TableFils.result_file + DatabaseHelper.comma + \
               DatabaseHelper.TableFils.analyze_file + DatabaseHelper.comma + \
               DatabaseHelper.TableFils.subtest_id + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(3) + ")"

    @staticmethod
    def insert_subtests():
        return "INSERT INTO " + DatabaseHelper.TableSubtest.table_name + "(" + DatabaseHelper.TableSubtest.test_id + DatabaseHelper.comma + \
               DatabaseHelper.TableSubtest.run_time + DatabaseHelper.comma + \
               DatabaseHelper.TableSubtest.id_dataset + DatabaseHelper.comma + \
               DatabaseHelper.TableSubtest.id_selection + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(4) + ")"

    @staticmethod
    def insert_methods():
        return "INSERT INTO " + DatabaseHelper.TableMethod.table_name + "(" + DatabaseHelper.TableMethod.method_type + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.first_file_id + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.second_file_id + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.first_origin_name + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.second_origin_name + DatabaseHelper.comma + \
               DatabaseHelper.TableMethod.join_ids + ") VALUES(" + DatabaseHelper.SupportClass.ret_ss(6) + ")"

        #
        #
        #
        #

    @staticmethod
    def delete_method(method_id):
        return "DELETE FROM " + DatabaseHelper.TableMethod.table_name + " WHERE " + \
               DatabaseHelper.TableMethod.id + " = " + str(method_id)

    @staticmethod
    def delete_test(test_id):
        return "DELETE FROM " + DatabaseHelper.TableTest.table_name + " WHERE " + \
               DatabaseHelper.TableTest.id + " = " + str(test_id)

    @staticmethod
    def delete_subtest(subtest_id):
        return "DELETE FROM " + DatabaseHelper.TableSubtest.table_name + " WHERE " + \
               DatabaseHelper.TableSubtest.id + " = " + str(subtest_id)


# delete methods

def delete_method_from_db(method_id):
    """
    Deletes method from SQL database.
    :param method_id: Method SQL ID
    :return:
    """
    if not method_id or method_id == None:
        return "Bad parameters"

    query = DatabaseHelper.delete_method(method_id)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query)

        if cursor.lastrowid:
            string = "A method with id " + str(method_id) + " was deleted"
            file_database.log("delete_method_from_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in deleting method with id " + str(method_id)
            file_database.log("delete_method_from_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


def delete_test_from_db(test_id):
    """
    Deletes test from SQL database.
    :param test_id: Test SQL ID
    :return:
    """
    if not test_id or test_id == None:
        return "Bad parameters"

    query = DatabaseHelper.delete_test(test_id)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query)

        if cursor.lastrowid:
            string = "A test with id " + str(test_id) + " was deleted"
            file_database.log("delete_test_from_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in deleting test with id " + str(test_id)
            file_database.log("delete_test_from_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


def delete_subtest_from_db(subtest_id):
    """
    Deletes test from SQL database.
    :param test_id: Test SQL ID
    :return:
    """
    if not subtest_id or subtest_id == None:
        return "Bad parameters"

    query = DatabaseHelper.delete_subtest(subtest_id)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query)

        if cursor.lastrowid:
            string = "A subtest with id " + str(subtest_id) + " was deleted"
            file_database.log("delete_subtest_from_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in deleting subtest with id " + str(subtest_id)
            file_database.log("delete_subtest_from_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


# adding methods

def add_user_to_db(username, information="empty"):
    """
    Add a new user to the SQL database.
    :param username: Username (VARCHAR 15)
    :param password: Password (VARCHAR 20)
    :param name: Name of user (VARCHAR 25)
    :param lastname: Last name of the user (VARCHAR 25)
    :param email: E-mail of the user (VARCHAR 20)
    :param information: Next information which should be saved (VARCHAR 255)
    :return: ID of the inserted record.
    """
    if not username or username == None:
        return "Bad parameters"

    query = DatabaseHelper.insert_us()
    args = (username, information)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            string = "A user with username " + str(username) + " was inserted"
            file_database.log("add_user_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting user with username " + str(username)
            file_database.log("add_user_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


def add_files_to_db(id_subtest, result_file="empty.txt", analyze_file="empty.txt"):
    """
    Adds files name to the SQL database.
    :param id_subtest: Subtest id (LONG)
    :param result_file: Path to a JSON file with result of testing method. (VARCHAR 255)
    :param analyze_file: Path to a JSON file with analyze the result of testing method. (VARCHAR 255)
    :return: ID of the inserted record.
    """
    if id_subtest == None:
        return "Bad parameters"

    query = DatabaseHelper.insert_fils()
    args = (result_file, analyze_file, id_subtest)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            string = "A record with files according to the subtest " + str(id_subtest) + " was inserted"
            file_database.log("add_files_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting record with files according to the subtest " + str(id_subtest)
            file_database.log("add_files_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)
    except Exception as er:
        print(er)

    finally:
        cursor.close()
        conn.close()


def add_test_to_db(id_test, id_user=-1, type=DatabaseHelper.Test_types.not_given, subtest_count=-1, launch_date=0,
                   id_method=-1, test_text=None):
    """
    Insert information about a test into the SQL database.
    :param id_test: ID of the test in file database (LONG)
    :param id_user: ID of an user who launched the test (LONG)
    :param type: Type of test (DatabaseHelper.TestType)
    :param subtest_count: A number representing count of repeating (INT)
    :param launch_date: Date of launch (Date)
    :param id_method: Id of method which was tested (INT)
    :return: ID of the inserted record.
    """
    if id_test == None:
        return "Bad parameters"

    query = DatabaseHelper.insert_tests()
    args = (id_user, type, subtest_count, launch_date, id_method, id_test, test_text)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            string = "A record with information about test with id " + str(id_test) + " was inserted"
            file_database.log("add_test_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting record with information about test with id " + str(id_test)
            file_database.log("add_test_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


def add_mols_to_db(id_subtest, mols_scores):
    """
    Adds molecules information to the SQL database.
    :param id_subtest: ID of subtest (LONG)
    :param mols_scores: List of dictionaries with keys name (molecule name) and similarity (molecule score)
    :return: Id of last inserted record.
    """
    if id_subtest == None or mols_scores == None:
        return "Bad parameters"

    query = DatabaseHelper.insert_mols()

    try:

        conn = get_connection()

        cursor = conn.cursor()
        for sc in mols_scores:
            id_subtest = int(id_subtest)
            molecule_name = str(sc['name'])
            molecule_score = float(sc['similarity'])
            score_molecule = None
            add_description = None
            if "score_molecule" in sc:
                score_molecule = str(sc["score_molecule"])
            if "description" in sc:
                add_description = str(sc["description"])
            args = (molecule_name, id_subtest, molecule_score, score_molecule, add_description)
            cursor.execute(query, args)
        if cursor.lastrowid:
            string = "Molecules from subtest with ID " + str(id_subtest) + " was inserted"
            file_database.log("add_mols_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting molecules from subtest with ID " + str(id_subtest)
            file_database.log("add_mols_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)
    except Exception as er:
        print(er)

    finally:
        cursor.close()
        conn.close()


def add_analyze_to_db(id_subtest, auc=0, ef_001=0, ef_002=0, ef_005=0, ef_0005=0):
    """
    Adds information about analyze of subtest in the SQL database.
    :param id_subtest: ID of subtest (LONG)
    :param auc: AUC (FLOAT)
    :param ef_001:  EF 0.01 (FLOAT)
    :param ef_002:  EF 0.02 (FLOAT)
    :param ef_005: EF 0.05 (FLOAT)
    :param ef_0005: EF 0.005 (FLOAT)
    :return: Id of inserted record.
    """
    if id_subtest == None:
        return "Bad parameters"

    query = DatabaseHelper.insert_analyzes()
    args = (auc, ef_001, ef_002, ef_005, ef_0005, id_subtest)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            string = "A record with analyze information about subtest with id " + str(id_subtest) + " was inserted"
            file_database.log("add_analyze_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting record with analyze information about test with id " + str(id_subtest)
            file_database.log("add_analyze_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)
    except Exception as e:
        print(e)

    finally:
        cursor.close()
        conn.close()


def add_subtest_to_db(id_test, run_time=0, dataset_name="empty", selection_name="empty"):
    """

    :param id_test: ID of test (LONG)
    :param run_time: Runtime (LONG)
    :param dataset_name: ID of data set in file DB (INT)
    :param selection_name: ID of selection in file DB (INT)
    :return: Id of inserted record.
    """
    if id_test == None:
        return "Bad parameters"

    query = DatabaseHelper.insert_subtests()
    args = (id_test, run_time, dataset_name, selection_name)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            string = "A record with information about subtest from test with id " + str(id_test) + " was inserted"
            file_database.log("add_subtest_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting record with information about subtest from test with id " + str(id_test)
            file_database.log("add_subtest_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


def add_method_to_db(type="unknown", first_file_name="empty", second_file_name="empty", first_origin_name="empty",
                     second_origin_name="empty", ids=None):
    """
    Adds a record about methods to the SQL database.

    :param second_origin_name: Origin name of method
    :param first_origin_name: An origin name of method
    :param type: Python(py)/General(gen) (VARCHAR 5)
    :param first_file_name: Name of method (VARCHAR 255)
    :param second_file_name: Name of method (VARCHAR 255)
    :return: Id of inserted record.
    """
    query = DatabaseHelper.insert_methods()
    args = (type, first_file_name, second_file_name, first_origin_name, second_origin_name, ids)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            string = "A record with information about method with name " + str(first_file_name) + " was inserted"
            file_database.log("add_method_to_db", string)
            r_value = cursor.lastrowid
        else:
            string = "A problem in inserting record with information about method with name " + str(first_file_name)
            file_database.log("add_method_to_db", string)
            r_value = string

        conn.commit()
        return r_value
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


# getting methods
def get_user(id_user, username):
    """
    Returns information about user from DB. One of parameters can be None:
    :param id_user: ID of user
    :param username: username
    :return: Result (only one row)
    """
    if id_user is not None:
        return read_from_database("*", DatabaseHelper.TableUs.table_name,
                                  "{0} = \'{1}\'".format(DatabaseHelper.TableUs.id, id_user), 1)
    elif username is not None:
        return read_from_database("*", DatabaseHelper.TableUs.table_name,
                                  "{0} = \'{1}\'".format(DatabaseHelper.TableUs.username, username), 1)
    else:
        return ""


def get_files_from_db(id_subtest):
    """
    Returns record from table files which is saved with ID subtest.
    :param id_subtest: ID of subtest
    :return: Result (only first), if id_subtest is None then empty string
    """
    if id_subtest is not None:
        return read_from_database("*", DatabaseHelper.TableFils.table_name,
                                  "{0} == {1}".format(DatabaseHelper.TableFils.subtest_id, id_subtest), 1)
    else:
        return ""


def get_methods_names(id):
    """
    Returns methods names for py version and file name for general version.

    :param id: ID of method in SQL database
    :return: List [type, first_file_name, second_file_name(None for type general)]
    """
    if id is not None:
        where = DatabaseHelper.Clauses.Where.Method.by_id(id)
        return read_from_database("*", DatabaseHelper.TableMethod.table_name, where, 1)
    else:
        return ""


def read_from_database(what, From, where, howmany, join=None):
    """
    Support function representing SQL command for select from SQL database.
    :param what: columns
    :param From: table (alternatively joins)
    :param where: which conditions should result comply
    :param howmany: how many rows should be returned. (0 = all)
    :return: List with results
    """
    query = "SELECT {0} FROM {1}".format(what, From)
    if join is not None:
        query += " {0}".format(join)
    if where is not None and not where == "":
        query += " WHERE {0}".format(where)
    try:

        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query)
        if howmany <= 0:
            return cursor.fetchall()
        else:
            return cursor.fetchmany(size=howmany)
    except sql_error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()


# management of DB

def create_table_sql(name, cols, suppress):
    """
    Creates new table in SQL DB.
    :param suppress: if True then suppresses error messages
    :param name: table name
    :param cols: columns name, SQL standard types
    :param drop_if: True/False - delete table if exists
    :return: Nothing
    """
    query = "CREATE TABLE {0} ({1})".format(name, cols)
    try:
        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute(query)

        conn.commit()
    except sql_error as error:
        if not suppress:
            print(error)
            print(name)
    except Exception as error:
        if not str(error).find("already exists") == -1:
            if not suppress:
                print(error)
        else:
            raise error

    finally:
        cursor.close()
        conn.close()


def drop_if_exists(name, suppress):
    """
    Drops a table with given name if exists

    :param name: Name of table
    :param suppress: Silent desriptor
    :return:
    """
    try:
        conn = get_connection()

        cursor = conn.cursor()
        cursor.execute("DROP TABLE IF EXISTS {0}".format(name))

        conn.commit()
    except sql_error as error:
        if not suppress:
            print(error)
            print(name)
    except Exception as error:
        if not str(error).find("already exists") == -1:
            if not suppress:
                print(error)
        else:
            raise error


def init_sqldatabase(delete_table_users, delete_table_tests, delete_table_molecules, delete_table_analyzes,
                     delete_table_files, delete_table_subtests, delete_table_methods, silent=True):
    """
    Initialize SQL database.
    :param delete_table_users: True/False - delete table if exists
    :param delete_table_tests:  True/False - delete table if exists
    :param delete_table_molecules: True/False - delete table if exists
    :param delete_table_analyzes: True/False - delete table if exists
    :param delete_table_files: True/False - delete table if exists
    :param delete_table_subtests: True/False - delete table if exists
    :param delete_table_methods: True/False - delete table if exists
    :return: True or False by result of creating
    """
    online, exception = is_sql_online()
    if online:
        if delete_table_users:
            delete_table_tests = True
            delete_table_molecules = True
            delete_table_analyzes = True
            delete_table_subtests = True
            delete_table_files = True
        if delete_table_methods:
            delete_table_tests = True
            delete_table_molecules = True
            delete_table_analyzes = True
            delete_table_subtests = True
            delete_table_files = True
        if delete_table_tests:
            delete_table_molecules = True
            delete_table_analyzes = True
            delete_table_subtests = True
            delete_table_files = True
        if delete_table_subtests:
            delete_table_molecules = True
            delete_table_analyzes = True
            delete_table_files = True
        if delete_table_molecules:
            drop_if_exists(DatabaseHelper.TableMol.table_name, silent)
        if delete_table_files:
            drop_if_exists(DatabaseHelper.TableFils.table_name, silent)
        if delete_table_analyzes:
            drop_if_exists(DatabaseHelper.TableAnalyze.table_name, silent)
        if delete_table_subtests:
            drop_if_exists(DatabaseHelper.TableSubtest.table_name, silent)
        if delete_table_tests:
            drop_if_exists(DatabaseHelper.TableTest.table_name, silent)
        if delete_table_methods:
            drop_if_exists(DatabaseHelper.TableMethod.table_name, silent)
        if delete_table_users:
            drop_if_exists(DatabaseHelper.TableUs.table_name, silent)


        create_table_methods(silent)
        if delete_table_methods:
            import general
            general.start_init_methods()
        create_table_us(silent)
        if delete_table_users:
            add_user_to_db("host", "")
        create_table_test(silent)
        create_table_subtests(silent)
        create_table_analyze(silent)
        create_table_fils(silent)
        create_table_mol(silent)
        return True
    else:
        print("A problem with SQL database: {0}".format(exception))
        return False


def is_sql_online():
    """
    If SQL database is initialized and there is no problem with connection return True else False
    :return: True/False
    """
    try:
        conn = get_connection()
        conn.close()
        return True, ""
    except Exception as e:
        return False, e


def create_table_us(suppress):
    """
    Creates table users

    :param delete_table_users: True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """

    table_name = DatabaseHelper.TableUs.table_name
    columns = DatabaseHelper.column_table_us()
    create_table_sql(table_name, columns, suppress)


def create_table_test(suppress):
    """
    Creates table tests.
    :param delete_table_tests:True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """
    table_name = DatabaseHelper.TableTest.table_name
    columns = DatabaseHelper.column_table_tests()
    create_table_sql(table_name, columns, suppress)


def create_table_mol(suppress):
    """
    Creates table molecules.
    :param delete_table_molecules: True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """
    table_name = DatabaseHelper.TableMol.table_name
    columns = DatabaseHelper.column_table_mols()
    create_table_sql(table_name, columns, suppress)


def create_table_analyze(suppress):
    """
    Creates table analyzes.
    :param delete_table_analyzes: True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """
    table_name = DatabaseHelper.TableAnalyze.table_name
    columns = DatabaseHelper.column_table_analyzes()
    create_table_sql(table_name, columns, suppress)


def create_table_fils(suppress):
    """
    Creates table files.
    :param delete_table_files: True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """
    table_name = DatabaseHelper.TableFils.table_name
    columns = DatabaseHelper.column_table_fils()
    create_table_sql(table_name, columns, suppress)


def create_table_subtests(suppress):
    """
    Creates table subtests
    :param delete_table_subtests:True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """
    table_name = DatabaseHelper.TableSubtest.table_name
    columns = DatabaseHelper.column_table_subtests()
    create_table_sql(table_name, columns, suppress)


def create_table_methods(suppress):
    """
    Creates table methods
    :param delete_table_methods: True if current table should be deleted.
    :param suppress: Suppress output of information
    :return: Nothing
    """
    table_name = DatabaseHelper.TableMethod.table_name
    columns = DatabaseHelper.column_table_methods()
    create_table_sql(table_name, columns, suppress)


def _read_config(configuration_path):
    """
    Load a configuration for the database to the global variable config.
    :param configuration_path: Path of a json file with a configuration.
    :return: True or false by the success of loading.
    """
    global config
    try:
        with open(configuration_path, "r") as config_file:
            config = json.load(config_file)['sql_conf']
        return True
    except Exception as e:
        print(e)
        return False


def init(database_configuration_path):
    """
    Initialize database module.
    :param database_configuration_path: Path to json file with configuration for the database.
    :return: True if inizalization was successful or raise an exception.
    """
    if _read_config(database_configuration_path):
        conn = get_connection()
        try:
            conn.close()
        except:
            pass
        return True
    else:
        print("Database configuration for server couldn't be read")
        return False


def connect_to_sqlite_db():
    """
    Creates a connection to SQLite DB on computer. This database is portable and is saved in the data folder in the root.
    :return: Connection object
    """
    import sqlite3
    global sql_error
    global sqlite_db
    conn = sqlite3.connect("../data/benchmark.db")
    cursor = conn.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    conn.commit()
    cursor.close()
    if not sqlite_db:
        print("Connected to SQLite DB saved in data folder.")
        sqlite_db = True
        sql_error = sqlite3.Error
    return conn


def get_connection():
    """
    Returns a connection to a DB.
    :return: Connection object
    """
    global conn
    global sql_error
    global mysql_db
    if config is not None:
        if sqlite_db:
            conn = connect_to_sqlite_db()
        else:
            try:
                import pymysql
                conn = pymysql.Connection(**config)
                if not mysql_db:
                    print("Connected to MYSQL server")
                    mysql_db = True
                    sql_error = pymysql.Error
            except ImportError:
                if not mysql_db:
                    print("[WARNING] PyMySQL driver not found. You are connected to a SQLite database.")
            except Exception as e:
                if not mysql_db:
                    print(e)
                    print("[WARNING] Bad log data. You are connected to a SQLite database.")
            if not mysql_db:
                mysql_db = True
                conn = connect_to_sqlite_db()
    else:
        conn = connect_to_sqlite_db()
    return conn
