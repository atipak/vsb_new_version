import urllib.request
import urllib.error
import urllib.parse
import json
import io

prolog = "http://pubchem.ncbi.nlm.nih.gov/rest/pug"
separator = '/'
output = ''
operation = ''
identify = ''
summary = ''


def getresult(url):
    try:
        connection = urllib.request.urlopen(url)
    except urllib.error.HTTPError:
        return ""
    else:
        return connection.read().rstrip()


def load_assay_url(output_type, way, identity, identifiers=''):
    # output type
    if output_type != 'CSV' and output_type != 'TXT' and output_type != 'JSON' and output_type != 'XML':
        print('bad output type')
        return -1
    output = output_type
    # operation
    if way == 'assay':
        operation = 'assay/aid'
    else:
        print('bad operation')
        return -1
    # identifiers
    # A ... active mols
    # I ... inactive mols
    # S ... substances ids
    # C ... compounds ids
    # M ... summary
    identify = ''
    summary = ''
    if identifiers != None and identifiers != '':
        oldidentity = ''
        un = 'un'
        false = 'false'
        true = 'true'
        cs_error = un
        ai_error = un
        for char in identifiers:
            if char == 'A':
                identify = identify + '_type=active'
                if ai_error != un:
                    ai_error = true
                else:
                    ai_error = false
            elif char == 'I':
                identify = identify + '_type=inactive'
                if ai_error != un:
                    ai_error = true
                else:
                    ai_error = false
            elif char == 'S':
                identify = '?sids' + identify
                oldidentity = identity
                identity = identity + '/sids'
                if cs_error != un:
                    cs_error = true
                else:
                    cs_error = false
            elif char == 'C':
                identify = '?cids' + identify
                oldidentity = identity
                identity = identity + '/cids'
                if cs_error != un:
                    cs_error = true
                else:
                    cs_error = false
            elif char == 'M':
                summary = 'summary/'
                cs_error = false
                ai_error = false
                identify = ''
            else:
                cs_error = true
        if cs_error == true or (cs_error == un and identify != '') or ai_error == true:
            print('bad identifiers')
            identify = ''
            identity = oldidentity

    url = prolog + separator + operation + separator + identity + separator + summary + output + identify
    print(url)
    return getresult(url)


def load_mol_url(output_type, way, identity):
    # output type
    if output_type != 'XML' and output_type != 'SDF':
        print('bad output type')
        return -1
    output = output_type
    # operation
    if way == 'CID':
        operation = 'compound/cid'
    elif way == 'SID':
        operation = 'substance/sid'
    elif way == 'name':
        operation = 'compound/name'
    elif way == 'structure':
        operation = 'compound/smiles'
    elif way == 'assay':
        operation = 'assay/aid'
    else:
        print('bad operation')
        return -1

    url = prolog + separator + operation + separator + str(identity) + separator + output
    print(url)
    return getresult(url)


def get_counts(string):
    counts = []
    buf = io.StringIO(string)
    for line in buf:
        if 'SIDCountAll' in line:
            counts.append(get_first_number(line))
        elif 'SIDCountActive' in line:
            counts.append(get_first_number(line))
        elif 'SIDCountInactive' in line:
            counts.append(get_first_number(line))
        elif 'CIDCountAll' in line:
            counts.append(get_first_number(line))
        elif 'CIDCountActive' in line:
            counts.append(get_first_number(line))
        elif 'CIDCountInactive' in line:
            counts.append(get_first_number(line))
    return counts


def get_first_number(string):
    number = ''
    cond = 'false'
    for char in string:
        if char in '0123456789':
            if cond == 'false':
                cond = 'true'
            number = number + char
        else:
            if cond == 'true':
                break
    try:
        return int(number)
    except:
        return ''


def load_mol_post(output_type, way, identities):
    url = 'http://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchi/cids/JSON'
    header = {'Content-Type': 'application/x-www-form-urlencoded'}
    names = ''
    for name in identities:
        names += str(name) + ','
    body = urllib.parse.urlencode({'SID': names})
    req = urllib.request.Request(url, body.encode(), header)
    connection = urllib.request.urlopen(req)
    print(connection)


def load_sdf(*args):
    maximum = 50
    mol_separator = '\n\n'
    iterator = 0
    ids = ""
    sdf_stream = []
    for arg in args:
        for item in arg:
            if iterator == 0:
                ids += str(item)
                iterator += 1
            elif iterator <= maximum - 1:
                ids += "," + str(item)
                iterator += 1
            elif iterator == maximum:
                ids += "," + str(item)
                sdf_stream.append(load_mol_url('SDF', 'SID', ids).decode(encoding='utf-8'))
                sdf_stream.append(mol_separator)
                ids = ""
                iterator = 0
        if iterator != 0:
            sdf_stream.append(load_mol_url('SDF', 'SID', ids).decode(encoding='utf-8'))
            sdf_stream.append(mol_separator)
            ids = ""
            iterator = 0
    return ''.join(sdf_stream)


def get_metadata(string_in_json):
    metadata = []
    reader = json.loads(string_in_json)
    sum = reader['AssaySummaries']['AssaySummary'][0]
    try:
        name = sum['Name']
    except:
        name = ""
    try:
        target = sum['Target']['Name']
    except:
        target = ""
    metadata.append(name)
    metadata.append(target)
    return metadata





