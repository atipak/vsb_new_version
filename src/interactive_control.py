from general import *
import file_formats
import json

# global variables
ex = "exit"


def show_help():
    print("Help")
    with open("../data/Console_help.txt", "r") as file:
        print(file.read())


def do_get():
    while True:
        print(
            "What would you like to get? For choice of an option insert a characteristic letter or a word by the documentation.")
        print("a ... analyze")
        print("d ... dataset")
        print("e ... exit")
        print("f ... file")
        print("g ... working group")
        print("me ... method")
        print("mo ... molecule")
        print("se ... selection")
        print("su ... subtest")
        print("t ... test")
        s = str(input(">>>")).strip()
        if s == "exit" or s == "e":
            break
        if s == "test" or s == "t":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting informations about a test by:")
            print("Test id : i")
            print("Working group id : g")
            print("Method id : m")
            print("Test type : t")
            print("Launch date : l")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            if s == "i":
                print("Insert a test id:")
                print("Example: 2")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Test.by_id(id)
            elif s == "g":
                print("Insert a working group id:")
                print("Example: 1")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Test.by_user_id(id)
            elif s == "m":
                print("Insert a method id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Test.by_method_id(id)
            elif s == "t":
                print("Insert a test type:")
                print("ls ... local launching")
                print("r  ... remote launching")
                print("mc ... metacentrum")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Test.by_test_type(id)
            elif s == "l":
                print("Insert a launch date:")
                id = input()
                if id == "exit" or id == "e":
                    break
                try:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_launch_date(id)
                except Exception as e:
                    print(e)
                    break
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableTest.table_name, where,
                                                       0)

            print(sql_database.DatabaseHelper.TableTest.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "selection" or s == "se":
            print("Insert data-set ID:")
            print("Example: 5")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            print("Insert selection ID:")
            print("Example: 3")
            v = str(input(">>>")).strip()
            if v == "exit" or v == "e":
                break
            print("Insert a path to the file, which you want to write down the result in:")
            print("Example: C:\\Users\\User\\Documents\\Your_file.json")
            path = str(input(">>>")).strip()
            path = normalize_path(path)
            if path == "exit" or path == "e":
                break
            if file_database.dataset_exist(s) and file_database.selection_exist(s, v):
                train = file_database.get_selection_train(s, v)
                valid = file_database.get_selection_valid(s, v)
                test = file_database.get_selection_test(s, v)
                meta = file_database.get_metadata(s)
                file_formats.export_selection(train, valid, test, meta, path)
            else:
                print("Dataset or selection doesn't exist")
                continue
            break
        elif s == "dataset" or s == "d":
            print("Insert data-set ID:")
            print("Example: 5")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            print("Insert a path to the file, which you want to write down the result in:")
            print("Example: C:\\Users\\User\\Documents\\Your_file.json")
            path = str(input(">>>")).strip()
            path = normalize_path(path)
            if path == "exit" or path == "e":
                break
            if file_database.dataset_exist(s):
                meta = file_database.get_metadata(s)
                active = file_database.get_active(s)
                inactive = file_database.get_inactive(s)
                file_formats.export_dataset(active, inactive, meta, path)
            else:
                print("Data set doesn't exist")
                continue
            break
        elif s == "working group" or s == "g":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting informations about a working group by:")
            print("Group name : n")
            print("Working group ID : i")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            elif s == "n":
                print("Insert working group:")
                print("Example: anonymous")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.User.by_username(id)
            elif s == "i":
                print("Insert working group ID:")
                print("Example: 1")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.User.by_id(id)
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableUs.table_name, where, 0)
            print(sql_database.DatabaseHelper.TableUs.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "file" or s == "f":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting informations about files used in a test by:")
            print("file id : i")
            print("subtest id : s")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            if s == "i":
                print("Insert file id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.File.by_id(id)
            elif s == "s":
                print("Insert subtest id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.File.by_subtest_id(id)
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableFils.table_name, where, 0)
            print(sql_database.DatabaseHelper.TableFils.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "molecule" or s == "mo":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting information about molecules by:")
            print("Molecule id : i")
            print("Molecule name : n")
            print("subtest id : s")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            if s == "i":
                print("Insert molecule ID:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_id(id)
            elif s == "n":
                print("Insert name of molecule:")
                print("Example: 256311 or molecule_name")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_molecule_name(id)
            elif s == "s":
                print("Insert subtest id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_subtest_id(id)
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMol.table_name,
                                                       where, 0)
            print(sql_database.DatabaseHelper.TableMol.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "subtest" or s == "su":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting informations about a subtest by:")
            print("Subtest id: i")
            print("Test id: t")
            print("Data set id: d")
            print("Selection id: s")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            if s == "i":
                print("Insert a subtest id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id(id)
            elif s == "t":
                print("Insert a SQL test id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_test_id(id)
            elif s == "d":
                print("Insert a data set id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(id)
            elif s == "s":
                print("Insert a selection id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_selection(id)
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name, where,
                                                       0)
            print(sql_database.DatabaseHelper.TableSubtest.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "method" or s == "me":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting informations about method used in a test by:")
            print("method id : i")
            print("method type: t")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            if s == "i":
                print("Insert method id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Method.by_id(id)
            elif s == "t":
                print("Insert method type:")
                print("Example: py or general")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Method.by_method_type(id)
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name, where,
                                                       0)
            print(sql_database.DatabaseHelper.TableMethod.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "analyze" or "a":
            print(
                "For a return to the previous menu insert the word 'exit', else insert for getting informations about analyze used in a test by:")
            print("Analyze id : i")
            print("Subtest id: s")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            if s == "i":
                print("Insert analyze id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Analyze.by_id(id)
            elif s == "s":
                print("Insert subtest id:")
                print("Example: 5")
                id = input()
                if id == "exit" or id == "e":
                    break
                where = sql_database.DatabaseHelper.Clauses.Where.Analyze.by_subtest_id(id)
            else:
                print("Command not recognized")
                continue
            vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableAnalyze.table_name, where,
                                                       0)
            print(sql_database.DatabaseHelper.TableAnalyze.column_names)
            for vys in vysledek:
                print(vys)
            break
        elif s == "m":
            print(
                "Didn't you want to choice mo (for molecules) or me (for methods)?")
        elif s == "s":
            print(
                "Didn't you want to choice se (for selection) or su (for subtest)?")
        elif s == "all_methods_names":
            for name in file_database.get_all_methods_names():
                print(name)
        else:
            print("Command not recognized")


def do_put():
    while True:
        print(
            "What would you like to add? For choice of an option insert a characteristic letter or a word by the documentation.")
        print("c ... configuration")
        print("d ... dataset")
        print("e ... exit")
        print("g ... working group")
        print("m ... method")
        print("s ... selection")
        s = str(input(">>>")).strip()
        if s == "exit" or s == "e":
            break
        if s == "configuration" or s == "c":
            print("Insert configuration name")
            print("Example: gridMff")
            name = str(input(">>>")).strip()
            remote_computers = []
            while True:
                remote_computer = {}
                print("Insert information about a remote computer")
                print("Insert hostname")
                print("Example: u-pl10.ms.mff.cuni.cz")
                remote_computer['hostname'] = str(input(">>>")).strip()
                print("Insert username")
                print("Example: my_name")
                remote_computer['username'] = str(input(">>>")).strip()
                print("Insert password")
                print("Example: my_secret_password")
                remote_computer['password'] = str(input(">>>")).strip()
                print("Insert type of OS")
                print("Choices: LINUX | WINDOWS")
                remote_computer['OS_type'] = str(input(">>>")).strip()
                print("Insert python version")
                print("Example: 2.7")
                remote_computer['py_version'] = str(input(">>>")).strip()
                print("Add a remote computer with these parameters? (y/n)")
                print("Hostname: {0}".format(remote_computer['hostname']))
                print("Username: {0}".format(remote_computer['username']))
                print("Password: {0}".format(remote_computer['password']))
                print("OS type: {0}".format(remote_computer['OS_type']))
                print("Python version: {0}".format(remote_computer['py_version']))
                s = str(input(">>>")).strip()
                if s == "y":
                    remote_computers.append(remote_computer)
                    print("Computer was added")
                else:
                    print("Computer wasn't added")
                print("Do you want to continue? (y/n)")
                s = str(input(">>>")).strip()
                if s == "n":
                    break
            path = "../configurations/remote_computers/"
            os.makedirs(path)
            with open(os.path.join(path, "{0}.json".format(name)), "w") as stream:
                json.dump({
                    "pcs": remote_computers
                }, stream, indent=2)

        if s == "selection" or s == "s":
            print("Insert data-set ID")
            print("Example: 3")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            print("Insert a path to the file, which information about selection are in:")
            print("path/to/the/file.json")
            path = str(input(">>>")).strip()
            path = normalize_path(path)
            if path == "exit" or path == "e":
                break
            if not os.path.exists(path):
                print("File not found, the selection could not be loaded.")
                continue
            else:
                load_selection(s, path)
        elif s == "dataset" or s == "d":
            print("Insert a path to the file, which information about data-set are in:")
            print("Example: path/to/the/file.json")
            path = str(input(">>>")).strip()
            path = normalize_path(path)
            if path == "exit" or path == "e":
                break
            print("Insert a path to a SDF file:")
            print("Example: path/to/the/file.sdf")
            path_sdf = str(input(">>>")).strip()
            path_sdf = normalize_path(path_sdf)
            if path_sdf == "exit" or path_sdf == "e":
                break
            if not os.path.exists(path) or not os.path.exists(path_sdf):
                print("Data-set could be inserted, the path to the SDF file or to the data-set file was wrong.")
                continue
            else:
                load_dataset(path, path_sdf)
        elif s == "working group" or s == "g":
            print("Insert group name:")
            print("Example: BigDee")
            username = str(input(">>>")).strip()
            if username == "exit" or username == "e":
                break
            print("Insert next informations:")
            print("Size=156, ...")
            info = str(input(">>>")).strip()
            id = sql_database.add_user_to_db(username, info)
            print("ID of the new working group is:" + str(id))
        elif s == "method" or s == "m":
            print("Method type: (py|general)")
            type = str(input(">>>")).strip()
            if type == "exit" or type == "e":
                break
            if not type == "py" and not type == "general":
                print("Bad parameters")
                continue
            print("Where is the method file located?")
            print("Example: path/to/file.py")
            path = input(">>> Path: ")
            path = normalize_path(path)
            if path == "exit" or path == "e":
                break
            if os.path.exists(path):
                r_value, n_path = file_database.add_method_file_new_version(path,
                                                                            get_id_of_method(os.path.basename(path)))
                if type == "py":
                    print("Do you insert (r)epresentation method or (s)imilarity method?")
                    m_t = str(input(">>>")).strip()
                    if m_t == "exit" or m_t == "e":
                        break
                    if m_t == "r":
                        first_file = os.path.basename(n_path)
                        second_file = "Empty"
                        first_origin_name = os.path.basename(path)
                        second_origin_name = "Empty"
                    elif m_t == "s":
                        first_file = "Empty"
                        first_origin_name = "Empty"
                        second_file = os.path.basename(n_path)
                        second_origin_name = os.path.basename(path)

                else:
                    first_file = os.path.basename(n_path)
                    first_origin_name = os.path.basename(path)
                    second_file = "Empty"
                    second_origin_name = "Empty"
                n_id = sql_database.add_method_to_db(type, first_file, second_file, first_origin_name,
                                                     second_origin_name)
                print("New path of method is:", n_path)
                print("New id of method is:", n_id)
            else:
                print("Given path doesn't exists")
        else:
            print("Command not recognized")


def do_run():
    import main
    # dbs_initialization()
    tests = []
    # prihlaseni uzivatele
    iduser = ""
    while True:
        print("Insert group name ('host' for host):")
        login = input('>>>')
        if login == "exit" or login == "e":
            return
        if login == "host":
            iduser = 1
            break
        else:
            try:
                iduser = verify_user(login)
                break
            except BadUserInformationException:
                print(
                    "Group name wasn't recognized, insert name again. For unknown working group insert 'host'.")
    while True:
        test = {}
        test["user"] = iduser
        test['task_type'] = 'run'
        # test's information
        print("Insert describing information about test:")
        print(
            "Example: This test was launched to test the method XY of the version Z after adding a new concept of a similarity measure.")
        s = str(input(">>>")).strip()
        if s == "exit" or s == "e":
            return
        test["details"] = str(s)
        # oznaceni metody
        while True:
            print("Repeat number:")
            print("Example: 4")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                return
            try:
                rep = int(s)
                break
            except:
                print("Wrong typed number, insert repeat number again.")
                continue
        test["repeat"] = str(rep)
        while True:
            c = False
            repr = ""
            sim = ""
            print("Python modules (python)/ another file (general)?")
            typ = str(input(">>>")).strip()
            if typ == "exit" or typ == "e":
                return
            if typ == "python":
                cc = True
                # optimized
                while True:
                    print("Optimized version? (y/n)")
                    opt = str(input(">>>")).strip()
                    if opt == "exit" or opt == "e":
                        cc = False
                        break
                    else:
                        if opt == "y":
                            opt = "yes"
                            break
                        elif opt == "n":
                            opt = "no"
                            break
                        else:
                            print("{0} wasn't recognized".format(opt))
                            continue
                if not cc: continue
                while True:
                    print("Insert a path to a folder with similarity method module:")
                    print("Example: path/to/the/folder")
                    sim = str(input(">>>")).strip()
                    if not sim.startswith("&"):
                        sim = normalize_path(sim)
                    if sim == "exit" or sim == "e":
                        cc = False
                        break
                    if not sim.startswith("&") and not os.path.exists(sim):
                        print(
                            "File not found, insert the path again. For a return to higher level of menu insert 'exit'")
                        continue
                    else:
                        break
                if not cc: continue
                while True:
                    print("Insert a path to a folder with representation method module")
                    print("Example: path/to/the/folder")
                    repr = str(input(">>>")).strip()
                    if not repr.startswith("&"):
                        repr = normalize_path(repr)
                    if repr == "exit" or repr == "e":
                        cc = False
                        break
                    if not repr.startswith("&") and not os.path.exists(repr):
                        print(
                            "File not found, insert the path again. For a return to higher level of menu insert 'exit'")
                        continue
                    else:
                        break
                if not cc: continue
                c = True
                test['repr_method'] = repr
                test['sim_method'] = sim
                test['optimalization'] = opt
                test['typefile'] = "python"
            elif typ == "general":
                cc = True
                while True:
                    print("Insert a path to a folder with an execute file with computational method:")
                    print("Example: path/to/the/folder")
                    path = str(input(">>>")).strip()
                    if not path.startswith("&"):
                        path = normalize_path(path)
                    if path == "exit" or path == "e":
                        cc = False
                        break
                    if not path.startswith("&") and not os.path.exists(path):
                        print(
                            "File not found, insert the path again. For a return to higher level of menu insert 'exit'")
                        continue
                    else:
                        test['file'] = path
                        break
                if not cc: continue
                c = True
                test['typefile'] = "general"
            if c: break
        # vybery
        selections = []
        print("Insert your chosen selections:")
        while True:
            print("For a return back insert 'exit' or if you are done type 'done'")
            print("Insert data-set ID:")
            print("Example: 5")
            dataset = str(input(">>>")).strip()
            if dataset == "exit":
                return
            elif dataset == "done":
                break
            print("Insert selection ID:")
            print("Example: 15/'ok' if you wanna take all selections")
            selection = str(input(">>>")).strip()
            if selection == "exit":
                return
            elif selection == "done":
                break
            elif selection == "ok":
                selections.append({"dataset": dataset})
                continue
            print("Insert selection {0} in data-set {1}?".format(selection, dataset))
            s = input(">>> (ok/exit/done)")
            if s == "ok":
                selections.append({"dataset": dataset, "selection": selection})
            elif s == "exit":
                return
            elif s == "done":
                break
            else:
                continue
        test["d/s"] = selections
        if len(selections) > 0:
            test["selections"] = "yes"
        else:
            test["selections"] = "no"
        break_option = False
        # vicevlakonovost vyberu
        if not break_option:
            while True:
                print(
                    "Do you want to launch process parallel testing of selections (y/n)?")
                par = str(input(">>>")).strip()
                if par == "y" or par == "n":
                    if par == "y":
                        test['parallel_selections'] = "True"
                        break_option = True
                    else:
                        test['parallel_selections'] = "False"
                    break
                else:
                    print("Parameter is assign wrong")
        # vicepc na vybery
        if not break_option:
            while True:
                print(
                    "Do you want to launch multi-computer parallel launching of selections(y/n)?")
                par = str(input(">>>")).strip()
                if par == "y" or par == "n":
                    if par == "y":
                        test['parallel_pc'] = "True"
                        break_option = True
                        while True:
                            print("Insert a path to the file with information about PCs")
                            print("Example: path/to/a/file/with/pcs.json")
                            path_s = str(input(">>>")).strip()
                            path_s = normalize_path(path_s)
                            if os.path.exists(path_s):
                                test['pc_to_use'] = path_s
                                break
                            else:
                                print("Wrong path")
                                continue
                    else:
                        test['parallel_pc'] = "False"
                    break
                else:
                    print("Wrong assigned parameter")
        if not break_option:
            while True:
                print(
                    "Do you want to launch selection on Metacentrum (y/n)?")
                par = str(input(">>>")).strip()
                if par == "y" or par == "n":
                    if par == "y":
                        test['metacentrum'] = "True"
                        while True:
                            print("Insert a path to the file with front nodes, which are prepared to use:")
                            print("Example: path/to/a/file/with/front_nodes.json")
                            path_s = str(input(">>>")).strip()
                            path_s = normalize_path(path_s)
                            if os.path.exists(path_s):
                                test['front_ends_nodes'] = path_s
                                break
                            else:
                                print("Wrong path")
                                continue
                        while True:
                            print("Insert a path to the file with login data:")
                            print("Example: path/to/a/file/with/login_data.json")
                            path_s = str(input(">>>")).strip()
                            path_s = normalize_path(path_s)
                            if os.path.exists(path_s):
                                test['login_data'] = path_s
                                break
                            else:
                                print("Wrong path")
                                continue
                    else:
                        test['metacentrum'] = "False"
                    break
                else:
                    print("Wrong assigned parameter")
                    continue
        tests.append(test)
        print("Add a next test? (y/n)")
        s = str(input(">>>")).strip()
        if s == "y":
            continue
        else:
            break
    test_count = len(tests)
    if test_count == 0:
        print("Count of entered tests is equal to zero")
        return
    ignore = []
    parallel_tests = ""
    while True:
        print("Would you like to launch tests in the parallel way (y/n)? ")
        par = str(input(">>>")).strip()
        if par == "y" or par == "n":
            if par == "y":
                parallel_tests = "True"
            else:
                parallel_tests = "False"
            break
        else:
            print("Wrong assigned parameter")
            continue
    metadata = {'test_count': test_count, "ignore": ignore, "parallel_tests": parallel_tests}
    print("Launch?")
    ans = input(">>> (y/n)")
    if ans == "y":
        input_path = file_formats.create_tests_input(tests, metadata)
        main.main_local(input_path)
        try:
            os.remove(input_path)
        except:
            pass


def do_init():
    """
    A dialog for initialization.

    :return: Nothing
    """
    print("Which database would you like to initialize? Confirm y(es)/n(o), another")
    print("Would you like to initialize data-sets database?")
    s = input(">>>")
    if s == ex:
        return
    if s == "y":
        file_database.update_database()
    print("Would you like to initialize SQL database?")
    s = input(">>>")
    if s == ex:
        return
    if s == "y":
        print("Delete the origin table of users (y/n)?")
        if input(">>>") == "y":
            tu = True
        else:
            tu = False
        print("Delete the origin table of tests (y/n)?")
        if input(">>>") == "y":
            tt = True
            delete_all_tests()
        else:
            tt = False
        print("Delete the origin table of molecules (y/n)?")
        if input(">>>") == "y":
            tmo = True
        else:
            tmo = False
        print("Delete the origin table of analyzes (y/n)?")
        if input(">>>") == "y":
            ta = True
        else:
            ta = False
        print("Delete the origin table of files (y/n)?")
        if input(">>>") == "y":
            tf = True
        else:
            tf = False
        print("Delete the origin table of subtests (y/n)?")
        if input(">>>") == "y":
            ts = True
        else:
            ts = False
        print("Delete the origin table of methods (y/n)?")
        if input(">>>") == "y":
            tme = True
            delete_all_methods()
        else:
            tme = False
        sql_database.init_sqldatabase(tu, tt, tmo, ta, tf, ts, tme, False)


def do_merge():
    """
    Merges SQL database with file.
    :return: Nothing
    """
    import datetime
    import json
    import shutil
    while True:
        print("For exit write 'e'.")
        print("Write your working group id")
        user_id = str(input("ID (i.e. 1): ")).strip()
        try:
            user_id = int(user_id)
        except Exception as e:
            print(e)
            continue
        print("Write a path where the input folder is placed")
        path = str(input("Path: ")).strip().replace("\"", "").replace("\'", "")
        if path == "e":
            break
        if not os.path.exists(path):
            print("The given path is wrong?")
            continue
        path = os.path.join(path, "move")
        if not os.path.exists(path):
            print("The given path is wrong?")
            continue
        info_file_path = os.path.join(path, "info.json")
        with open(info_file_path, "r") as stream:
            tests = json.load(stream)['data']
            for test in tests:
                try:
                    test_id = test["test_id"]
                    test_type = test['test_type']
                    subcount = test['subcount']
                    date = test['date']
                    description = test['description']
                    try:
                        date = str(date).split("-")
                        date = datetime.date(int(date[0]), int(date[1]), int(date[2]))
                    except Exception as e:
                        print(e)
                        continue
                    test_folder_path = os.path.join(path, str(test_id))
                    method_subdict = test['method']
                    type = method_subdict['type']
                    sname = method_subdict['sname']
                    fname = method_subdict['fname']
                    sfile = method_subdict['sfile']
                    ffile = method_subdict['ffile']
                    fid = get_id_of_method(fname)
                    f_method_path = os.path.join(test_folder_path, ffile)
                    if os.path.exists(f_method_path):
                        r_value, ffile_path = file_database.add_method_file_new_version(f_method_path, fid)
                    else:
                        ffile_path = "Empty"
                    sid = get_id_of_method(sname)
                    s_method_path = os.path.join(test_folder_path, sfile)
                    if os.path.exists(s_method_path):
                        r_value, sfile_path = file_database.add_method_file_new_version(s_method_path, sid)
                    else:
                        sfile_path = "Empty"
                    m_id = sql_database.add_method_to_db(type, ffile_path, sfile_path, fname, sname)
                    t_id = file_database.get_safe_number_of_test()
                    t_folder_path = file_database.new_test(t_id)
                    sql_database.add_test_to_db(t_id, user_id, test_type, subcount, date, m_id, description)
                    for key in test:
                        if str(key).startswith("subtest"):
                            subtest = test[key]
                            runtime = subtest["runtime"]
                            sel_id = subtest["id_selection"]
                            subtest_id = subtest["id"]
                            ds_id = subtest["id_dataset"]
                            ef_0_01 = subtest["ef_0_01"]
                            ef_0_02 = subtest["ef_0_02"]
                            auc = subtest["auc"]
                            ef_0_05 = subtest["ef_0_05"]
                            ef_0_005 = subtest["ef_0_005"]
                            st_id = sql_database.add_subtest_to_db(t_id, runtime, ds_id, sel_id)
                            sql_database.add_analyze_to_db(st_id, auc, ef_0_01, ef_0_02, ef_0_05, ef_0_005)
                            molecules = subtest['molecule_values']
                            sql_database.add_mols_to_db(st_id, molecules)
                            a_file = "analyze_" + str(subtest_id) + ".json"
                            t_file = "test_" + str(subtest_id) + ".json"
                            analyze_file = os.path.join(test_folder_path, a_file)
                            test_file = os.path.join(test_folder_path, t_file)
                            if os.path.exists(analyze_file) and os.path.exists(test_file):
                                a_file_path = file_database.add_analysis_file(analyze_file, t_folder_path, subtest_id)
                                t_file_path = file_database.add_result_file(test_file, t_folder_path, subtest_id)
                                sql_database.add_files_to_db(st_id, t_file_path, a_file_path)
                except Exception as e:
                    print(e)
                    continue


def do_get_data():
    """
    Return a file with data from database.
    :return:
    """
    import shutil
    while True:
        print("For exit write 'e'.")
        print("Write a path where the result folder should be placed")
        path = str(input("Path: ")).strip().replace("\"", "").replace("\'", "")
        if path == "e":
            break
        if os.path.exists(path):
            print("Are you sure that you want to override existing files because the result path already exists (y/n)?")
            ans = str(input("Answer: ")).strip()
            if not ans == "y":
                continue
        base_folder_path = os.path.join(path, "move")
        try:
            os.mkdir(base_folder_path)
        except Exception:
            pass
        print("Write the date of the first result (include, format: DD.MM.YYYY)")
        f_res = str(input("Date: ")).strip().split(".")
        if f_res[0] == "e":
            break
        try:
            f_res = "\"" + "{0}-{1}-{2}".format(f_res[2], f_res[1], f_res[0]).replace("\"", "").replace("\'", "") + "\""
        except Exception as e:
            print("Bad date format: " + e)
            continue
        print("Write the date of the first result (include, format: DD.MM.YYYY)")
        l_res = str(input("Date: ")).strip().split(".")
        if l_res[0] == "e":
            break
        try:
            l_res = "\"" + "{0}-{1}-{2}".format(l_res[2], l_res[1], l_res[0]).replace("\"", "").replace("\'", "") + "\""
        except Exception as e:
            print("Bad date format: " + e)
            continue
        print("Data processing...")
        tests = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableTest.table_name,
                                                sql_database.DatabaseHelper.Clauses.Where.Test.by_between_dates(f_res,
                                                                                                                l_res),
                                                0)
        test_list = []
        i = 1
        # create temp path
        # iteration over tests
        for test_info in tests:
            result_dictionary = {}
            print("Data processing...")
            result_dictionary['test_id'] = test_info[6]
            result_dictionary['test_type'] = test_info[2]
            result_dictionary['subcount'] = test_info[3]
            test_id = test_info[6]
            test_sql_id = test_info[0]
            result_dictionary['description'] = test_info[7]
            test_folder_path = os.path.join(base_folder_path, str(test_id))
            try:
                os.mkdir(test_folder_path)
            except Exception:
                pass
            i += 1
            method_id = test_info[4]
            result_dictionary['date'] = (test_info[5]).isoformat()
            method_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name,
                                                          sql_database.DatabaseHelper.Clauses.Where.Method.by_id(
                                                              method_id), 0)[0]
            method_dict = {
                "type": method_info[1],
                "sfile": os.path.basename(method_info[3]),
                "ffile": os.path.basename(method_info[2]),
                "fname": method_info[4],
                "sname": method_info[5]
            }
            first_method_path = os.path.join(file_database.get_methods_folder(), method_info[2])
            second_method_path = os.path.join(file_database.get_methods_folder(), method_info[3])
            try:
                shutil.copytree(first_method_path, os.path.join(test_folder_path, os.path.basename(first_method_path)))
            except Exception as e:
                print("Error: " + str(e))
            try:
                shutil.copytree(second_method_path,
                                os.path.join(test_folder_path, os.path.basename(second_method_path)))
            except Exception as e:
                print("Error: " + str(e))
            result_dictionary['method'] = method_dict
            subtests_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name,
                                                            sql_database.DatabaseHelper.Clauses.Where.Subtest.by_test_id(
                                                                test_sql_id), 0)
            i = 1
            for subtest in subtests_info:
                sub_dict = {
                    "runtime": subtest[4],
                    "id_dataset": subtest[2],
                    "id_selection": subtest[3]
                }
                analyze_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableAnalyze.table_name,
                                                               sql_database.DatabaseHelper.Clauses.Where.Analyze.by_subtest_id(
                                                                   subtest[0]), 0)
                files_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableFils.table_name,
                                                             sql_database.DatabaseHelper.Clauses.Where.File.by_subtest_id(
                                                                 subtest[0]), 0)
                try:
                    analyze_info = analyze_info[0]
                except:
                    print("Bad analyze info")
                    continue
                try:
                    files_info = files_info[0]
                except:
                    print("Bad file info")
                    continue
                sub_dict["auc"] = analyze_info[2]
                sub_dict["ef_0_005"] = analyze_info[3]
                sub_dict["ef_0_01"] = analyze_info[4]
                sub_dict["ef_0_02"] = analyze_info[5]
                sub_dict["ef_0_05"] = analyze_info[6]
                sub_dict['id'] = (os.path.basename(files_info[2])).split(".")[0].split("_")[1]
                afile_path = os.path.abspath(files_info[2])
                rfile_path = os.path.abspath(files_info[3])
                try:
                    shutil.copy2(afile_path, os.path.join(test_folder_path, os.path.basename(afile_path)))
                except Exception as e:
                    print("Error: " + str(e))
                try:
                    shutil.copy2(rfile_path, os.path.join(test_folder_path, os.path.basename(rfile_path)))
                except Exception as e:
                    print("Error: " + str(e))
                molecules_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMol.table_name,
                                                                 sql_database.DatabaseHelper.Clauses.Where.Molecule.by_subtest_id(
                                                                     subtest[0]), 0)
                molecules_values = []
                for mol in molecules_info:
                    molecules_values.append(
                        {"name": mol[2], "similarity": mol[3], "score_molecule": mol[4], "description": mol[5]})
                sub_dict['molecule_values'] = molecules_values
                key_name = 'subtest_' + str(i)
                result_dictionary[key_name] = sub_dict
                i += 1
            test_list.append(result_dictionary)
        file_location = os.path.join(base_folder_path, "info.json")
        file_formats.write_run_query_result(test_list, file_location, True)
        break


def do_man():
    """
    Interactive mode of managing the program
    :return: Nothing
    """
    while True:
        print(
            "What would you like to do? For choice of an option insert a characteristic letter or a word by the documentation.")
        print("m ... merge (merging local database and file with data from remote SQL database)")
        print("g ... get (getting information about test for merging option)")
        print("e ... exit")
        print("i ... init (initialize databases)")
        s = str(input(">>>")).strip()
        if s == "exit" or s == "e":
            break
        if s == "merge" or s == "m":
            do_merge()
        elif s == "get" or s == "g":
            do_get_data()
        elif s == "i" or s == "init":
            do_init()


def do_delete():
    while True:
        print(
            "What would you like to delete? For choice of an option insert a characteristic letter or a word by the documentation.")
        print("d ... dataset")
        print("e ... exit")
        print("m ... method")
        print("s ... selection")
        print("t ... test")
        s = str(input(">>>")).strip()
        if s == "exit" or s == "e":
            break
        if s == "test" or s == "t":
            print(
                "For a return to the previous menu insert the word 'exit', else insert SQL ID of test:")
            print("Example: 5")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            else:
                delete_test(s)
            break
        elif s == "selection" or s == "s":
            print(
                "For a return to the previous menu insert the word 'exit', else insert name of data-set:")
            print("Example: APHA")
            name = str(input(">>>")).strip()
            if name == "exit" or name == "e":
                break
            else:
                print("Insert selection ID:")
                print("Example: 5")
                id = str(input(">>>")).strip()
                delete_selection(name, id)
            break
        elif s == "dataset" or s == "d":
            print(
                "For a return to the previous menu insert the word 'exit', else insert name of data-set:")
            print("Example: APHA")
            name = str(input(">>>")).strip()
            if name == "exit" or name == "e":
                break
            else:
                delete_data_set(name)
            break
        elif s == "method" or s == "m":
            print(
                "For a return to the previous menu insert the word 'exit', else insert SQL ID of method:")
            print("Example: 5")
            s = str(input(">>>")).strip()
            if s == "exit" or s == "e":
                break
            else:
                delete_method(s)
            break
        else:
            print("Command not recognized")


def normalize_path(path):
    """
    Corrects a path.
    :param path: Original string
    :return: Normalized string
    """
    if path.startswith("\""):
        path = path[1:]
    if path.endswith("\""):
        path = path[:len(path) - 1]
    return path
