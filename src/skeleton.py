import json
import os
import multiprocessing, logging
import sys


def divide_repr(train_molecules, validation_molecules, test_mols, train_reprezentation, validation_reprezentation,
                test_reprezentation, representation):
    """
    Divide representations from variable representation to variables train_act_repr, train_act_repr, test_repr by their name.

    :param representation: Proxy dictionary with representation of all molecules.
    :param test_reprezentation: Proxy list for representation of elements of test_mols
    :param train_reprezentation: Proxy dict for representation of elements of train_molecules
    :param validation_reprezentation: Proxy dict for representation of elements of validation_molecules
    :param train_molecules: Dictionary with (in)active molecules names in train data set.
    :param validation_molecules: Dictionary with (in)active molecules names in validation data set.
    :param test_mols: Molecules in test_mols data set.
    :return: Nothing
    """
    keys = representation.keys()
    t_act = train_reprezentation['active']
    t_inact = train_reprezentation['inactive']
    v_act = validation_reprezentation['active']
    v_inact = validation_reprezentation['inactive']
    for repr_molecule_name in keys:
        if repr_molecule_name in train_molecules['active']:
            t_act.append({'reprezentation': representation[repr_molecule_name], 'name': str(repr_molecule_name)})
        elif repr_molecule_name in train_molecules['inactive']:
            t_inact.append({'reprezentation': representation[repr_molecule_name], 'name': str(repr_molecule_name)})
        elif repr_molecule_name in validation_molecules['inactive']:
            v_act.append({'reprezentation': representation[repr_molecule_name], 'name': str(repr_molecule_name)})
        elif repr_molecule_name in validation_molecules['active']:
            v_inact.append({'reprezentation': representation[repr_molecule_name], 'name': str(repr_molecule_name)})
        elif repr_molecule_name in test_mols:
            test_reprezentation.append(
                {'reprezentation': representation[repr_molecule_name], 'name': str(repr_molecule_name)})
    train_reprezentation['active'] = t_act
    train_reprezentation['inactive'] = t_inact
    validation_reprezentation['active'] = v_act
    validation_reprezentation['inactive'] = v_inact
    dd = 47


def repr_method(func, i, representation, sdf_path):
    """
    Run function "func" on molecules in list of molecules from index start to index stop.

    :param sdf_path: Path to sdf_file.
    :param representation: Proxy dictionary with representation of molecules.
    :param func: Function making representations
    :param start: Start index
    :param stop: End index
    :return: Nothing
    """
    from rdkit import Chem
    ms = Chem.SDMolSupplier(sdf_path)
    step = len(ms) / multiprocessing.cpu_count()
    start = int(i * step)
    stop = int((i + 1) * step)
    for k in range(start, stop):
        mol = ms[k]
        representation[mol.GetProp('_Name')] = func(mol)


def sim_method(func, start, stop, scores, train_molecules, validation_molecules, test_repr):
    """
    Run function "func" on representations in list representation from index start to index stop.

    :param test_repr: Proxy list of representation of elements of test molecules
    :param validation_molecules: Proxy dictionary of representation of elements of (in)active molecules
    :param train_molecules: Proxy dictionary of representation of elements of (in)active train molecules
    :param scores: Proxy list for score of molecules.
    :param func: Function creating scores
    :param start: Start index
    :param stop: End index
    :return: Nothing
    """
    for i in range(start, stop):
        scores[i] = func(train_molecules, validation_molecules, test_repr[i]['reprezentation'], test_repr[i]['name'])


def write_result(output_path, scores, metadata):
    """
    Write result into file.
    :param output_path: Path to output file.
    :param scores: List with dictionaries containing scores of molecules from test.
    :param metadata: Metadata to test.
    :return: True if writing of result was successful.
    """
    if output_path is not None and not output_path == '':
        path_dir = os.path.dirname(output_path)
        if not os.path.exists(os.path.dirname(output_path)) and not path_dir == '':
            os.makedirs(os.path.dirname(output_path))
        with open(output_path, 'w') as output_stream:
            json.dump({
                'data': scores,
                'metadata': metadata
            }, output_stream, indent=2)
        return True
    else:
        return False


def get_main_file(root_of_tree):
    """
    Returns a main launching file.

    :param root_of_tree: Main folder of method.
    :return: True/False, filename
    """
    files = os.listdir(root_of_tree)
    for file in files:
        if not file.find("main") == -1:
            return True, os.path.basename(file)
    return False, ""


def import_module(module_path, module_name):
    try:
        if sys.version_info >= (3, 5):
            import importlib
            # spec = importlib.util.spec_from_file_location(module_name, module_path)
            # mod = importlib.util.module_from_spec(spec)
            # spec.loader.exec_module(mod)
            mod_name = os.path.basename(module_path).replace(".py", "")
            method_folder_name = os.path.basename(os.path.dirname(module_path))
            mod_name = method_folder_name + "." + mod_name
            mod = importlib.import_module(mod_name)
            importlib.invalidate_caches()
            return mod
        else:
            import imp
            mod = imp.load_source(module_name, module_path)
            return mod
    except Exception as e:
        sys.tracebacklimit = None
        raise IOError("{0} couldn't be loaded: {0}".format(module_name, e))


def execute_opt(config, trains, valids, test):
    """
    Execute test on given selection using dictionary config. This method is determined for optimized version.

    :param config: Dictionary with needed elements adjusting test configuration.
    :param trains: Molecules from train data set
    :param valids: Molecules from validation data set
    :param test: Molecules from test data set.
    :return: If no bad situation sets in true if test was successful else false else raise an exception.
    """
    try:
        mngr = multiprocessing.Manager()
    except Exception as e:
        print(e)
    representation = mngr.dict()
    scores = mngr.list()
    train_molecules = mngr.dict()
    train_molecules['active'] = []
    train_molecules['inactive'] = []
    validation_molecules = mngr.dict()
    validation_molecules['active'] = []
    validation_molecules['inactive'] = []
    test_repr = mngr.list()

    if config is not None:
        try:
            if not os.path.exists(str(os.path.abspath(config['sdf']))):
                print("SDF file is missing")
                return False
            # loading module
            r_value, filename = get_main_file(config['repr_method_path'])
            repr_module_path = os.path.join(os.path.abspath(config['repr_method_path']), filename)
            repr_module = import_module(repr_module_path, "repr_module")
            try:
                processes = []
                for i in range(0, multiprocessing.cpu_count()):
                    t = multiprocessing.Process(target=repr_method,
                                                args=(repr_module.loading, i, representation,
                                                      str(os.path.abspath(config['sdf']))))
                    t.start()
                    processes.append(t)
                for t in processes:
                    t.join()
            except Exception as e:
                print("Error in multiprocessing")
                raise e
        except Exception as e:
            adds = ""
            if not str(e).find("File error") == -1:
                adds += "\nIf the path exists and this error occurs, check if you don't have application stored in location which uses non ASCII characters. For example: from czech alphabet - users/Jaromír Novák/myApp"
            print("Unexpected intern error in {0} during enumeration representation. Error description: {1}".format(
                os.path.basename(config['repr_method_path']), e) + adds)
        try:
            divide_repr(trains, valids, test, train_molecules, validation_molecules, test_repr, representation)
        except Exception as e:
            print("Unexpected intern error during dividing molecules: \n", e)
            return False
        try:
            r_value, filename = get_main_file(config['sim_method_path'])
            sim_module_path = os.path.join(os.path.basename(config['sim_method_path']), filename)
            sim_module = import_module(sim_module_path, "sim_module")
            try:
                processes = []
                step = len(test_repr) / multiprocessing.cpu_count()
                ############ WARNING ##############
                for _ in range(len(test_repr)):
                    scores.append({})
                for i in range(0, multiprocessing.cpu_count()):
                    t = multiprocessing.Process(target=sim_method,
                                                args=(sim_module.scoring, int(i * step), int((i + 1) * step), scores,
                                                      train_molecules, validation_molecules, test_repr))
                    t.start()
                    processes.append(t)
                for t in processes:
                    t.join()
            except Exception as e:
                print("Error in threading")
                raise e
        except Exception as e:
            print("Unexpected intern error in {0} during enumeration score. Error description: {1}".format(
                os.path.basename(config['sim_method_path']), e))
            return False
        sc = list(scores)
        if write_result(config['output'], sc, config):
            return True
        else:
            print('Output_path parameter wasn\'t found. Result couldn\'t be written.')
    else:
        sys.tracebacklimit = None
        raise IOError('The configuration file not found')


def execute_no_opt(config, trains, valids, test):
    """
    Execute test on given selection using dictionary config. This method is determined for non-optimized version.
    :param config: Dictionary with needed elements adjusting test configuration.
    :param trains: Molecules from train data set
    :param valids: Molecules from valid data set
    :param test: Molecules from test data set.
    :return: If no bad situation sets in true if test was successful else false else raise an exception.
    """
    if config is not None:
        # creates path
        r_value, filename = get_main_file(config['repr_method_path'])
        repr_module_path = os.path.join(os.path.abspath(config['repr_method_path']), filename)
        repr_module = import_module(repr_module_path, "repr_module")
        try:
            if not os.path.exists(str(os.path.abspath(config['sdf']))):
                print("SDF file is missing")
                return False
            represent = repr_module.loading(str(os.path.abspath(config['sdf'])))
        except Exception as e:
            adds = ""
            if not str(e).find("File error") == -1:
                adds += "\nIf the path exists and this error occurs, check if you don't have application stored in location which uses non ASCII characters. For example: from czech alphabet - users/Jaromír Novák/myApp"
            print("Unexpected intern error in {0} during enumeration representation. Error description: {1}".format(
                os.path.basename(config['repr_method_path']), e) + adds)
            return False
        # creates path
        r_value, filename = get_main_file(config['sim_method_path'])
        sim_module_path = os.path.join(os.path.basename(config['sim_method_path']), filename)
        sim_module = import_module(sim_module_path, "sim_module")
        try:
            score = sim_module.scoring(represent, trains, valids, test)
        except Exception as e:
            print("Unexpected intern error in {0} during enumeration score. Error description: {1}".format(
                os.path.basename(config['sim_method_path']), e))
            return False
        if write_result(config['output'], score, config):
            return True
        else:
            print('Output_path parameter wasn\'t found. Result couldn\'t be written.')
    else:
        sys.tracebacklimit = None
        raise IOError('The configuration file not found')
